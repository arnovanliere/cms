<?php

global $passed_tests;

include __DIR__ . "/../test_config.php";

class ClientTest extends Client {

    /**
     * ClientTest constructor.
     * @param Database $db       Database to initialize Dashboard with
     * @param string $lang       Language of the dashboard
     * @param string $site_title The title of the site
     * @throws SetupException    If unknown language is provided
     */
    public function __construct(Database $db, string $lang, string $site_title) {
        parent::__construct($db, $lang, $site_title);
    }

    /**
     * Test if correct default page is returned
     * @param string $expected The alias of the expected page
     * @throws PagesException  If no pages were added
     */
    public function test_default_page(string $expected) {
        $actual = $this->get_default_page();
        if ($actual != $expected) {
            TestUtils::print(TestUtils::RED, "$actual != $expected\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "$actual == $expected\n");
        }
    }

}

TestUtils::print_filename(__FILE__);
$client_test = new ClientTest($db, $language, $site_title);
$client_test->add_module($pages);
$client_test->test_default_page("home");
$passed_tests++;