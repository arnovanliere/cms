<?php

global $passed_tests;

include __DIR__ . "/../../test_config.php";

class StyleTest extends Style {

    /**
     * Test the 'get_mod_def_page()' for the Blog-module
     */
    public function test_get_default_page() {
        if ($this->get_mod_def_page() != Style::STYLE_PAGE_TAG) {
            TestUtils::print(TestUtils::RED, "default page != expected default page\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "default page == expected default page\n");
        }
    }

}

TestUtils::print_filename(__FILE__);
$style_test = new StyleTest();
$style_test->test_get_default_page();
$passed_tests++;
