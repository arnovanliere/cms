<?php

global $passed_tests;

include __DIR__ . "/../../test_config.php";

class CalendarTest extends Calendar {

    /**
     * CalendarTest constructor.
     * @param array $options     Options to add to the Calendar-module to test
     * @throws CalendarException If duplicate column-names are provided
     */
    public function __construct(array $options) {
        parent::__construct($options);
    }

    /**
     * Test if correct options are returned
     * @param array $expected The expected options
     */
    public function test_get_options(array $expected) {
        $actual = $this->get_options();
        if ($actual != $expected) {
            TestUtils::print(TestUtils::RED, "actual options != expected options\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "actual options == expected options\n");
        }
    }

}

TestUtils::print_filename(__FILE__);
$calendar_test = new CalendarTest($calendar_options);
$calendar_test->test_get_options($calendar_options);
$passed_tests++;