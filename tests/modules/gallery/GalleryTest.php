<?php

global $passed_tests;

include __DIR__ . "/../../test_config.php";

class GalleryTest extends Gallery {

    /**
     * CalendarTest constructor.
     * @param null|array $options Options to add to the Gallery-module to test
     */
    public function __construct(?array $options = null) {
        parent::__construct($options);
    }

    /**
     * Test if options are correctly saved.
     * @param bool $expected The expected return-value of get_var_categories()
     */
    public function test_get_var_categories(bool $expected) {
        if ($this->get_var_categories() != $expected) {
            TestUtils::print(TestUtils::RED, "actual options != expected options\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "actual options == expected options\n");
        }
    }

}

TestUtils::print_filename(__FILE__);
$gallery_test = new GalleryTest([Gallery::OPT_CATEGORIES]);
$gallery_test->test_get_var_categories(true);
$passed_tests++;
$gallery_test = new GalleryTest([]);
$gallery_test->test_get_var_categories(false);
$passed_tests++;