<?php

global $passed_tests;

include __DIR__ . "/../../test_config.php";

class PagesTest extends Pages {

    /**
     * PagesTest constructor.
     * @param array $options  Array of PageDetails to add
     * @throws PagesException If Pages() throws it
     */
    public function __construct(array $options) {
        parent::__construct($options, self::MODE_MULTIPLE_PAGES);
    }

    /**
     * Test get_pages()
     * @param array $expected The expected pages
     */
    public function test_get_pages(array $expected) {
        $actual = $this->get_pages();
        if ($actual != $expected) {
            TestUtils::print(TestUtils::RED, "actual pages != expected pages\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "actual pages == expected pages\n");
        }
    }

    /**
     * Test get_page_title()
     * @param string $alias         The alias of the page to retrieve the title from
     * @param string|null $expected The expected title
     */
    public function test_get_page_title(string $alias, ?string $expected) {
        $actual = $this->get_page_title($alias);
        $actual = (isset($actual)) ? $actual : "null";
        $expected  = (isset($expected)) ? $expected : "null";
        if ($actual != $expected) {
            TestUtils::print(TestUtils::RED, "$actual != $expected\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "$actual == $expected\n");
        }
    }

    /**
     * Test get_default_page()
     * @param PageDetails|null $expected The expected details of the default page
     */
    public function test_get_default_page(?PageDetails $expected) {
        $actual = $this->get_default_page();
        $actual = (isset($actual)) ? $actual->title . "|" . $actual->alias : "null";
        $expected  = (isset($expected)) ? $expected->title . "|" . $expected->alias : "null";
        if ($actual !== $expected) {
            TestUtils::print(TestUtils::RED, "$actual != $expected\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "$actual == $expected\n");
        }
    }

    /**
     * Test get_mod_page() for different modules
     * @param string $class         The name of the module to test
     * @param string|null $expected The expected alias of the page that contains the module
     */
    public function test_get_mod_page(string $class, ?string $expected) {
        $actual = $this->get_mod_page($class);
        $actual = (isset($actual)) ? $actual->alias : "null";
        $expected  = (isset($expected)) ? $expected : "null";
        if ($actual !== $expected) {
            TestUtils::print(TestUtils::RED, "$actual != $expected\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "$actual == $expected\n");
        }
    }

}

TestUtils::print_filename(__FILE__);
$pages_test = new PagesTest($pages_details);
$pages_test->test_get_pages($pages_details);
$passed_tests++;
$pages_test->test_get_page_title("", null);
$passed_tests++;
$pages_test->test_get_page_title("home", "Home");
$passed_tests++;
$pages_test->test_get_default_page($pages_details[0]);
$passed_tests++;
$pages_test->test_get_mod_page(Calendar::class, "events");
$passed_tests++;
$pages_test->test_get_mod_page(Blog::class, "blog");
$passed_tests++;
$pages_test->test_get_mod_page(Gallery::class, "gallery");
$passed_tests++;