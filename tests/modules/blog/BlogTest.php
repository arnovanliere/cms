<?php

global $passed_tests;

include __DIR__ . "/../../test_config.php";

class BlogTest extends Blog {

    /**
     * Test the 'get_mod_def_page()' for the Blog-module
     */
    public function test_get_default_page() {
        if ($this->get_mod_def_page() != Blog::ADD_POST_PAGE_TAG) {
            TestUtils::print(TestUtils::RED, "default page != expected default page\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "default page == expected default page\n");
        }
    }

}

TestUtils::print_filename(__FILE__);
$blog_test = new BlogTest();
$blog_test->test_get_default_page();
$passed_tests++;