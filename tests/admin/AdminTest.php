<?php

global $passed_tests;

include __DIR__ . "/../test_config.php";

class AdminTest extends Dashboard {

    /**
     * AdminTest constructor.
     * @param Database $db       Database to initialize Dashboard with
     * @param string $lang       Language of the dashboard
     * @param string $site_title The title of the site
     * @throws SetupException    If unknown language is provided
     */
    public function __construct(Database $db, string $lang, string $site_title) {
        parent::__construct($db, $lang, $site_title);
    }

    /**
     * Test if correct default module is returned
     * @param string $expected The ID of the module that should be returned
     */
    public function test_module(string $expected) {
        $actual = $this->get_default_module();
        if ($actual != $expected) {
            TestUtils::print(TestUtils::RED, "$actual != $expected\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "$actual == $expected\n");
        }
    }

}

TestUtils::print_filename(__FILE__);
$admin_test = new AdminTest($db, $language, $site_title);
$admin_test->add_module($calendar);
$admin_test->test_module(Calendar::ID);
$passed_tests++;