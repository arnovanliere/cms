<?php

include_once __DIR__ . "/../config/classes.php";
include_once __DIR__ . "/test_config.php";

$nr_pages = 0;
// Only count those pages if HTTP-tests are ran
if (!CLI::has_option("--gitlab")) {
    $nr_pages = TestUtils::count_pages($pages_details);
}

$passed_tests = 0;
$total_tests = 20 + $nr_pages;

/**
 * Execute all files in 'dir'.
 * @param string $dir     The directory to find the files in
 * @param bool $skip_http Whether to skip the HTTP-tests
 */
function find_files(string $dir, bool $skip_http) {
    foreach (new DirectoryIterator($dir) as $filesAndDir) {
        $name = $filesAndDir->getFilename();
        if ($filesAndDir->isFile() &&
            $name != "test_suite.php" &&
            $name != "test_config.php") {
            try {
                if (!$skip_http || $name != "HttpTest.php") {
                    include "${dir}/${name}";
                }
            } catch (Exception $e) {
                TestUtils::print(TestUtils::RED, "Exception: " . $e->getMessage() . "\n", true);
            }
        } else if ($filesAndDir->isDir() &&
                   $filesAndDir->getFilename() != "." &&
                   $filesAndDir->getFilename() != "..") {
            find_files("${dir}/{$filesAndDir->getFilename()}", $skip_http);
        }
    }
}

find_files(__DIR__, CLI::has_option("--gitlab"));
TestUtils::print(TestUtils::GREEN, "\n=== $passed_tests/$total_tests TESTS PASSED ===\n");