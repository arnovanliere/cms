<?php

global $passed_tests;

include __DIR__ . "/../test_config.php";

class HttpTest extends Client {

    /**
     * HttpTest constructor.
     * @param Database $db       Database to initialize Dashboard with
     * @param string $lang       Language of the dashboard
     * @param string $site_title The title of the site
     * @throws SetupException    If unknown language is provided
     */
    public function __construct(Database $db, string $lang, string $site_title) {
        parent::__construct($db, $lang, $site_title);
    }

    /**
     * Test if all added pages load with HTTP-code 200
     * @param string $alias Alias of the page to load
     */
    public function test_get_page(string $alias) {
        global $passed_tests;
        $url = "localhost/?p=" . $alias;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT,10);
        curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code != 200) {
            TestUtils::print(TestUtils::RED, "Loading page with alias \"$alias\" loaded with status $http_code\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "Loading page with alias \"$alias\" succeeded\n");
            $passed_tests++;
        }
    }

}

shell_exec("./../run.sh");
TestUtils::print_filename(__FILE__);
function load_pages(array $pages) {
    global $db, $language, $site_title;
    $http_test = new HttpTest($db, $language, $site_title);
    foreach ($pages as $page) {
        $http_test->test_get_page($page->alias);
        if (isset($page->sub_pages)) {
            load_pages($page->sub_pages);
        }
    }
}
load_pages($pages_details);