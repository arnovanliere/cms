<?php

global $passed_tests;

include __DIR__ . "/../test_config.php";

class SetupTest extends Setup {

    /**
     * SetupTest constructor.
     * @param Database $db       Database to initialize Dashboard with
     * @param string $lang       Language of the dashboard
     * @param string $site_title The title of the site
     * @throws SetupException    If unknown language is provided
     */
    public function __construct(Database $db, string $lang, string $site_title) {
        parent::__construct($db, $lang, $site_title);
    }

    /**
     * Test add_module(Calendar) and get_calendar()
     * @param Calendar $expected The expected Calendar-module
     */
    public function test_calendar_module(Calendar $expected) {
        $actual = $this->get_calendar();
        if ($actual !== $expected) {
            TestUtils::print(TestUtils::RED, "Actual Calendar-module != expected Calendar-module\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "Actual Calendar-module == expected Calendar-module\n");
        }
    }

    /**
     * Test add_module(Pages) and get_pages()
     * @param Pages $expected The expected Pages-module
     */
    public function test_pages_module(Pages $expected) {
        $actual = $this->get_pages();
        if ($actual !== $expected) {
            TestUtils::print(TestUtils::RED, "Actual Pages-module != expected Pages-module\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "Actual Pages-module == expected Pages-module\n");
        }
    }

    /**
     * Test add_module(Style) and get_style()
     * @param Style $expected The expected Style-module
     */
    public function test_style_module(Style $expected) {
        $actual = $this->get_style();
        if ($actual !== $expected) {
            TestUtils::print(TestUtils::RED, "Actual Style-module != expected Style-module\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "Actual Style-module == expected Style-module\n");
        }
    }

    /**
     * Test add_module(Blog) and get_blog()
     * @param Blog $expected The expected Blog-module
     */
    public function test_blog_module(Blog $expected) {
        $actual = $this->get_blog();
        if ($actual !== $expected) {
            TestUtils::print(TestUtils::RED, "Actual Blog-module != expected Blog-module\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "Actual Blog-module == expected Blog-module\n");
        }
    }

    /**
     * Test add_module(Gallery) and get_gallery)
     * @param Gallery $expected The expected Gallery-module
     */
    public function test_gallery_module(Gallery $expected) {
        $actual = $this->get_gallery();
        if ($actual !== $expected) {
            TestUtils::print(TestUtils::RED, "Actual Gallery-module != expected Gallery-module\n", true);
        } else {
            TestUtils::print(TestUtils::GREEN, "Actual Gallery-module == expected Gallery-module\n");
        }
    }

}

TestUtils::print_filename(__FILE__);
$setup_test = new SetupTest($db, $language, $site_title);
$setup_test->add_module($calendar);
$setup_test->add_module($pages);
$setup_test->add_module($style);
$setup_test->add_module($blog);
$setup_test->add_module($gallery);
$passed_tests++;
$setup_test->test_pages_module($pages);
$passed_tests++;
$setup_test->test_style_module($style);
$passed_tests++;
$setup_test->test_calendar_module($calendar);
$passed_tests++;
$setup_test->test_blog_module($blog);
$passed_tests++;
$setup_test->test_gallery_module($gallery);
$passed_tests++;