<?php

include_once __DIR__ . "/../config/classes.php";

$language = Setup::LANG_EN;
$site_title = "CMS Tests";

// Initialize Database
$db = new Database("localhost", "cms", "root", "password");

// Create Calendar-module
$calendar_options = [
    new CalendarOption("Title", "title", CalendarOption::INPUT_TEXT,
                       [CalendarOption::REQUIRED, CalendarOption::SEARCHABLE]),
    new CalendarOption("Date", "date", CalendarOption::INPUT_DATETIME,
                       [CalendarOption::REQUIRED])
];
$calendar = new Calendar($calendar_options);

// Create Gallery-module
$gallery = new Gallery();

// Create Style-module
$style = new Style();

// Create Blog-module
$blog = new Blog();

// Create Pages-module
$pages_details = [
    new PageDetails("Home", "home"),
    new PageDetails("About", "about"),
    new PageDetails("Blog", "blog", $blog),
    new PageDetails("Events", "events", $calendar),
    new PageDetails("Dropdown", "drop", null, false, false, [
        new PageDetails("Subpage 1", "sub1"),
        new PageDetails("Subpage 2", "sub2")
    ]),
    new PageDetails("Gallery", "gallery", $gallery),
    new PageDetails("Contact", "contact")
];
$pages = new Pages($pages_details, Pages::MODE_MULTIPLE_PAGES);