<?php
global $app;
if ($app != null && isset($_SESSION["user"]["id"])) {
    $app->get_db()->exec("UPDATE " . Database::TABLE_USERS . " SET login_hash = NULL WHERE id = :id",
                         ["id" => $_SESSION["user"]["id"]]);
}
session_start();
session_destroy();
header("Location:/admin/index.php?p=login");