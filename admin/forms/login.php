<?php
/**
 * Log a user in and set hash in $_SESSION and in database
 * @param Dashboard $app The parent to which user tries to login
 * @throws Exception     If random_bytes throws an exception
 */
function login(Dashboard $app) {
    if (!isset($_POST["username"]) && isset($_POST["password"])) {
        return;
    }

    if ($app->verify_user($_POST["username"], $_POST["password"])) { // Valid credentials
        $_SESSION["login"] = true;
        $_SESSION["user"]["login_hash"] = bin2hex(random_bytes(32));
        $app->get_db()->exec("UPDATE " . Database::TABLE_USERS . " SET login_hash = :hash WHERE id = :id",
                             ["hash" => $_SESSION["user"]["login_hash"], "id" => $_SESSION["user"]["id"]]);
        unset($_GET["p"]);
        $app->load_default_module();
        die();
    } else { // Invalid credentials
        $_SESSION["login-success"] = false;
    }
}