<?php
/**
 * @param Dashboard $app The Dashboard from which this is loaded
 * @throws Exception     If random_bytes() throws exception
 */
function reset_pw(Dashboard $app) {
    if (!isset($_POST["send-mail"]) || !isset($_POST["username"])) {
        return;
    }
    $query = "SELECT * FROM " . Database::TABLE_USERS . " WHERE username = :username OR email = :email";
    $params = ["username" => $_POST["username"], "email" => $_POST["username"]];
    $results = $app->get_db()->exec_fetch($query, $params);
    $_SESSION[Dashboard::SESS_RESET_PW_RES] = count($results) == 1;
    if ($_SESSION[Dashboard::SESS_RESET_PW_RES]) { // User found
        $results = $results[0]; // Pick first result, there is only 1 result

        // Create hash and link, save hash in database
        $hash = bin2hex(random_bytes(16));
        $host = isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off" ? "https" : "http";
        $url = "$host://{$_SERVER["SERVER_NAME"]}/admin/?m=".User::ID."&p=".User::RESET_PW_PAGE_TAG."&h=$hash";
        $app->get_db()->exec("UPDATE " . Database::TABLE_USERS . " SET reset_hash = :hash WHERE id = :id",
                             ["hash" => $hash, "id" => $results["id"]]);

        // Send mail
        $mail_to = $results["email"];
        $mail_headers = "MIME-Version: 1.0" . "\r\n";
        $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $mail_headers .= 'From: <no-reply@'.$_SERVER["SERVER_NAME"].'>' . "\r\n";
        $mail_subject = $app->get_string(DASH_RESET_PW_MAIL_SUBJECT);
        $mail_contents = $app->get_string(DASH_RESET_PW_MAIL_CONTENT, [$results["username"], $url]);
        mail($mail_to, $mail_subject, $mail_contents, $mail_headers);

        // Hide all characters before @ (if substring before @ > 2) except first 2
        $explode = explode("@", $results["email"]);
        if (strlen($explode[0]) > 2) {
            $email = substr($results["email"], 0, 2);
            for ($i = 0; $i < strlen($explode[0]) - 2; $i++) {
                $email .= "*";
            }
            $email .= "@{$explode[1]}";
        } else {
            $email = $results["email"];
        }
        $_SESSION[Dashboard::SESS_RESET_PW_MSG] = $app->get_string(DASH_RESET_PW_SUCCESS, $email);
    } else {
        $_SESSION[Dashboard::SESS_RESET_PW_MSG] = $app->get_string(DASH_RESET_PW_ERROR, $_POST["username"]);
    }
}