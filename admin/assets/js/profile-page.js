var changePassword = document.getElementById("change-password");
var changePasswordForm = document.getElementById("change-password-form");
if (changePassword != null) {
    changePassword.addEventListener("click", function () {
        changePasswordForm.classList.toggle("visible");
    });
}
