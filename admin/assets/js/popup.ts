const popups: HTMLCollectionOf<Element> = document.getElementsByClassName("bg-popup");
for (let i = 0; i < popups.length; i++) {
    const close: HTMLCollectionOf<Element> = popups[i].getElementsByClassName("close");
    close[0].addEventListener("click", () => {
        popups[i].classList.add("hidden");
    });
}

document.addEventListener("keyup", (e) => {
    if (e.key === "Escape") { // Hide only on escape
        for (let i = 0; i < popups.length; i++) {
            // Search for first visible popup, hide it and break out of loop
            if (!popups[i].classList.contains("hidden")) {
                popups[i].classList.add("hidden");
                break;
            }
        }
    }
});
