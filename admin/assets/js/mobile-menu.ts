const menuButton: HTMLElement = document.getElementById("mobile-menu");

if (menuButton != null) {
    menuButton.addEventListener("click", () => {
        menuButton.classList.toggle("open");
    });
}