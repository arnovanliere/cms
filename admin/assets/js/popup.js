var popups = document.getElementsByClassName("bg-popup");
var _loop_1 = function (i) {
    var close_1 = popups[i].getElementsByClassName("close");
    close_1[0].addEventListener("click", function () {
        popups[i].classList.add("hidden");
    });
};
for (var i = 0; i < popups.length; i++) {
    _loop_1(i);
}
document.addEventListener("keyup", function (e) {
    if (e.key === "Escape") { // Hide only on escape
        for (var i = 0; i < popups.length; i++) {
            // Search for first visible popup, hide it and break out of loop
            if (!popups[i].classList.contains("hidden")) {
                popups[i].classList.add("hidden");
                break;
            }
        }
    }
});
