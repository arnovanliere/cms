const changePassword: HTMLElement = document.getElementById("change-password");
const changePasswordForm: HTMLElement = document.getElementById("change-password-form");

if (changePassword != null) {
    changePassword.addEventListener("click", () => {
        changePasswordForm.classList.toggle("visible");
    });
}