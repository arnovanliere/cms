<?php

include_once __DIR__ . "/../config/classes.php";
include_once __DIR__ . "/pages/error.php";
include_once __DIR__ . "/pages/login.php";
include_once __DIR__ . "/pages/reset_pw.php";
include_once __DIR__ . "/forms/login.php";
include_once __DIR__ . "/forms/reset_pw.php";

class Dashboard extends Setup {
    /**
     * @var User Module for viewing profile / editing profile
     */
    private User $user;

    /**
     * Constants to save results of forms in ('/forms/reset_pw.php')
     */
    public const SESS_RESET_PW_RES = "reset-pw-res";
    public const SESS_RESET_PW_MSG = "reset-pw-msg";

    /**
     * Dashboard constructor.
     * @param Database $db       Database to connect to
     * @param string $lang       Constant representing language
     * @param string $site_title The title of the site
     * @throws SetupException    If unknown language is provided
     */
    public function __construct(Database $db, string $lang, string $site_title) {
        parent::__construct($db, $lang, $site_title);
        $this->user = new User();
    }

    /**
     * Render the menu for a module with one page
     * @param string $id      The ID of the module
     * @param int $pag_name   The name of the page
     * @param string $tag     The tag of the page
     * @return string         HTML-code of the menu for this module
     * @throws SetupException If an unknown language is set
     */
    private function get_menu_one_page(string $id, int $pag_name, string $tag): string {
        $active_page = (isset($_GET["p"])) ? $_GET["p"] : $this->get_default_module();
        $class = ($active_page == $tag) ? ' class="active"' : "";
        return "<li$class><a href='/admin/?m=$id&p=$tag'>{$this->get_string($pag_name)}</a></li>";
    }

    /**
     * Render the menu for a module with multiple pages
     * @param string $id      The ID of the module
     * @param int $mod_name   The name of the module
     * @param array $pages    Array with pages of the module
     * @return string         HTML-code of the menu for this module
     * @throws SetupException If an unknown language is set
     */
    private function get_menu_multiple_pages(string $id, int $mod_name, array $pages): string {
        $active_page = (isset($_GET["p"])) ? $_GET["p"] : $this->get_default_module();
        $class = "";
        foreach ($pages as $alias => $name) {
            if ($alias == $active_page)
                $class = ' class="active"';
        }
        ob_start();
        echo "<li$class><a href='/admin/?m=$id'>{$this->get_string($mod_name)}</a><ul>";
        foreach ($pages as $alias => $name) {
            $class = ($alias == $active_page) ? ' class="active"' : "";
            echo "<li$class><a href='/admin/?m=$id&p=$alias'>{$this->get_string($name)}</a></li>";
        }
        echo "</ul></li>";
        return ob_get_clean();
    }

    /**
     * Call the appropriate function to render the list-item (with possible sublist) with the pages of the module
     * @param Module|null $module The module to render menu for
     * @return void|string        The HTML-code of the menu for this module
     * @throws SetupException     If an unknown language is set
     */
    private function get_menu_module(?Module $module) {
        if ($module == null) return;
        $id = $module->get_mod_id();
        $name = $module->get_mod_name();
        $def_page_name = $module->get_mod_pages()[$module->get_mod_def_page()];
        $pages = $module->get_mod_pages();
        if (count($module->get_mod_pages()) == 1)
            return $this->get_menu_one_page($id, $def_page_name, $module->get_mod_def_page());
        return $this->get_menu_multiple_pages($id, $name, $pages);
    }

    /**
     * Retrieve the menu for the CMS
     * @return string         HTML-code of the menu
     * @throws SetupException If unknown language is set
     */
    public function get_menu(): string {
        $tag = (isset($_GET["p"])) ? $_GET["p"] : $this->get_default_module();
        ob_start();
        echo "<div id='mobile-menu'><span></span><span></span><span></span></div><nav><ul>";
        echo $this->get_menu_module($this->get_calendar());
        echo $this->get_menu_module($this->get_pages());
        echo $this->get_menu_module($this->get_gallery());
        echo $this->get_menu_module($this->get_style());
        echo $this->get_menu_module($this->get_blog());
        echo "<li><a href='/admin/forms/logout.php'>{$this->get_string(ADMIN_LOGOUT)}</a>";
        echo "<ul><li";
        if ($tag == User::ADD_USER_PAGE_TAG) echo ' class="active"';
        echo "><a href='/admin/?m=".User::ID."&p=".User::ADD_USER_PAGE_TAG."'>
                {$this->get_string(USER_PAG_NAME_ADD_USER)}</a></li><li";
        if ($tag == User::PROFILE_PAGE_TAG) echo ' class="active"';
        echo "><a href='/admin/?m=".User::ID."&p=".User::PROFILE_PAGE_TAG."'>
                {$this->get_string(USER_PAG_NAME_PROFILE_MENU)}<br>
                <span id='username-menu'>@{$this->get_logged_in_user()->username}</span></a>
              </li></ul></li></nav>";
        return ob_get_clean();
    }

    /**
     * Load the module corresponding with 'id'
     * @param string $id      The id of the module to load
     * @throws SetupException If unknown language is set
     * @throws Exception      If popup throws exception
     */
    public function load_module(string $id) {
        switch ($id) {
            case Blog::ID: {
                if ($this->get_blog() != null) { $this->get_blog()->load($this); break; }
            }
            case Calendar::ID: {
                if ($this->get_calendar() != null) { $this->get_calendar()->load($this); break; }
            }
            case Pages::ID: {
                if ($this->get_pages() != null) { $this->get_pages()->load($this); break; }
            }
            case Gallery::ID: {
                if ($this->get_gallery() != null) { $this->get_gallery()->load($this); break; }
            }
            case Style::ID: {
                if ($this->get_style() != null) { $this->get_style()->load($this); break; }
            }
            case User::ID: {
                $this->user->load($this);
                break;
            }
            default: echo get_error_page($this);
        }
    }

    /**
     * Check if SESSION-variable is set and if hash in SESSION is the same as in database
     * @throws SetupException If unknown language is set
     */
    public function require_login() {
        if (!isset($_SESSION["login"]) || !$_SESSION["login"] || !isset($_SESSION["user"])) {
            echo get_login_page($this);
            die();
        }
        $res = $this->get_db()->exec_fetch("SELECT * FROM " . Database::TABLE_USERS . " WHERE id = :id",
            ["id" => $_SESSION["user"]["id"]]);
        if ($_SESSION["user"]["login-hash"] != $res["login-hash"]) {
            $this->get_db()->exec("UPDATE " . Database::TABLE_USERS . " SET login-hash = NULL WHERE id = :id",
                ["id" => $_SESSION["user"]["id"]]);
            echo get_login_page($this);
            die();
        }
    }

    /**
     * Load the default module after logging in
     */
    public function load_default_module() {
        header("Location:/admin/?m=".$this->get_default_module());
        die();
    }

    /**
     * Get the tag of the default page
     * @return string Tag of the default page
     */
    protected function get_default_module(): string {
        if ($this->get_calendar() != null) {
            return Calendar::ID;
        } else if ($this->get_pages() != null) {
            return Pages::ID;
        } else if ($this->get_gallery() != null) {
            return Gallery::ID;
        } else if ($this->get_style() != null) {
            return Style::ID;
        }
        return User::ID;
    }

    /**
     * Get the default header for all Dashboard-pages
     * @return string The standard header
     */
    public static function get_header(): string {
        ob_start(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://kit.fontawesome.com/6dfbda6995.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@200;400;500;900&display=swap">
        <link rel="stylesheet" href="/admin/assets/css/main.css">
        <link rel="stylesheet" href="/admin/assets/css/module-calendar.css">
        <link rel="stylesheet" href="/admin/assets/css/module-gallery.css">
        <link rel="stylesheet" href="/admin/assets/css/module-pages.css">
        <link rel="stylesheet" href="/admin/assets/css/module-user.css">
        <link rel="stylesheet" href="/admin/assets/css/module-blog.css">
        <link rel="stylesheet" href="/admin/assets/css/responsive.css">
        <script src="/admin/assets/js/popup.min.js" defer></script>
        <script src="/admin/assets/js/profile-page.min.js" defer></script>
        <script src="/admin/assets/js/mobile-menu.min.js" defer></script>
        <?php return ob_get_clean();
    }

    /**
     * Verify a user
     * @param string $username Username of the user to verify
     * @param string $password Password of the user to verify
     * @return bool            Whether user is legit or not
     */
    public function verify_user(string $username, string $password): bool {
        $results = $this->get_db()->exec_fetch("SELECT * FROM ".Database::TABLE_USERS." WHERE username = :username",
            ["username" => $username]);
        foreach ($results as $result) {
            if (password_verify($password, $result["password"])) {
                $_SESSION["user"] = $result;
                return true;
            }
        }
        return false;
    }

    /**
     * Get user-details of the currently logged in user
     * @return UserDetails|null 'null' if no user is logged in, otherwise instance of UserDetails
     */
    public function get_logged_in_user(): ?UserDetails {
        if (isset($_SESSION["user"])) {
            $user = $_SESSION["user"];
            return new UserDetails($user["username"], $user["password"], $user["email"]);
        }
        return null;
    }

}