<?php
/**
 * Render Login-page
 * @param Dashboard $app  The app to which this is connected
 * @return string         HTML-code of the page
 * @throws SetupException If unknown language is set
 * @throws Exception      If random_bytes throws exception
 */
function get_login_page(Dashboard $app): string {
    if (isset($_POST["login"])) login($app);
    if (isset($_POST["recover-pw"]) || isset($_POST["send-mail"]))
        { echo get_recover_password_page($app); die(); }
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(DASH_PAG_NAME_LOGIN) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
        <div id="login-form">
            <h1><?php echo $app->get_string(DASH_PAG_NAME_LOGIN) ?></h1>
            <form method="post">
                <label for="username"><?php echo $app->get_string(ADMIN_USERNAME) ?></label>
                <input type="text" name="username" id="username" autofocus>
                <label for="password"><?php echo $app->get_string(ADMIN_PASSWORD) ?></label>
                <input type="password" name="password" id="password">
                <button type="submit" name="login"><?php echo $app->get_string(ADMIN_BTN_LOGIN) ?></button>
                <button type="submit" name="recover-pw">Password Forgotten</button>
                <?php
                if (isset($_SESSION["login-success"])) {
                    echo "<div class='error'>{$app->get_string(ADMIN_LOGIN_ERROR)}</div>";
                }
                ?>
            </form>
        </div>
    </body>
    </html>
    <?php return ob_get_clean();
}