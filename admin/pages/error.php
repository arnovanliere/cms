<?php
/**
 * Render the error-page
 * @param Dashboard $app  Dashboard instance from which this page is loaded
 * @return string         HTML-code of the page
 * @throws SetupException If unknown language is set
 */
function get_error_page(Dashboard $app): string {
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(DASH_PAG_NAME_ERROR) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
        <div class="page-content">
            <h1><?php echo $app->get_string(DASH_PAG_NAME_ERROR) ?></h1>
        </div>
    </body>
    </html>
    <?php return ob_get_clean();
}