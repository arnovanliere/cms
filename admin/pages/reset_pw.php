<?php
/**
 * Render "Recover Password"-page
 * @param Dashboard $app  The app to which this is connected
 * @return string         HTML-code of the page
 * @throws SetupException If unknown language is set
 * @throws Exception      If Popup throws exception
 */
function get_recover_password_page(Dashboard $app): string {
    if (isset($_POST["send-mail"])) reset_pw($app);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(DASH_PAG_NAME_RECOVER_PW) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
    <div id="login-form">
        <h1><?php echo $app->get_string(DASH_PAG_NAME_RECOVER_PW) ?></h1>
        <form method="post">
            <label for="username"><?php echo $app->get_string(DASH_INPUT_USERNAME_EMAIL) ?></label>
            <input type="text" name="username" id="username" required autofocus>
            <button type="submit" name="send-mail"><?php echo $app->get_string(DASH_BTN_RESET_PW) ?></button>
        </form>
    </div>
    <?php
    if (isset($_SESSION[Dashboard::SESS_RESET_PW_RES])) {
        if ($_SESSION[Dashboard::SESS_RESET_PW_RES]) {
            Popup::show_popup($app, Popup::SUCCESS, $_SESSION[Dashboard::SESS_RESET_PW_MSG]);
        } else {
            Popup::show_popup($app, Popup::ERROR,
                      "<b>{$app->get_string(ERROR_MSG)}</b>
                               {$_SESSION[Dashboard::SESS_RESET_PW_MSG]}");
        }
        unset($_SESSION[Dashboard::SESS_RESET_PW_RES]);
        unset($_SESSION[Dashboard::SESS_RESET_PW_MSG]);
    }
    ?>
    </body>
    </html>
    <?php return ob_get_clean();
}