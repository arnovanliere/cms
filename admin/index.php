<?php

include_once __DIR__ . "/../config/config.php";

// Create Dashboard and add modules
$dashboard = new Dashboard($db, $language, $site_title);
$dashboard->add_module($blog);
$dashboard->add_module($calendar);
$dashboard->add_module($style);
$dashboard->add_module($pages);

if (!isset($_GET["m"])) {
	$dashboard->load_default_module();
	die();
}

$dashboard->load_module($_GET["m"]);