<?php

session_start();

include_once __DIR__ . "/classes.php";

$language = Setup::LANG_EN;
$site_title = "Test Environment CMS";

// Initialize the Database
$db = new Database("localhost", "cms", "root", "password");

// Create Calendar-module
$calendar_options = [
	new CalendarOption("Title", "title", CalendarOption::INPUT_TEXT, 
		[CalendarOption::REQUIRED, CalendarOption::SEARCHABLE]),
	new CalendarOption("Date", "date", CalendarOption::INPUT_DATETIME, 
		[CalendarOption::REQUIRED, CalendarOption::FILTERABLE])
];
$calendar = new Calendar($calendar_options);
$calendar->add_side_module();
$calendar->init($db);

// Create Style-module
$style = new Style();
$style->init($db);

// Create Blog-module
$blog = new Blog();

// Create Pages-module and add modules to pages
$pages_details = [
	new PageDetails("Home", "home"),
	new PageDetails("About", "about"),
	new PageDetails("Blog", "blog"),
	new PageDetails("Events", "events"),
	new PageDetails("Dropdown", "drop", null, false, false, [
		new PageDetails("Subpage 1", "sub1"),
		new PageDetails("Subpage2", "sub2")
	]),
	new PageDetails("Gallery", "gallery"),
	new PageDetails("Contact", "contact")
];
$pages = new Pages($pages_details, Pages::MODE_MULTIPLE_PAGES);
$pages->init($db);