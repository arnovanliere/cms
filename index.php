<?php

include_once __DIR__ . "/config/config.php";

// Create Client and add modules
$client = new Client($db, $language, $site_title);
$client->add_module($blog);
$client->add_module($calendar);
$client->add_module($style);
$client->add_module($pages);

if (!isset($_GET["p"])) {
	$client->load_default_page();
	die();
}

$client->load_page($_GET["p"]);