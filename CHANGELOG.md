# Changelog
All notable changes to this project will be documented in this file.

## [1.1.0] - 2020-08-03
### Added
- Setup-tool 

## [1.0.0] - 2020-07-18
- Initial release


[1.1.0]: https://gitlab.com/arnovanliere/cms/-/tags/v1.1.0
[1.0.0]: https://gitlab.com/arnovanliere/cms/-/tags/v1.0.0
