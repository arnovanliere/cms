#!/bin/bash

BUILD_DIR="build"

DIR_ADMIN="admin"
DIR_CLIENT="client"
DIR_CONFIG="config"
DIR_MODULES="modules"
DIR_UTILS="utils"
DIR_STRINGS="strings"
DIR_UI="ui"
FILE_CLIENT="index.php"

TO_COPY=("$DIR_ADMIN" \
         "$DIR_CLIENT" \
         "$DIR_CONFIG" \
         "$DIR_MODULES" \
         "$DIR_UTILS/$DIR_STRINGS" \
         "$DIR_UTILS/$DIR_UI" \
         "$FILE_CLIENT")

# Print a colored message.
#   param 1  Color ("green" / "red" / "neutral" / "white").
#   param 2  Message to print.
function print() {
    if [ "$1" == "green" ]; then
        echo -e "\x1b[32m===     $2     ===\x1b[0m"
    elif [ "$1" == "red" ]; then
        echo -e "\x1b[31m$2\x1b[0m"
    elif [ "$1" == "neutral" ]; then
        echo -e "\x1b[93m===     $2     ===\x1b[0m"
    elif [ "$1" == "white" ]; then
        echo "$2"
    fi
}

# Print a message that the script failed to
# enter a directory and exit the script.
#   param 1  Name of the directory which the script failed to enter
function fail_enter_dir() {
    print "red" "FAILED TO ENTER $1"
    exit 1
}


# Compile TypeScript
print "neutral" "Compiling TypeScript"
./compile.sh
print "green" "TypeScript compiled"
printf "\n\n"

# Move to /cms
cd ..

# Create build-directory if it doesn't exist yet
if [ ! -d $BUILD_DIR ]; then
    mkdir $BUILD_DIR
    print "green" "'build' created"
    printf "\n\n"
fi

# Remove old build
(
    print "neutral" "Removing old build"
    cd $BUILD_DIR || fail_enter_dir $BUILD_DIR
    rm -rf ./*
    print "green" "Old build removed"
    printf "\n\n"
)

# Copy all elements to the build-directory
print "neutral" "Copying build-files"
for i in ${TO_COPY[*]}; do
    print "white" "Copy $i"
    cp -rf "$i" $BUILD_DIR
done
print "green" "Build-files copied"

# Move some files to a subdirectory
if [ ! -d "$BUILD_DIR/utils" ]; then
    (
        cd $BUILD_DIR || fail_enter_dir $BUILD_DIR
        mkdir $DIR_UTILS
    )
fi


cd $BUILD_DIR || fail_enter_dir $BUILD_DIR

# Remove TypeScript
find . -type f -name "*.ts" -delete
# Rename '*.min.js' to '*.minified'
find . -name "*.min.js" -exec sh -c 'mv "$1" "${1%.min.js}.minified"' _ {} \;
# Remove '*.js'
find . -type f -name "*.js" -delete
# Rename '*.minified' to '*.min.js'
find . -name "*.minified" -exec sh -c 'mv "$1" "${1%.minified}.min.js"' _ {} \;

mv $DIR_STRINGS $DIR_UTILS
mv $DIR_UI $DIR_UTILS
