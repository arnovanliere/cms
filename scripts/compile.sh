#!/bin/bash

cd ..

if [[ ${#} == 1 ]]; then
    source=${1}
    # Delete old generated file
    find . -wholename "${source}" -delete

    # Compile TypeScript to JavaScript
    echo -e "\x1b[93m=== COMPILE TypeScript TO JavaScript ===\x1b[0m"
    echo "Compiling ${source}";
    tsc "${source}";

    # Minify JavaScript
    echo
    echo -e "\x1b[93m=== MINIFY JavaScript ===\x1b[0m"
    echo "Minify ${source/.ts/.js}";
    input="${source/.ts/.js}";
    output="${input/.js/.min.js}";
    java -jar scripts/compiler.jar --js "${input}" --js_output_file "${output}"
else
    # Delete previously generated files
    find . -name '*.min.js' -delete

    echo -e "\x1b[93m=== COMPILE TypeScript TO JavaScript ===\x1b[0m"
    # Compile .ts files to .js
    while read -r line; do
        # Remove ./
        source="${line/.\//''}"
        echo "Compiling ${source}"
        tsc "${source}"
    done < <(find . -type f -name "*.ts")

    echo
    echo -e "\x1b[93m=== MINIFY JavaScript ===\x1b[0m"
    # Minify .js files to .min.js
    while read -r line; do
        # Remove ./
        source="${line/.\//''}"
        # Replace .js with .min.js for the output
        output="${source/.js/.min.js}"
        echo "Minify ${source}"
        java -jar scripts/compiler.jar --js "${source}" --js_output_file "${output}"
    done < <(find . -type f -name "*.js")
fi
