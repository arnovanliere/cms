#!/bin/bash

if [[ ${#} -ge 1 ]]; then
	  tail -n"${1}" /var/log/apache2/error.log
else
    tail -n1 /var/log/apache2/error.log
fi