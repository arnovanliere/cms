#!/bin/bash

GREEN='\033[0;32m'
NC='\033[0m'

apache_green() {
    echo -e "${GREEN}apache2 ${1}${NC}"
}

mysql_green() {
    echo -e "${GREEN}mysql ${1}${NC}"
}

if [[ ${#} == 2 ]] && [[ ${2} == "--update" ]]; then
    sudo apt-get -y install apache2
    sudo apt-get -y upgrade apache2
    apache_green "installed/upgraded"
    sudo apt-get -y install mysql-server
    sudo apt-get -y upgrade mysql-server
    mysql_green "installed/upgraded"
fi

if [[ ${#} == 0 ]] || [[ ${1} == "start" ]]; then
    sudo service apache2 start
    apache_green "started"
    sudo service mysql start
    mysql_green "started"
elif [[ ${1} == "stop" ]]; then
    sudo service apache2 stop
    apache_green "stopped"
    if [[ ${#} == 2 ]] && [[ ${2} == "--hard" ]]; then
        sudo killall -9 mysqld mysqld_safe
    else
        sudo service mysql stop
    fi
    mysql_green "stopped"
elif [[ ${1} == "restart" ]]; then
    sudo service apache2 restart
    apache_green "restarted"
    sudo service mysql restart
    mysql_green "restarted"
fi