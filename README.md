# CMS
<div style="text-align: left">
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/arnovanliere/cms/master)
![License](https://img.shields.io/badge/license-MIT-green)
</div>
This is a very basic CMS with different modules for, 
among other things, generating calendars and editing text-pages.<br>
It contains examples for the admin-side as well as the client-side.

## Setup
If you want to use this CMS, you can do it as follows:
1. Clone this Git-repo `git clone git@gitlab.com:arnovanliere/cms.git`
2. Manual
   1. Setup correct credentials for the database (line 6 in `/setup/config.php`)<br>
      (no need for setting up tables, that's all handled by the CMS)
   2. Setup correct language in `/config/config.php`<br>
      current options: Dutch (`Setup::LANG_NL`)<br>
      and English (`Setup::LANG_EN`)
   3. Setup the modules to load in `/setup/config.php`
   4. Load the modules in `/index.php` and in `/admin/index.php`
3. Use setup-tool
   1. Start local webserver and navigate to `localhost/setup`
   2. Fill in form and configuration gets saved for you
4. Setup client-side style in `/client/assets/css/*.css`
5. Start a local web server to test it with: `./scripts/run.sh start --update`
6. Navigate to `localhost` in a web browser to view the client-side
7. Navigate to `localhost/admin` to view the admin-side

### Setup on website
To use this on a website, the only folders/files you need to copy are:
- `./admin`
- `./client`
- `./config`
- `./modules`
- `./utils/strings`
- `./utils/ui`
- `./index.php`

The other files/folders are only for local development and testing.<br>
To automate the build, execute `./build.sh` in the `scripts`-directory.

#### **WARNING**
You should make sure you do not copy the `setup`-folder because it contains<br>
a webpage to generate the configuration for you website. Every visitor of your<br>
website could potentially reconfigure your website if you publish that folder.

## Modules
<ul>
    <li>Blog
        <div>Adding posts / editing posts / adding comments</div>
    </li>
    <li>Calendar
        <div>Adding/editing events.</div>
    </li>
    <li>Gallery
        <div>Simple photo gallery.</div>
    </li>
    <li>Pages
        <div>Editing text-pages and assigning modules to pages.</div>
    </li>
    <li>Style
        <div>Editing basic style.</div>
    </li>
</ul>

## File-structure
```
.
├── admin
│   ├── assets          (assets for the dashboard)
│   ├── forms           (forms for the dashboard)
│   ├── pages           (functions to generate the different 
│   │                    pages for admin-side)
│   ├── Dashboard.php   (class to handle admin-side)
│   └── index.php       (example index-page of a dashboard using this CMS)
├── client
│   ├── assets          (assets for client-side)
│   ├── forms           (forms for client-side)
│   ├── pages           (function to generate the different pages 
│   │                    for client-side)
│   └── Client.php      (class to handle client-side)
├── config
│   ├── classes.php
│   └── config.php      (configuration for the example)
├── modules             (the different modules availible with this CMS)
│   ├── base            (base classes for the modules)
│   ├── blog
│   ├── calendar
│   ├── database
│   ├── gallery
│   ├── pages
│   ├── setup           (contains base-class for Dashboard() and Client())
│   ├── style
│   └── user
├── scripts             (some scripts for starting webserver, compiling TypeScript etc.)
├── setup               (contains web-page to generate configuration)
├── tests
│   ├── admin           (tests for the Admin-module)
│   ├── client          (tests for the Client-module)
│   ├── http            (tests for running local webserver and
│   │                    fetching all pages, not ran in pipeline)
│   ├── modules         (tests for all the modules)
│   ├── setup
│   ├── test_config.php (configuration to run the tests against)
│   └── test_suite.php  (file to run all the tests)
├── utils
│   ├── general
│   ├── strings         (files with translations for strings used in the CMS)
│   ├── tests
│   └── ui              (utilities for UI)
├── index.php           (example index-page of the client-side)
└── README.md
```
