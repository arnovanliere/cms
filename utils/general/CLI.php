<?php

class CLI {

    /**
     * Check if a certain option was provided when running the script
     * @param string $option The option to check for
     * @return bool          'true' if it was provided, 'false' otherwise
     */
    public static function has_option(string $option): bool {
        if (php_sapi_name() != 'cli') {
            return false;
        }
        global $argv;
        foreach ($argv as $arg) {
            if ($arg == $option) {
                return true;
            }
        }
        return false;
    }

}