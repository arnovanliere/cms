<?php

class Popup {
    /**
     * Different types of popups
     */
    public const SUCCESS = "success";
    public const ERROR = "error";

    /**
     * Show an popup
     * @param Setup $parent   An instance of either 'Dashboard' or 'Client' to retrieve strings
     * @param string $type    Type (either Popup::SUCCESS or Popup::ERROR)
     * @param string $message Message for in the popup
     * @throws Exception      If an unknown type is specified
     */
    static function show_popup(Setup $parent, string $type, string $message) {
        if ($type != self::SUCCESS && $type != self::ERROR) {
            throw new Exception("Unknown type specified for popup.");
        }
        ob_start(); ?>
        <div class="bg-popup">
            <div class="popup">
                <div class="<?php echo $type ?>">
                    <?php
                    switch ($type) {
                        case self::SUCCESS: echo '<i class="fas fa-check"></i>'; break;
                        case self::ERROR: echo '<i class="fas fa-times"></i>';
                    }
                    ?>
                </div>
                <h2>
                    <?php
                    switch ($type) {
                        case self::SUCCESS: echo $parent->get_string(SUCCESS); break;
                        case self::ERROR: echo $parent->get_string(ERROR);
                    }
                    ?>
                </h2>
                <span><?php echo $message ?></span>
                <div class="close">
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
        <?php echo ob_get_clean();
    }

}