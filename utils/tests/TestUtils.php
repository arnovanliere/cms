<?php

class TestUtils {
    /**
     * Different colors for print()
     */
    public const RED = 0;
    public const GREEN = 1;

    /**
     * Count number pages in 'pages'
     * @param array $pages Array to count the pages in
     * @return int         Number of pages
     */
    public static function count_pages(array $pages): int {
        $nr_pages = 0;
        foreach ($pages as $page) {
            if (isset($page->sub_pages)) {
                $nr_pages += self::count_pages($page->sub_pages);
            }
            $nr_pages++;
        }
        return $nr_pages;
    }

    /**
     * Print a colored message
     * @param int $color       0 for red, 1 for green
     * @param string $message  Message to print
     * @param bool|null $abort Whether script should be aborted
     */
    public static function print(int $color, string $message, ?bool $abort = null) {
        global $passed_tests, $total_tests;
        switch ($color) {
            case self::RED: {
                echo "\x1b[31m" . $message . "\x1b[0m";
                break;
            }
            case self::GREEN: {
                echo "\x1b[32m" . $message . "\x1b[0m";
                break;
            }
        }
        if ($abort) {
            self::print(self::GREEN, "\n=== $passed_tests TEST(S) PASSED ===\n");
            self::print(self::RED, "=== " . ($total_tests - $passed_tests) . " TEST(S) SKIPPED ===\n");
            self::print(self::RED, "=== TESTS FAILED ===\n");
            exit(1);
        }
    }

    /**
     * Print header before running tests
     * @param string $file The file in which the tests are ran
     */
    public static function print_filename(string $file) {
        echo "\n=== RUNNING TESTS IN '" . basename($file) . "' ===\n";
    }

}