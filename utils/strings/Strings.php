<?php

class Strings {

    /**
     * Replace the first occurrence of '%#s' in 'source'
     * @param string $source  The string to search in for '%#s'
     * @param string $replace The string to replace '%#s' with
     * @return string         The result
     */
    public static function str_replace_first(string $source, string $replace) {
        $pos = strpos($source, "%#s");
        if ($pos === false) return $source;
        $explode = explode("%#s", $source);
        $shift = array_shift( $explode );
        $implode = implode("%#s", $explode);
        return $shift.$replace.$implode;
    }

}