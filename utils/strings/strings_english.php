<?php
$str_english = [
    ERROR_MSG                       => "Error: ",
    SUCCESS                         => "Success",
    ERROR                           => "Error",
    ADMIN_USERNAME                  => "Username",
    ADMIN_PASSWORD                  => "Password",
    ADMIN_BTN_LOGIN                 => "Login",
    ADMIN_LOGOUT                    => "Logout",
    ADMIN_LOGIN_ERROR               => "Username/password invalid",
    DASH_PAG_NAME_LOGIN             => "Dashboard - Login",
    DASH_PAG_NAME_ERROR             => "Error",
    DASH_PAG_NAME_RECOVER_PW        => "Recover Password",
    DASH_BTN_RESET_PW               => "Reset password",
    DASH_INPUT_USERNAME_EMAIL       => "Email/Username",
    DASH_RESET_PW_MAIL_SUBJECT      => "Password Reset",
    DASH_RESET_PW_MAIL_CONTENT      => "Dear %#s,<br>You have requested a password reset. To reset your password you " .
                                       "can visit <a href='%#s'>this url</a>. " .
                                       "If you have not request this password reset, it is recommended to change " .
                                       "your password as fast as possible.<br><br>" .
                                       "Kind Regards,<br>The Webmaster<br><br>PS: You cannot reply to this email.",
    DASH_RESET_PW_SUCCESS           => "An email is sent to %#s with an URL to reset your password.",
    DASH_RESET_PW_ERROR             => "No account with username or email \"%#s\" is found.",
    BLOG_MOD_NAME                   => "Blog",
    BLOG_PAG_NAME_ADD_POST          => "Add Post",
    BLOG_PAG_NAME_ADDED_POSTS       => "Added Posts",
    BLOG_PAG_NAME_COMMENTS          => "Comments",
    BLOG_MSG_ADD_POST_SUCCESS       => "Post successfully added",
    BLOG_MSG_ADD_POST_ERROR         => "Something went wrong when trying to add your post.",
    BLOG_MSG_DELETE_POST_SUCCESS    => "Post successfully deleted",
    BLOG_MSG_DELETE_POST_ERROR      => "Something went wrong when trying to delete your post.",
    BLOG_MSG_UPDATE_POST_SUCCESS    => "Post successfully updated",
    BLOG_MSG_UPDATE_POST_ERROR      => "Something went wrong when trying to update your post.",
    BLOG_MSG_NO_POSTS_ADDED         => "No posts added yet",
    BLOG_MSG_NO_COMMENTS_ON_POST    => "No comments on this post",
    BLOG_MSG_DEL_COMMENT_SUCCESS    => "Comment successfully deleted.",
    BLOG_MSG_DEL_COMMENT_ERROR      => "Something went wrong when deleting a comment.",
    BLOG_MSG_BLOG_PAGE              => "Blog is placed on this page. Go to the page of the Blog-module to submit changes.",
    BLOG_INPUT_TITLE                => "Title*",
    BLOG_INPUT_POST                 => "Post*",
    BLOG_INPUT_COMMENTS             => "Comments enabled",
    BLOG_BTN_ADD_POST               => "Add Post",
    BLOG_BTN_SAVE_CHANGES           => "Save changes",
    BLOG_TITLE_POST                 => "Post: ",
    BLOG_COMMENTS_DISABLED          => "Comments are disabled",
    BLOG_NO_COMMENTS_YET            => "No comments yet",
    BLOG_NR_COMMENTS                => "%#s comments",
    BLOG_POST_NOT_FOUND             => "Post not found",
    CAL_MOD_NAME                    => "Calendar",
    CAL_PAG_NAME_ADD_EVENT          => "Add Event",
    CAL_PAG_NAME_EDIT_EVENT         => "Edit Event",
    CAL_PAG_NAME_ADDED_EVENTS       => "Added Events",
    CAL_BTN_ADD_EVENT               => "Add event",
    CAL_BTN_EDIT_EVENT              => "Save changes",
    CAL_BTN_DELETE_EVENT            => "Delete",
    CAL_BTN_SEARCH                  => "Search",
    CAL_BTN_RESET                   => "Reset",
    CAL_BTN_FILTER                  => "Filter",
    CAL_TITLE_DELETE_EVENT          => "Delete",
    CAL_MSG_ADD_SUCCESS             => "Event added",
    CAL_MSG_ADD_ERROR               => "Something went wrong when adding the event",
    CAL_MSG_EDIT_SUCCESS            => "Changes to the event saved successfully",
    CAL_MSG_EDIT_ERROR              => "Something went wrong when saving changes to the event",
    CAL_MSG_DELETE_SUCCESS          => "Event deleted successfully",
    CAL_MSG_DELETE_ERROR            => "Something went wrong when deleting the event",
    CAL_MSG_NO_EVENTS_FOUND         => "No events found",
    CAL_MSG_CAL_PAGE                => "Calendar is placed on this page. Go to the page of the Calendar-module to submit changes.",
    CAL_MSG_NO_EVENTS_FOUND_SEARCH  => "No events found for search: ",
    CAL_EDIT_CONCERNING             => "Edit data concerning",
    GAL_MOD_NAME                    => "Gallery",
    GAL_PAG_NAME_ADD_PHOTOS         => "Add Photo",
    GAL_PAG_NAME_ADDED_PHOTOS       => "Uploaded Photos",
    GAL_PAG_NAME_CATEGORIES         => "Categories",
    GAL_BTN_RESET                   => "Reset",
    GAL_BTN_UPLOAD                  => "Upload",
    GAL_BTN_DELETE_CATEGORY         => "Delete",
    GAL_BTN_CREATE_CATEGORY         => "Create category",
    GAL_BTN_FILTER                  => "Filter",
    GAL_MSG_UPLOAD_SUCCESS          => "Photo is uploaded.",
    GAL_MSG_UPLOAD_ERROR            => "Something went wrong when uploading.",
    GAL_MSG_ERR_EXT                 => "File doesn't have the right extension.",
    GAL_MSG_ERR_SIZE                => "File cannot be bigger than 2MB.",
    GAL_MSG_ERR_GENERAL             => "A general error occurred.",
    GAL_MSG_NO_PHOTOS_UPLOADED      => "No photos are uploaded.",
    GAL_MSG_GAL_PAGE                => "Gallery is placed on this page. Go to the page of the Gallery-module to submit changes.",
    GAL_MSG_ERROR_ADDING_PHOTOS     => "You need to add at least 1 category before you can start uploading photos.",
    GAL_MSG_DELETE_PHOTO_SUCCESS    => "Photo successfully deleted.",
    GAL_MSG_DELETE_PHOTO_ERROR      => "Something went wrong when trying to delete the photo.",
    GAL_TITLE_DELETE_CATEGORY       => "Delete",
    GAL_TITLE_CATEGORY_NAME         => "Category",
    GAL_MSG_ADD_CATEGORY_SUCCESS    => "Category added",
    GAL_MSG_ADD_CATEGORY_ERROR      => "Category not added. Are you sure this category doesn't exist yet?",
    GAL_MSG_NO_CATEGORIES_FOUND     => "No categories added.",
    GAL_MSG_DELETE_CATEGORY_SUCCESS => "Category deleted.",
    GAL_MSG_DELETE_CATEGORY_ERROR   => "Something went wrong when deleting the category. Are you sure there are no photos in this category?",
    PAG_MOD_NAME                    => "Pages",
    PAG_PAG_MSG_CHANGES_SUCCESS     => "Changes saved successfully",
    PAG_PAG_MSG_CHANGES_ERROR       => "Something went wrong",
    PAG_PAG_NAME_EDIT_PAGE          => "Edit Page",
    PAG_BTN_SAVE_PAGE               => "Save",
    STYLE_MOD_NAME                  => "Style",
    STYLE_PAG_NAME_STYLE            => "Style",
    STYLE_PRIMARY_COLOR             => "Primary color",
    STYLE_PRIMARY_COLOR_DARK        => "Primary color dark",
    STYLE_ACCENT_COLOR              => "Accent color",
    STYLE_BACKGROUND_COLOR          => "Background color",
    STYLE_DEFAULT_FONT              => "Default font",
    STYLE_TITLE_FONT                => "Font for titles",
    STYLE_BTN_SAVE_CHANGES          => "Save",
    STYLE_MSG_CHANGES_SUCCESS       => "Changes saved successfully",
    STYLE_MSG_CHANGES_ERROR         => "Something went wrong when saving your changes",
    USER_MOD_NAME                   => "Profile",
    USER_PAG_NAME_PROFILE           => "Profile",
    USER_PAG_NAME_PROFILE_MENU      => "Edit Profile",
    USER_PAG_NAME_ADD_USER          => "Add User",
    USER_INPUT_OLD_PW               => "Old password",
    USER_INPUT_NEW_PW               => "New password",
    USER_INPUT_CONF_NEW_PW          => "Confirm new password",
    USER_INPUT_USERNAME             => "Username",
    USER_INPUT_EMAIL                => "Email",
    USER_INPUT_PASSWORD             => "Password",
    USER_INPUT_CONF_PASSWORD        => "Confirm password",
    USER_TITLE_USERNAME             => "Username",
    USER_TITLE_EMAIL                => "Email",
    USER_TITLE_PASSWORD             => "Password",
    USER_BTN_CHANGE_PW              => "Change password",
    USER_BTN_ADD_USER               => "Add User",
    USER_UPDATE_PW_NON_MATCH        => "Passwords do not match",
    USER_UPDATE_PW_TOO_SHORT        => "Password has to be at least 8 characters",
    USER_UPDATE_PW_OLD_PW_ERR       => "Old password is incorrect",
    USER_UPDATE_PW_GENERAL_ERR      => "A general error occurred",
    USER_UPDATE_PW_SUCCESS          => "Password updated successfully",
    USER_UPDATE_PW_ERROR            => "Something went wrong when trying to update your password",
    USER_RESET_PW_SUCCESS           => "Password successfully reset. You can now login with your new password.",
    USER_RESET_PW_ERROR             => "Something went wrong when trying to reset your password",
    USER_ADD_ERROR_USERNAME         => "This username is already in use.",
    USER_ADD_ERROR_EMAIL            => "This email is already in use.",
    USER_ADD_ERROR_PW_MATCH         => "Passwords do not match.",
    USER_ADD_ERROR_PW_LENGTH        => "Password has to be at least 8 characters.",
    USER_ADD_ERROR_GENERAL          => "Something went wrong when trying to add the user-account."
];