<?php
// General
const ERROR_MSG = 45;
const SUCCESS   = 46;
const ERROR     = 47;

// Admin
const ADMIN_USERNAME             = 0;
const ADMIN_PASSWORD             = 1;
const ADMIN_BTN_LOGIN            = 2;
CONST ADMIN_LOGOUT               = 3;
const ADMIN_LOGIN_ERROR          = 4;
const DASH_PAG_NAME_LOGIN        = 5;
const DASH_PAG_NAME_ERROR        = 6;
const DASH_PAG_NAME_RECOVER_PW   = 79;
const DASH_BTN_RESET_PW          = 80;
const DASH_INPUT_USERNAME_EMAIL  = 81;
const DASH_RESET_PW_MAIL_SUBJECT = 85;
const DASH_RESET_PW_MAIL_CONTENT = 86;
const DASH_RESET_PW_SUCCESS      = 87;
const DASH_RESET_PW_ERROR        = 88;

// Blog
const BLOG_MOD_NAME                = 102;
const BLOG_PAG_NAME_ADD_POST       = 32;
const BLOG_PAG_NAME_ADDED_POSTS    = 101;
const BLOG_PAG_NAME_COMMENTS       = 122;
const BLOG_MSG_ADD_POST_SUCCESS    = 110;
const BLOG_MSG_ADD_POST_ERROR      = 111;
const BLOG_MSG_DELETE_POST_SUCCESS = 118;
const BLOG_MSG_DELETE_POST_ERROR   = 119;
const BLOG_MSG_UPDATE_POST_SUCCESS = 120;
const BLOG_MSG_UPDATE_POST_ERROR   = 121;
const BLOG_MSG_NO_POSTS_ADDED      = 117;
const BLOG_MSG_NO_COMMENTS_ON_POST = 123;
const BLOG_MSG_DEL_COMMENT_SUCCESS = 125;
const BLOG_MSG_DEL_COMMENT_ERROR   = 126;
const BLOG_MSG_BLOG_PAGE           = 127;
const BLOG_INPUT_TITLE             = 112;
const BLOG_INPUT_POST              = 113;
const BLOG_INPUT_COMMENTS          = 114;
const BLOG_BTN_ADD_POST            = 115;
const BLOG_BTN_SAVE_CHANGES        = 116;
const BLOG_TITLE_POST              = 124;
const BLOG_COMMENTS_DISABLED       = 138;
const BLOG_NO_COMMENTS_YET         = 139;
const BLOG_NR_COMMENTS             = 140;
const BLOG_POST_NOT_FOUND          = 141;

// Calendar
const CAL_MOD_NAME                   = 7;
const CAL_PAG_NAME_ADD_EVENT         = 8;
const CAL_PAG_NAME_EDIT_EVENT        = 9;
const CAL_PAG_NAME_ADDED_EVENTS      = 27;
const CAL_BTN_ADD_EVENT              = 10;
const CAL_BTN_EDIT_EVENT             = 11;
const CAL_BTN_DELETE_EVENT           = 28;
const CAL_BTN_SEARCH                 = 35;
const CAL_BTN_RESET                  = 37;
const CAL_BTN_FILTER                 = 38;
const CAL_TITLE_DELETE_EVENT         = 29;
const CAL_MSG_ADD_SUCCESS            = 21;
const CAL_MSG_ADD_ERROR              = 22;
const CAL_MSG_EDIT_SUCCESS           = 23;
const CAL_MSG_EDIT_ERROR             = 24;
const CAL_MSG_DELETE_SUCCESS         = 30;
const CAL_MSG_DELETE_ERROR           = 31;
const CAL_EDIT_CONCERNING            = 25;
const CAL_MSG_NO_EVENTS_FOUND        = 26;
const CAL_MSG_NO_EVENTS_FOUND_SEARCH = 36;
const CAL_MSG_CAL_PAGE               = 33;

// Gallery
const GAL_MOD_NAME                    = 41;
const GAL_PAG_NAME_ADD_PHOTOS         = 12;
const GAL_PAG_NAME_ADDED_PHOTOS       = 40;
const GAL_PAG_NAME_CATEGORIES         = 42;
const GAL_BTN_RESET                   = 64;
const GAL_BTN_UPLOAD                  = 39;
const GAL_BTN_DELETE_CATEGORY         = 54;
const GAL_BTN_FILTER                  = 62;
const GAL_BTN_CREATE_CATEGORY         = 56;
const GAL_MSG_UPLOAD_SUCCESS          = 43;
const GAL_MSG_UPLOAD_ERROR            = 44;
const GAL_MSG_ERR_EXT                 = 48;
const GAL_MSG_ERR_SIZE                = 49;
const GAL_MSG_ERR_GENERAL             = 50;
const GAL_MSG_NO_PHOTOS_UPLOADED      = 51;
const GAL_MSG_GAL_PAGE                = 52;
const GAL_TITLE_DELETE_CATEGORY       = 53;
const GAL_TITLE_CATEGORY_NAME         = 55;
const GAL_MSG_ADD_CATEGORY_SUCCESS    = 57;
const GAL_MSG_ADD_CATEGORY_ERROR      = 58;
const GAL_MSG_NO_CATEGORIES_FOUND     = 59;
const GAL_MSG_DELETE_CATEGORY_SUCCESS = 60;
const GAL_MSG_DELETE_CATEGORY_ERROR   = 61;
const GAL_MSG_ERROR_ADDING_PHOTOS     = 63;
const GAL_MSG_DELETE_PHOTO_SUCCESS    = 65;
const GAL_MSG_DELETE_PHOTO_ERROR      = 66;

// Pages
const PAG_MOD_NAME                = 104;
const PAG_PAG_MSG_CHANGES_SUCCESS = 13;
const PAG_PAG_MSG_CHANGES_ERROR   = 14;
const PAG_PAG_NAME_EDIT_PAGE      = 15;
const PAG_BTN_SAVE_PAGE           = 128;

// Style
const STYLE_MOD_NAME            = 106;
const STYLE_PAG_NAME_STYLE      = 16;
const STYLE_PRIMARY_COLOR       = 129;
const STYLE_PRIMARY_COLOR_DARK  = 130;
const STYLE_ACCENT_COLOR        = 131;
const STYLE_BACKGROUND_COLOR    = 132;
const STYLE_DEFAULT_FONT        = 133;
const STYLE_TITLE_FONT          = 134;
const STYLE_BTN_SAVE_CHANGES    = 135;
const STYLE_MSG_CHANGES_SUCCESS = 136;
const STYLE_MSG_CHANGES_ERROR   = 137;

// User
const USER_MOD_NAME              = 107;
const USER_PAG_NAME_PROFILE      = 19;
const USER_PAG_NAME_ADD_USER     = 90;
const USER_PAG_NAME_PROFILE_MENU = 109;
const USER_INPUT_OLD_PW          = 67;
const USER_INPUT_NEW_PW          = 68;
const USER_INPUT_CONF_NEW_PW     = 69;
const USER_INPUT_USERNAME        = 91;
const USER_INPUT_EMAIL           = 92;
const USER_INPUT_PASSWORD        = 93;
const USER_INPUT_CONF_PASSWORD   = 94;
const USER_TITLE_USERNAME        = 70;
const USER_TITLE_EMAIL           = 71;
const USER_TITLE_PASSWORD        = 72;
const USER_BTN_CHANGE_PW         = 73;
const USER_BTN_ADD_USER          = 95;
const USER_UPDATE_PW_NON_MATCH   = 74;
const USER_UPDATE_PW_TOO_SHORT   = 75;
const USER_UPDATE_PW_OLD_PW_ERR  = 76;
const USER_UPDATE_PW_GENERAL_ERR = 77;
const USER_UPDATE_PW_SUCCESS     = 78;
const USER_UPDATE_PW_ERROR       = 82;
const USER_RESET_PW_SUCCESS      = 83;
const USER_RESET_PW_ERROR        = 84;
const USER_ADD_ERROR_USERNAME    = 96;
const USER_ADD_ERROR_EMAIL       = 97;
const USER_ADD_ERROR_PW_MATCH    = 98;
const USER_ADD_ERROR_PW_LENGTH   = 99;
const USER_ADD_ERROR_GENERAL     = 100;
