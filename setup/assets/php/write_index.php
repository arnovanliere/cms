<?php

/**
 * Write /admin/index.php and ./index.php
 */


require_once __DIR__ . "/form.php";

$config = "// Create Client and add modules";
$config .= "\n\$client = new Client(\$db, \$language, \$site_title);";
if (isset($_POST["blog-add"])) $config .= "\n\$client->add_module(\$blog);";
if (isset($_POST["cal-add"])) $config .= "\n\$client->add_module(\$calendar);";
if (isset($_POST["gal-add"])) $config .= "\n\$client->add_module(\$gallery);";
if (isset($_POST["style-add"])) $config .= "\n\$client->add_module(\$style);";
$config .= "\n\$client->add_module(\$pages);";

/**
 * Write client-config
 */
$client = "<?php\n\ninclude_once __DIR__ . \"/config/config.php\";\n\n";
$client .= $config;
$client .= "\n\nif (!isset(\$_GET[\"p\"])) {\n\t\$client->load_default_page();";
$client .= "\n\tdie();\n}\n\n\$client->load_page(\$_GET[\"p\"]);";
$res = file_put_contents("../../../index.php", $client);
if ($res === false) {
    res(500, "Something went wrong when saving ./index.php. Please try again.");
}

/**
 * Write admin-config
 */
$admin = "<?php\n\ninclude_once __DIR__ . \"/../config/config.php\";\n\n";
$admin .= $config;
$admin .= "\n\nif (!isset(\$_GET[\"m\"])) {\n\t\$dashboard->load_default_module();";
$admin .= "\n\tdie();\n}\n\n\$dashboard->load_module(\$_GET[\"m\"]);";
$admin = str_replace("Client", "Dashboard", $admin);
$admin = str_replace("client", "dashboard", $admin);
$res = file_put_contents("../../../admin/index.php", $admin);
if ($res === false) {
    res(500, "Something went wrong when saving ./admin/index.php. Please try again.");
}
