<?php

/**
 * Write /config/config.php
 */

require_once __DIR__ . "/form.php";
require_once __DIR__ . "/utils.php";

// Initial includes etc
$file = "<?php\n\nsession_start();\n\ninclude_once __DIR__ . \"/classes.php\";\n\n";

// Language for CMS
$file .= "\$language = Setup::{$_POST["gen-lang"]};\n";

// Site-title
$file .= "\$site_title = \"{$_POST["gen-title"]}\";\n\n";

// Database
$file .= "// Initialize the Database\n";
$file .= "\$db = new Database(\"{$_POST["db-dns"]}\", \"{$_POST["db-name"]}\", ";
$file .= "\"{$_POST["db-username"]}\", \"{$_POST["db-pw"]}\");\n\n";

// Calendar
if (isset($_POST["cal-add"])) {
    $file .= "// Create Calendar-module\n\$calendar_options = [\n";
    for ($i = 0; $i < count($_POST["cal-field-name"]); $i++) { // Add calendar-options
        $file .= "\tnew CalendarOption(\"{$_POST["cal-field-name"][$i]}\", \"{$_POST["cal-col-name"][$i]}\", ";
        switch ($_POST["cal-field-type"][$i]) {
            case "type-text": $file .= "CalendarOption::INPUT_TEXT"; break;
            case "type-date": $file .= "CalendarOption::INPUT_DATE"; break;
            case "type-date-time": $file .= "CalendarOption::INPUT_DATETIME"; break;
            case "type-bool": $file .= "CalendarOption::INPUT_CHECKBOX"; break;
            case "type-int": $file .= "CalendarOption::INPUT_NUMBER"; break;
            case "type-float": $file .= "CalendarOption::INPUT_FLOAT"; break;
        }
        if ($_POST["cal-field-required"][$i] === "yes" ||
            $_POST["cal-field-searchable"][$i] === "yes" ||
            $_POST["cal-field-filterable"][$i] === "yes") {
            $file .= ", \n\t\t[";
            if ($_POST["cal-field-required"][$i] === "yes")
                $file .= "CalendarOption::REQUIRED, ";
            if ($_POST["cal-field-searchable"][$i] === "yes")
                $file .= "CalendarOption::SEARCHABLE, ";
            if ($_POST["cal-field-filterable"][$i] === "yes")
                $file .= "CalendarOption::FILTERABLE, ";
            $file = substr($file, 0, -2);
            $file .= "]";
        }
        $file .= ")";
        if ($i !== count($_POST["cal-field-name"]) - 1) $file .= ",\n";
    }
    // Add calendar-options to calendar
    $file .= "\n];\n\$calendar = new Calendar(\$calendar_options);\n";
    $file .= "\$calendar->add_side_module();\n\$calendar->init(\$db);\n\n";
}

// Gallery
if (isset($_POST["gal-add"])) {
    $file .= "// Create Gallery-module\n\$gallery = new Gallery(";
    $file .= isset($_POST["gal-cat"]) ? "[Gallery::OPT_CATEGORIES]);\n\n" : ");\n\n";
}

// Style
if (isset($_POST["style-add"])) {
    $file .= "// Create Style-module\n\$style = new Style();\n";
    $file .= "\$style->init(\$db);\n\n";
}

// Blog
if (isset($_POST["blog-add"])) {
    $file .= "// Create Blog-module\n\$blog = new Blog();\n\n";
}

// Pages
$file .= "// Create Pages-module and add modules to pages\n\$pages_details = [";
$pages = get_pages_config();
foreach ($pages as $page) {
    add_page($file, $page);
}
$file = substr($file, 0, -1);
$file .= "\n];\n\$pages = new Pages(\$pages_details, Pages::";
if ($_POST["gen-mode"] === "multi-page") $file .= "MODE_MULTIPLE_PAGES);";
if ($_POST["gen-mode"] === "one-page") $file .= "MODE_ONE_PAGE);";
$file .= "\n\$pages->init(\$db);";

// Write the configuration
$res = file_put_contents("../../../config/config.php", $file);
if ($res === false) {
    res(500, "Something went wrong when saving ./config/config.php. Please try again.");
}

// Create admin

$db = new Database($_POST["db-dns"], $_POST["db-name"], $_POST["db-username"], $_POST["db-pw"]);
$create_admin = $db->exec("INSERT INTO " . Database::TABLE_USERS . "(username, email, password) 
                          VALUES(:username, :email, :password)",
    ["username" => $_POST["admin-username"], "email" => $_POST["admin-email"],
        "password" => password_hash($_POST["admin-password"], PASSWORD_DEFAULT)]);
if (!$create_admin) {
    res(500, "Something went wrong when creating the admin-user.");
}