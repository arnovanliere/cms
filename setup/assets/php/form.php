<?php

require __DIR__ . "/../../../config/classes.php";

/**
 * Return a result and stop script.
 * @param int $code            The HTTP status code.
 * @param string|null $message Possible message to return with the code.
 */
function res(int $code, ?string $message) {
    echo json_encode(
        [
            "success" => $code === 200,
            "code" => $code,
            "message" => isset($message) ? $message : null,
        ]
    );
    die();
}

/**
 * Check the presence of keys in $_POST
 * @param array $arr   The array with keys to check
 * @return bool|string False if no entities are defined, a key of 'arr' if an entity
 *                     is not fully defined and true if all entities are fully defined.
 */
function check_arr(array $arr) {
    // Check if at least one (partial) entity is defined
    $set = false;
    foreach ($arr as $key => $value) if (isset($_POST[$key]) && $_POST[$key] !== "") $set = true;
    if (!$set) return false;

    // Check if all keys are set
    foreach ($arr as $key => $value) {
        if (!isset($_POST[$key]) || $_POST[$key] === "" || empty($_POST[$key])) {
            return strval($value);
        } else if (is_array($_POST[$key])) {
            foreach ($_POST[$key] as $item => $val) {
                if (!isset($_POST[$key][$item]) || $_POST[$key][$item] === "" || empty($_POST[$key][$item])) {
                    return strval($value);
                }
            }
        }
    }
    return true;
}

/**
 * Check if all required keys are present in $_POST.
 */
function check_required_keys() {
    $req_keys = [
        "gen-title"      => "General - Title",
        "gen-lang"       => "General - Language",
        "gen-mode"       => "General - Page-mode",
        "admin-username" => "Admin - Username",
        "admin-email"    => "Admin - Email",
        "admin-password" => "Admin - Password",
        "db-dns"         => "Database - DNS",
        "db-name"        => "Database - Name",
        "db-username"    => "Database - Username",
        "db-pw"          => "Database - Password"
    ];
    $err = false;
    $msg = "ERROR: Not all required fields are filled in. The following are missing:\n";
    foreach ($req_keys as $key => $value) {
        if (!isset($_POST[$key]) || $_POST[$key] === "") {
            $err = true;
            $msg .= " - $value\n";
        }
    }

    if ($err) res(400, $msg);
}

/**
 * Check if admin email is valid an if password is long enough.
 */
function check_admin() {
    if (!filter_var($_POST["admin-email"], FILTER_VALIDATE_EMAIL)) {
        res(400, "ERROR Admin: Email is not a valid email-address");
    }
    if (strlen($_POST["admin-password"]) < 8) {
        res(400, "ERROR Admin: Password should be at least 8 characters");
    }
}

/**
 * Check if the required keys are set for Pages.
 */
function check_pages() {
    $keys = [
        "pag-name"    => "Name",
        "pag-tag"     => "Tag",
        "pag-mod"     => "Module"
    ];
    $check = check_arr($keys);
    if ($check === false) res(400, "ERROR Pages: At least one page should be added.");
    if (is_string($check)) res(400, "ERROR Pages: Not all '$check'-values are set.");
    if (count(array_unique($_POST["pag-tag"])) < count($_POST["pag-tag"])) {
        res(400, "ERROR Pages: Not all tags are unique.");
    }
}

/**
 * Check if the required keys are set for Calendar.
 */
function check_calendar() {
    if (!isset($_POST["cal-add"])) return;
    $keys = [
        "cal-field-name"       => "Name",
        "cal-col-name"         => "Column-name",
        "cal-field-type"       => "Type",
        "cal-field-required"   => "Required",
        "cal-field-filterable" => "Filterable",
        "cal-field-searchable" => "Searchable",
    ];
    $check = check_arr($keys);
    if ($check === false) res(400, "ERROR Calendar: At least one field should be added.");
    if (is_string($check)) res(400, "ERROR Calendar: Not all '$check'-values are set.");
}

check_required_keys();
check_admin();
check_pages();
check_calendar();

include_once __DIR__ . "/write_index.php";
include_once __DIR__ . "/write_config.php";

res(200, "Configuration saved and written to\n - ./admin/index.php\n - ./index.php");
