<?php

/**
 * Retrieve an array with all pages and subpages from $_POST.
 * @return array The pages.
 */
function get_pages_config(): array {
    $pages = [];
    for ($i = 0; $i < count($_POST["pag-name"]); $i++) {
        $page = [
            "pag-name" => $_POST["pag-name"][$i],
            "pag-tag" => $_POST["pag-tag"][$i],
            "pag-mod" => $_POST["pag-mod"][$i],
            "pag-sidebar" => $_POST["pag-sidebar"][$i],
            "pag-footer" => $_POST["pag-footer"][$i],
            "pag-subpages" => []
        ];
        while ($i + 1 < count($_POST["pag-name"]) && $_POST["pag-subpage"][$i + 1] === "true") {
            $i++;
            array_push($page["pag-subpages"], [
                "pag-name" => $_POST["pag-name"][$i],
                "pag-tag" => $_POST["pag-tag"][$i],
                "pag-mod" => $_POST["pag-mod"][$i],
                "pag-sidebar" => $_POST["pag-sidebar"][$i],
                "pag-footer" => $_POST["pag-footer"][$i]
            ]);
        }
        array_push($pages, $page);
    }
    return $pages;
}

/**
 * Create a string from an array an stitch the items together with ", ".
 * @param array $pieces The array to create string from.
 * @return string       The array as string.
 */
function stitch(array $pieces): string {
    $str = ""; $len = count($pieces);
    foreach ($pieces as $index => $piece) {
        $str .= $piece;
        if ($index !== $len - 1) $str .= ", ";
    }
    return $str;
}

/**
 * Set the value from module in a PagesDetail.
 * @param string $val  The module-value to set.
 * @param string $file The result after value is set.
 */
function set_mod(string $val, string &$file) {
    $arr = explode(", ", $file);
    $arr[count($arr) - 3] = $val;
    $file = stitch($arr);
}

/**
 * Set the value from sidebar in a PagesDetail.
 * @param string $val  The sidebar-value to set.
 * @param string $file The result after value is set.
 */
function set_sidebar(string $val, string &$file) {
    $arr = explode(", ", $file);
    $arr[count($arr) - 2] = $val;
    $file = stitch($arr);
}

/**
 * Set the value from footer in a PagesDetail.
 * @param string $val  The footer-value to set.
 * @param string $file The result after value is set.
 */
function set_footer(string $val, string &$file) {
    $arr = explode(", ", $file);
    $arr[count($arr) - 1] = $val;
    $file = stitch($arr);
}

/**
 * Set the PageDetails for a specified page and its subpages.
 * @param string $file  The string to write the config to.
 * @param array $page   The page to write config of.
 * @param bool $subpage Whether page to write is subpage or not.
 */
function add_page(string &$file, array $page, bool $subpage = false) {
    if ($subpage) $file .= "\n\t\t";
    else $file .= "\n\t";
    $file .= "new PageDetails(\"{$page["pag-name"]}\", \"{$page["pag-tag"]}\", null, false, false";
    // Add calendar side-module
    if ($page["pag-sidebar"] === "calendar") {
        set_mod("\$calendar->get_side_module()", $file);
    }
    // Add a module
    if ($page["pag-mod"] !== "none" && $page["pag-sidebar"] !== "calendar") {
        $mod = "null";
        switch ($page["pag-mod"]) {
            case "blog": set_mod("\$blog", $file); break;
            case "calendar": set_mod("\$calendar", $file); break;
            case "gallery": set_mod("\$gallery", $file); break;
        }
        set_mod($mod, $file);
    }
    // Add sidebar
    if ($page["pag-sidebar"] === "yes") {
        set_sidebar("yes", $file);
    }
    // Add footer
    if ($page["pag-footer"] === "yes") {
        set_footer("yes", $file);
    }
    if (isset($page["pag-subpages"]) && count($page["pag-subpages"]) !== 0) {
        $file .= ", [";
        foreach ($page["pag-subpages"] as $index => $subpage) {
            add_page($file, $subpage, true);
            if ($index === count($page["pag-subpages"]) - 1)
                $file = substr($file, 0, -1);
        }
        $file .= "\n\t]";
    } else {
        $pieces = explode(", ", $file);
        if ($pieces[count($pieces) - 1] === "false") unset($pieces[count($pieces) - 1]);
        if ($pieces[count($pieces) - 1] === "false") unset($pieces[count($pieces) - 1]);
        if ($pieces[count($pieces) - 1] === "null") unset($pieces[count($pieces) - 1]);
        $file = stitch($pieces);
    }
    $file .= "),";
}
