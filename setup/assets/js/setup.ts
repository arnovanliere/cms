// Values and options for modules dropdown
const options = {
    blog: {
        option: "Blog",
        value: "blog"
    },
    calendar: {
        option: "Calendar",
        value: "calendar"
    },
    gallery: {
        option: "Gallery",
        value: "gallery"
    },
    calendarSideModule: {
        option: "Calendar side-module",
        value: "calendar"
    }
};

// Buttons
const calAddField = document.getElementById("cal-btn-add-field") as HTMLButtonElement;
const pagAddPage = document.getElementById("pag-btn-add-page") as HTMLButtonElement;

// Templates
const calTemplateField = document.getElementById("cal-field") as HTMLTemplateElement;
const pagTemplatePage = document.getElementById("pag-add-page") as HTMLTemplateElement;

// Checkboxes
const getCheckbox = (name: string) => document.querySelector<HTMLInputElement>(`input[type=\"checkbox\"][name=\"${name}\"]`);
const calCheckbox = getCheckbox("cal-add");
const galCheckbox = getCheckbox("gal-add");
const galCatCheckbox = getCheckbox("gal-cat");
const blogCheckbox = getCheckbox("blog-add");

/**
 * Insert an option in a select-element.
 * @param option The option to insert
 * @param select The select-element into which to insert the option.
 */
function insertOption(option: keyof typeof options, select: HTMLSelectElement) {
    const optionToInsert = document.createElement("option");
    optionToInsert.innerHTML = options[option].option;
    optionToInsert.value = options[option].value;
    select.append(optionToInsert);
}

/**
 * Set select-elements with options for modules to add to pages.
 */
function setSelects() {
    const modulesSelect = document.querySelectorAll<HTMLSelectElement>("select[name=\"pag-mod[]\"]");
    const sidebarSelect = document.querySelectorAll<HTMLSelectElement>("select[name=\"pag-sidebar[]\"]");
    modulesSelect.forEach(select => {
        const galOpt = select.querySelector(`option[value=\"${options["gallery"].value}\"]`);
        const calOpt = select.querySelector(`option[value=\"${options["calendar"].value}\"]`);
        const blogOpt = select.querySelector(`option[value=\"${options["blog"].value}\"]`);
        if (galCheckbox.checked && galOpt == null) insertOption("gallery", select);
        else if (!galCheckbox.checked && galOpt != null) galOpt.remove();
        if (calCheckbox.checked && calOpt == null) insertOption("calendar", select);
        else if (!calCheckbox.checked && calOpt != null) calOpt.remove();
        if (blogCheckbox.checked && blogOpt == null) insertOption("blog", select);
        else if (!blogCheckbox.checked && blogOpt != null) blogOpt.remove();
    });
    sidebarSelect.forEach(select => {
        const calSideModuleOpt = select.querySelector(`option[value=\"${options["calendarSideModule"].value}\"]`);
        if (calCheckbox.checked && calSideModuleOpt == null) insertOption("calendarSideModule", select);
        else if (!calCheckbox.checked && calSideModuleOpt != null) calSideModuleOpt.remove();
    });
}

/**
 * Toggle selects and buttons tied to page-mode.
 */
function setMode() {
    const modeSelect = document.querySelector<HTMLSelectElement>("select[name=\"gen-mode\"]");
    const footerSelect = document.querySelectorAll("select[name=\"pag-footer[]\"]");
    const sideBarSelect = document.querySelectorAll("select[name=\"pag-sidebar[]\"]");
    const subPageButtons = document.querySelectorAll<HTMLButtonElement>("button.add-sub-page");
    const onePageMode = modeSelect.value === "one-page";
    const show = (el: HTMLElement) => el.style.display = "flex";
    const hide = (el: HTMLElement) => el.style.display = "none";
    const selectCallback = (select: HTMLElement) => onePageMode ? hide(select.parentElement) : show(select.parentElement)
    footerSelect.forEach(selectCallback);
    sideBarSelect.forEach(selectCallback);
    subPageButtons.forEach(button => onePageMode ? hide(button) : show(button));
    if (onePageMode) {
        // Remove fieldset for subpages
        const subPagesFields = pagAddPage.parentElement.querySelectorAll("fieldset fieldset fieldset");
        subPagesFields.forEach(field => field.remove());
    }
}

// Set correct selects for the selected page-mode
document.querySelector<HTMLSelectElement>("select[name=\"gen-mode\"]").addEventListener("change", setMode);

// Toggle blog-module
blogCheckbox.addEventListener("input", setSelects);

// Toggle calendar-module
calCheckbox.addEventListener("input", () => {
    if (calCheckbox.checked) {
        // Show the button to add a field
        calAddField.style.display = "block";
    } else {
        // Hide button to add a field
        calAddField.style.display = "none";
        // Remove all added fields
        calAddField.parentElement.querySelectorAll("fieldset").forEach(field => {
            (field as HTMLElement).remove();
        });
    }
    setSelects();
});

// Add fieldset for adding field to calendar
calAddField.addEventListener("click", () => {
    // Clone the template and insert it before the button
    calTemplateField.parentNode.insertBefore(calTemplateField.content.cloneNode(true), calAddField);
});

// Toggle gallery-module
galCheckbox.addEventListener("input", () => {
    galCatCheckbox.parentElement.style.display = galCheckbox.checked ? "flex" : "none";
    setSelects();
});

// Add fieldset for page
pagAddPage.addEventListener("click", () => {
    // Add fieldset for page-details
    const page = pagTemplatePage.content.cloneNode(true);
    pagTemplatePage.parentNode.insertBefore(page, pagAddPage);
    // Retrieve last button for adding sub-page (the last button is the one which is just inserted)
    const buttons = pagTemplatePage.parentElement.querySelectorAll("button.add-sub-page");
    buttons[buttons.length - 1].addEventListener("click", e => {
        // The fieldset for details of the page
        const fieldSet = (e.target as HTMLButtonElement).parentElement;
        // Insert the fieldset for details of the subpage
        const subPage = pagTemplatePage.content.cloneNode(true);
        fieldSet.insertBefore(subPage, e.target as HTMLButtonElement);
        // Remove the button for adding sub-pages out of the fieldset for details of the subpage
        fieldSet.querySelectorAll("button.add-sub-page")[0].remove();
        // Change text in 'legend'
        fieldSet.querySelector("fieldset:last-of-type").querySelector("legend").innerText = "Subpage";
        setSelects();
        setMode();
    });
    setSelects();
    setMode();
});

// Submit form
document.getElementById("save-config").addEventListener("click", e => {
    e.preventDefault();
    const subPages = document.querySelectorAll("fieldset fieldset fieldset");
    subPages.forEach(subPage => {
        const input = subPage.querySelector<HTMLInputElement>("input[name=\"pag-subpage[]\"]");
        if (input) input.value = "true";
    });
    const data = new FormData(document.querySelector("form"));
    fetch("/setup/assets/php/form.php", { method: "POST", body: data })
        .then(res => res.json())
        .then(json => {
            alert(json["message"]);
        });
});