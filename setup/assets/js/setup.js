// Values and options for modules dropdown
var options = {
    blog: {
        option: "Blog",
        value: "blog"
    },
    calendar: {
        option: "Calendar",
        value: "calendar"
    },
    gallery: {
        option: "Gallery",
        value: "gallery"
    },
    calendarSideModule: {
        option: "Calendar side-module",
        value: "calendar"
    }
};
// Buttons
var calAddField = document.getElementById("cal-btn-add-field");
var pagAddPage = document.getElementById("pag-btn-add-page");
// Templates
var calTemplateField = document.getElementById("cal-field");
var pagTemplatePage = document.getElementById("pag-add-page");
// Checkboxes
var getCheckbox = function (name) { return document.querySelector("input[type=\"checkbox\"][name=\"" + name + "\"]"); };
var calCheckbox = getCheckbox("cal-add");
var galCheckbox = getCheckbox("gal-add");
var galCatCheckbox = getCheckbox("gal-cat");
var blogCheckbox = getCheckbox("blog-add");
/**
 * Insert an option in a select-element.
 * @param option The option to insert
 * @param select The select-element into which to insert the option.
 */
function insertOption(option, select) {
    var optionToInsert = document.createElement("option");
    optionToInsert.innerHTML = options[option].option;
    optionToInsert.value = options[option].value;
    select.append(optionToInsert);
}
/**
 * Set select-elements with options for modules to add to pages.
 */
function setSelects() {
    var modulesSelect = document.querySelectorAll("select[name=\"pag-mod[]\"]");
    var sidebarSelect = document.querySelectorAll("select[name=\"pag-sidebar[]\"]");
    modulesSelect.forEach(function (select) {
        var galOpt = select.querySelector("option[value=\"" + options["gallery"].value + "\"]");
        var calOpt = select.querySelector("option[value=\"" + options["calendar"].value + "\"]");
        var blogOpt = select.querySelector("option[value=\"" + options["blog"].value + "\"]");
        if (galCheckbox.checked && galOpt == null)
            insertOption("gallery", select);
        else if (!galCheckbox.checked && galOpt != null)
            galOpt.remove();
        if (calCheckbox.checked && calOpt == null)
            insertOption("calendar", select);
        else if (!calCheckbox.checked && calOpt != null)
            calOpt.remove();
        if (blogCheckbox.checked && blogOpt == null)
            insertOption("blog", select);
        else if (!blogCheckbox.checked && blogOpt != null)
            blogOpt.remove();
    });
    sidebarSelect.forEach(function (select) {
        var calSideModuleOpt = select.querySelector("option[value=\"" + options["calendarSideModule"].value + "\"]");
        if (calCheckbox.checked && calSideModuleOpt == null)
            insertOption("calendarSideModule", select);
        else if (!calCheckbox.checked && calSideModuleOpt != null)
            calSideModuleOpt.remove();
    });
}
/**
 * Toggle selects and buttons tied to page-mode.
 */
function setMode() {
    var modeSelect = document.querySelector("select[name=\"gen-mode\"]");
    var footerSelect = document.querySelectorAll("select[name=\"pag-footer[]\"]");
    var sideBarSelect = document.querySelectorAll("select[name=\"pag-sidebar[]\"]");
    var subPageButtons = document.querySelectorAll("button.add-sub-page");
    var onePageMode = modeSelect.value === "one-page";
    var show = function (el) { return el.style.display = "flex"; };
    var hide = function (el) { return el.style.display = "none"; };
    var selectCallback = function (select) { return onePageMode ? hide(select.parentElement) : show(select.parentElement); };
    footerSelect.forEach(selectCallback);
    sideBarSelect.forEach(selectCallback);
    subPageButtons.forEach(function (button) { return onePageMode ? hide(button) : show(button); });
    if (onePageMode) {
        // Remove fieldset for subpages
        var subPagesFields = pagAddPage.parentElement.querySelectorAll("fieldset fieldset fieldset");
        subPagesFields.forEach(function (field) { return field.remove(); });
    }
}
// Set correct selects for the selected page-mode
document.querySelector("select[name=\"gen-mode\"]").addEventListener("change", setMode);
// Toggle blog-module
blogCheckbox.addEventListener("input", setSelects);
// Toggle calendar-module
calCheckbox.addEventListener("input", function () {
    if (calCheckbox.checked) {
        // Show the button to add a field
        calAddField.style.display = "block";
    }
    else {
        // Hide button to add a field
        calAddField.style.display = "none";
        // Remove all added fields
        calAddField.parentElement.querySelectorAll("fieldset").forEach(function (field) {
            field.remove();
        });
    }
    setSelects();
});
// Add fieldset for adding field to calendar
calAddField.addEventListener("click", function () {
    // Clone the template and insert it before the button
    calTemplateField.parentNode.insertBefore(calTemplateField.content.cloneNode(true), calAddField);
});
// Toggle gallery-module
galCheckbox.addEventListener("input", function () {
    galCatCheckbox.parentElement.style.display = galCheckbox.checked ? "flex" : "none";
    setSelects();
});
// Add fieldset for page
pagAddPage.addEventListener("click", function () {
    // Add fieldset for page-details
    var page = pagTemplatePage.content.cloneNode(true);
    pagTemplatePage.parentNode.insertBefore(page, pagAddPage);
    // Retrieve last button for adding sub-page (the last button is the one which is just inserted)
    var buttons = pagTemplatePage.parentElement.querySelectorAll("button.add-sub-page");
    buttons[buttons.length - 1].addEventListener("click", function (e) {
        // The fieldset for details of the page
        var fieldSet = e.target.parentElement;
        // Insert the fieldset for details of the subpage
        var subPage = pagTemplatePage.content.cloneNode(true);
        fieldSet.insertBefore(subPage, e.target);
        // Remove the button for adding sub-pages out of the fieldset for details of the subpage
        fieldSet.querySelectorAll("button.add-sub-page")[0].remove();
        // Change text in 'legend'
        fieldSet.querySelector("fieldset:last-of-type").querySelector("legend").innerText = "Subpage";
        setSelects();
        setMode();
    });
    setSelects();
    setMode();
});
// Submit form
document.getElementById("save-config").addEventListener("click", function (e) {
    e.preventDefault();
    var subPages = document.querySelectorAll("fieldset fieldset fieldset");
    subPages.forEach(function (subPage) {
        var input = subPage.querySelector("input[name=\"pag-subpage[]\"]");
        if (input)
            input.value = "true";
    });
    var data = new FormData(document.querySelector("form"));
    fetch("/setup/assets/php/form.php", { method: "POST", body: data })
        .then(function (res) { return res.json(); })
        .then(function (json) {
        alert(json["message"]);
    });
});
