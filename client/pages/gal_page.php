<?php

require_once __DIR__ . "/../../modules/gallery/pages/client.php";

/**
 * Render client-side page with Gallery-module
 * @param Client $client       Client-instance to which this is connected
 * @param PageDetails $details PageDetails of the page to render
 * @return string              HTML-code of the page
 * @throws CalendarException   If default-page contains Calendar and throws exception
 * @throws PagesException      If no pages are added
 * @throws SetupException      If unknown language is set
 */
function get_gallery_page(Client $client, PageDetails $details): string {
    $content = get_client_content_gallery($client, $details);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $client->get_lang() ?>">
    <head>
        <title><?php echo $details->title ?></title>
        <?php echo $client->get_header() ?>
        <script src="/client/assets/js/gallery.min.js" defer></script>
    </head>
    <body>
    <nav>
        <?php $client->get_menu($details->alias) ?>
    </nav>
    <div>
        <h1><?php echo $details->title ?></h1>
        <?php echo $content ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}