<?php

require_once __DIR__ . "/../../modules/blog/pages/client.php";

/**
 * Render client-side page with overview of posts
 * @param Client $client       Client-instance to which this is connected
 * @param PageDetails $details PageDetails of the page to render
 * @return string              HTML-code of the page
 * @throws PagesException      If no pages were added
 * @throws PagesException      If no pages were added
 * @throws SetupException      When get_string() throws an exception
*/
function get_blog_page(Client $client, PageDetails $details): string {
    $content = get_client_content_blog($client, $details);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $client->get_lang() ?>">
    <head>
        <title><?php echo $details->title ?></title>
        <?php echo $client->get_header() ?>
    </head>
    <body>
    <nav>
        <?php $client->get_menu($details->alias) ?>
    </nav>
    <div>
        <h1><?php echo $details->title ?></h1>
        <?php echo $content ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}
