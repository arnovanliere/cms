<?php
/**
 * Render client-side page
 * @param Client $client       Client-instance to which this is connected
 * @param PageDetails $details PageDetails of the page to render
 * @return string              HTML-code of the page
 * @throws PagesException      If no pages were added
 * @throws CalendarException   If something went wrong with the side-module
 * @throws SetupException      If unknown language is set
 */
function get_default_page(Client $client, PageDetails $details): string {
    $alias = (isset($_GET["p"])) ? $_GET["p"] : $client->get_default_page();
    $page = $client->get_pages()->get_page($alias);
    $sidebar = $client->get_pages()->get_sidebar($client->get_db(), $page->alias);
    $footer = $client->get_pages()->get_footer($client->get_db(), $page->alias);
    $content = $client->get_pages()->get_content($client->get_db(), $page->alias);
    $cal_side_mod_page = $client->get_pages()->get_side_mod_page();
    $has_cal_side_mod = $cal_side_mod_page != null && $alias == $cal_side_mod_page->alias;
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $client->get_lang() ?>">
    <head>
        <title><?php echo $details->title ?></title>
        <?php echo $client->get_header() ?>
    </head>
    <body>
        <nav>
            <?php $client->get_menu($alias) ?>
        </nav>
        <?php
        if ($sidebar != null) {
            ob_start(); ?>
            <aside>
                <?php echo $sidebar ?>
            </aside>
            <?php echo ob_get_clean();
        }
        ?>
        <div>
            <h1><?php echo $details->title ?></h1>
            <?php
            if ($has_cal_side_mod) {
                $client->get_calendar()->get_side_module()->render_module($client);
            }
            ?>
            <div><?php echo str_replace("\n", "<br>", $content) ?></div>
        </div>
        <?php
        if ($footer) {
            ob_start(); ?>
            <aside>
                <?php echo $footer ?>
            </aside>
            <?php echo ob_get_clean();
        }
        ?>
    </body>
    </html>
    <?php return ob_get_clean();
}