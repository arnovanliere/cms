<?php
/**
 * Render client-side page with overview of posts
 * @param Client $client       Client-instance to which this is connected
 * @param PageDetails $details PageDetails of the page to render
 * @return string              HTML-code of the page
 * @throws PagesException      If no pages were added
 * @throws SetupException      If unknown language is set
 */
function get_blog_detail_page(Client $client, PageDetails $details): string {
    $alias = (isset($_GET["p"])) ? $_GET["p"] : $client->get_default_page();
    if (!isset($_GET["pid"])) { echo get_blog_page($client, $details); die; }
    $query = "SELECT * FROM " . Database::TABLE_BLOG . " WHERE id = :id";
    $post = $client->get_db()->exec_fetch($query, ["id" => $_GET["pid"]]);
    if (isset($_POST["post-comment"])) add_comment($client);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $client->get_lang() ?>">
    <head>
        <title><?php echo $details->title ?></title>
        <?php echo $client->get_header() ?>
    </head>
    <body>
    <nav>
        <?php $client->get_menu($alias) ?>
    </nav>
    <div>
        <h1><?php echo $details->title ?></h1>
        <?php
        if ((is_bool($post) && !$post) || empty($post)) {
            echo $client->get_string(BLOG_POST_NOT_FOUND);
        } else {
            $post = $post[0];
            ob_start(); ?>
            <div class="post">
                <div class="header">
                    <span><?php echo $post["title"] ?></span>
                    <span><?php echo $post["creation_date"] ?></span>
                </div>
                <div class="content">
                    <?php echo $post["post"] ?>
                </div>
                <?php
                if ($post["comments_enabled"] == 0) {
                    echo $client->get_string(BLOG_COMMENTS_DISABLED);
                } else {
                    $comments = json_decode($post["comments"], true);
                    echo "<ul class='comments'>";
                    echo "<li>
                              <form method='post'>
                                  <input type='hidden' name='post-id' value='{$_GET["pid"]}'>
                                  <input type='text' name='name' placeholder='Your Name' required>
                                  <input type='email' name='email' placeholder='Email' required>
                                  <textarea name='comment' placeholder='Type your comment here' required></textarea>
                                  <button type='submit' name='post-comment'>Post Comment</button>
                              </form>
                          </li>";
                    if (!isset($post["comments"]) || empty($post["comments"])) {
                        echo "</ul>{$client->get_string(BLOG_NO_COMMENTS_YET)}";
                    } else {
                        foreach (array_reverse($comments, true) as $key => $comment) {
                            ob_start(); ?>
                            <li data-comment="<?php echo $key ?>">
                                <span><?php echo $comment["name"] ?></span>
                                <span><?php echo $comment["time"] ?></span>
                                <span><?php echo $comment["email"] ?></span>
                                <span><?php echo $comment["comment"] ?></span>
                                <span class="reply-comment" data-comment="<?php echo $key ?>">Reply</span>
                                <?php
                                if (isset($comment["comments"]) && !empty($comment["comments"])) {
                                    echo "<ul>";
                                    foreach (array_reverse($comment["comments"], true) as $sub_key => $sub_comment) {
                                        ob_start(); ?>
                                        <li>
                                            <span><?php echo $sub_comment["name"] ?></span>
                                            <span><?php echo $sub_comment["time"] ?></span>
                                            <span><?php echo $sub_comment["email"] ?></span>
                                            <span><?php echo $sub_comment["comment"] ?></span>
                                        </li>
                                        <?php echo ob_get_clean();
                                    }
                                    echo "</ul>";
                                }
                                ?>
                            </li>
                        <?php } ob_get_clean();
                    }
                    echo "</ul>";
                }
                ?>
            </div>
            <?php echo ob_get_clean();
        }
        ?>
        <!-- Template for input to reply to a comment -->
        <template>
            <li>
                <form method="post">
                    <input type='hidden' name='post-id' value='<?php echo $_GET["pid"] ?>'>
                    <input type='hidden' name='comment-id'> <!-- ID of the comment the user is replying to -->
                    <input type='text' name='name' placeholder='Your Name' required>
                    <input type='email' name='email' placeholder='Email' required>
                    <textarea name='comment' placeholder='Type your comment here' required></textarea>
                    <button type='submit' name='post-comment'>Post Comment</button>
                </form>
            </li>
        </template>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}