<?php
/**
 * Render client-side page
 * @param Client $client  Client-instance to which this is connected
 * @return string         HTML-code of the page
 * @throws PagesException If default-page cannot be retrieved
 */
function get_one_page_site(Client $client): string {
    $pages = $client->get_pages()->get_pages();
    $explode = explode("#", $_SERVER["REQUEST_URI"]);
    $active = ($_SERVER["REQUEST_URI"] !== "/") ? $explode[count($explode) - 1] : $client->get_default_page();
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $client->get_lang() ?>">
    <head>
        <title><?php echo $client->get_site_title() ?></title>
        <?php echo $client->get_header() ?>
    </head>
    <body>
        <nav>
            <?php echo $client->get_menu($active) ?>
        </nav>
        <?php
        foreach ($pages as $page) {
            $content = $client->get_pages()->get_content($client->get_db(), $page->alias);
            echo "<div id='{$page->alias}'>";
            echo "<h1>{$page->title}</h1>";
            if ($page->calendar != null) echo get_client_content_calendar($client, $page);
            else if ($page->gallery != null) echo get_client_content_gallery($client, $page);
            else if ($page->blog != null) echo get_client_content_blog($client, $page);
            else echo str_replace("\n", "<br>", $content);
            echo "</div>";
        }
        ?>
    </body>
    <?php return ob_get_clean();
}