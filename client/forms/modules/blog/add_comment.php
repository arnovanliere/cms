<?php
/**
 * Add a comment to a post
 * @param Client $client The Client-instance to which the Blog-module is added
 */
function add_comment(Client $client) {
    if (!isset($_POST["post-comment"]) || !isset($_POST["post-id"]) ||
        !isset($_POST["name"]) || !isset($_POST["email"]) ||
        !isset($_POST["comment"])) return;

    // Fetch the post to which the user wants to post comment
    $query = "SELECT * FROM " . Database::TABLE_BLOG . " WHERE id = :id";
    $post = $client->get_db()->exec_fetch($query, ["id" => $_POST["post-id"]]);

    if ((is_bool($post) && !$post) || count($post) != 1) {
        return; // TODO show error to client, query failed
    }

    if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        return; // TODO show error to client, invalid email
    }

    $post = $post[0]; // There is only 1 post, grab the first one
    $post_comments = json_decode($post["comments"], true); // Decode as associative array

    // Create the new comment
    $comment_obj = [
        "name" => $_POST["name"],
        "time" => date("Y-m-d H:i:s"),
        "email" => $_POST["email"],
        "comment" => $_POST["comment"],
    ];

    if (isset($_POST["comment-id"])) { // It's a comment an a comment
        array_push($post_comments[$_POST["comment-id"]]["comments"], $comment_obj);
    } else { // It's a comment on the post, sub-comments are available
        $comment_obj["comments"] = [];
        array_push($post_comments, $comment_obj);
    }

    $post_comments = json_encode($post_comments);
    $update_query = "UPDATE " . Database::TABLE_BLOG . " SET comments = :comments WHERE id = :id";
    $update_params = ["comments" => $post_comments, "id" => $_POST["post-id"]];
    $update_success = $client->get_db()->exec($update_query, $update_params);
    if (!$update_success) {
        // TODO show error to client, update failed
    }
}