var thumbs = document.getElementsByClassName("thumb");
var lightBoxes = document.getElementById("lightboxes");
var firstLightBox = document.getElementsByClassName("lightbox-item")[0];
var lightBoxesNav = document.getElementById("lightboxes-nav");
var indicator = document.getElementById("indicator");
var navPrev = document.getElementById("nav-prev");
var navNext = document.getElementById("nav-next");
var closeLightBox = document.getElementById("close-lightbox");
var leftMargin = 0;
var destination;
var stepSize = 2 * thumbs.length - 1;
var curImg = 0;
var totalImgs = thumbs.length;
var interval = false;
var _loop_1 = function (i) {
    thumbs[i].addEventListener("click", function () {
        if (!interval) {
            // Show correct image
            leftMargin = -i * 100;
            firstLightBox.style.marginLeft = leftMargin + "%";
            setIndicator(i + 1);
            // Slide lighbox up
            lightbox(true);
        }
    });
};
for (var i = 0; i < thumbs.length; i++) {
    _loop_1(i);
}
navPrev.addEventListener("click", function () {
    if (!interval) {
        // If you go to previous from first image, you go to last image
        if (leftMargin >= 0) {
            setIndicator(thumbs.length);
            destination = (curImg - 1) * -100;
            move(destination, stepSize);
        }
        else {
            // Else go to previous image
            setIndicator(curImg - 1);
            move(leftMargin + 100, 2);
        }
    }
});
navNext.addEventListener("click", function () {
    if (!interval) {
        // If you go to next image from last image, you go to first image
        if (leftMargin <= ((thumbs.length - 1) * -100)) {
            setIndicator(1);
            move(0, stepSize);
        }
        else {
            // Else go to next image
            setIndicator(curImg + 1);
            move(leftMargin - 100, 2);
        }
    }
});
closeLightBox.addEventListener("click", function () {
    if (!interval) {
        // Slide lightbox down
        lightbox(false);
    }
});
function lightbox(show) {
    if (!interval) {
        // Slide lightbox
        lightBoxes.style.top = (show) ? "0" : "100vh";
        // Hide/show navigation
        lightBoxesNav.style.top = (show) ? "0" : "100vh";
    }
}
function setIndicator(dest) {
    curImg = dest;
    indicator.innerText = curImg + " / " + totalImgs;
}
function move(dest, step) {
    interval = true;
    var id = setInterval(frame, 1);
    var curMargin = parseInt(firstLightBox.style.marginLeft);
    function frame() {
        if (leftMargin === dest) {
            clearInterval(id);
            interval = false;
        }
        else {
            leftMargin = (dest < curMargin) ? leftMargin - step : leftMargin + step;
            // It is gone before destination
            if (dest < curMargin && leftMargin < dest) {
                leftMargin = dest;
            }
            // It is gone beyond destination
            if (dest > curMargin && leftMargin > dest) {
                leftMargin = dest;
            }
            // Set margin
            firstLightBox.style.marginLeft = leftMargin + "%";
        }
    }
}
