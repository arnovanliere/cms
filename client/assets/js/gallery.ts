const thumbs: HTMLCollectionOf<Element> = document.getElementsByClassName("thumb");

const lightBoxes: HTMLElement = document.getElementById("lightboxes");
const firstLightBox: HTMLElement = <HTMLElement> document.getElementsByClassName("lightbox-item")[0];
const lightBoxesNav: HTMLElement = document.getElementById("lightboxes-nav");

const indicator: HTMLElement = document.getElementById("indicator");

const navPrev: HTMLElement = document.getElementById("nav-prev");
const navNext: HTMLElement = document.getElementById("nav-next");
const closeLightBox: HTMLElement = document.getElementById("close-lightbox");

let leftMargin: number = 0;
let destination: number;
let stepSize: number = 2 * thumbs.length - 1;
let curImg: number = 0;
const totalImgs: number = thumbs.length;

let interval: boolean = false;

for (let i = 0; i < thumbs.length; i++) {
    thumbs[i].addEventListener("click", () => {
        if (!interval) {
            // Show correct image
            leftMargin = -i * 100;
            firstLightBox.style.marginLeft = leftMargin + "%";
            setIndicator(i + 1);

            // Slide lighbox up
            lightbox(true);
        }
    });
}

navPrev.addEventListener("click", () => {
    if (!interval) {
        // If you go to previous from first image, you go to last image
        if (leftMargin >= 0) {
            setIndicator(thumbs.length);
            destination = (curImg - 1) * -100;
            move(destination, stepSize);
        } else {
            // Else go to previous image
            setIndicator(curImg - 1);
            move(leftMargin + 100, 2);
        }
    }
});

navNext.addEventListener("click", () => {
    if (!interval) {
        // If you go to next image from last image, you go to first image
        if (leftMargin <= ((thumbs.length - 1) * -100)) {
            setIndicator(1);
            move(0, stepSize);
        } else {
            // Else go to next image
            setIndicator(curImg + 1);
            move(leftMargin - 100, 2);
        }
    }
});

closeLightBox.addEventListener("click", () => {
    if (!interval) {
        // Slide lightbox down
        lightbox(false)
    }
});

function lightbox(show: boolean) {
    if (!interval) {
        // Slide lightbox
        lightBoxes.style.top = (show) ? "0" : "100vh";
        // Hide/show navigation
        lightBoxesNav.style.top = (show) ? "0" : "100vh";
    }
}

function setIndicator(dest: number) {
    curImg = dest;
    indicator.innerText = curImg + " / " + totalImgs;
}

function move(dest: number, step: number) {
    interval = true;
    let id: number = setInterval(frame, 1);
    const curMargin: number = parseInt(firstLightBox.style.marginLeft);
    function frame() {
        if (leftMargin === dest) {
            clearInterval(id);
            interval = false;
        } else {
            leftMargin = (dest < curMargin) ? leftMargin - step : leftMargin + step;
            // It is gone before destination
            if (dest < curMargin && leftMargin < dest) {
                leftMargin = dest;
            }
            // It is gone beyond destination
            if (dest > curMargin && leftMargin > dest) {
                leftMargin = dest;
            }
            // Set margin
            firstLightBox.style.marginLeft = leftMargin + "%";
        }
    }
}
