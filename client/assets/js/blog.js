var replyButtons = document.getElementsByClassName("reply-comment");
var inputTemplate = document.getElementsByTagName("template")[0];
var _loop_1 = function (i) {
    replyButtons[i].addEventListener("click", function () {
        var commentId = replyButtons[i].getAttribute("data-comment");
        var post = document.querySelector("li[data-comment=\"" + commentId + "\"]");
        var commentForm = inputTemplate.content.cloneNode(true);
        var existingInput = post.querySelector("ul li:first-of-type form") != null;
        if (!existingInput) { // Only add input if there isn't already one
            var existingSubComments = post.querySelector("ul") != null;
            if (!existingSubComments) { // If there are no sub-comments yet, append a 'ul' first
                post.append(document.createElement("ul"));
            }
            post.querySelector("ul").prepend(commentForm);
            post.querySelector("ul li form input[name=\"comment-id\"]").value = commentId;
        }
    });
};
for (var i = 0; i < replyButtons.length; i++) {
    _loop_1(i);
}
