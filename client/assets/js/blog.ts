const replyButtons: HTMLCollectionOf<Element> = document.getElementsByClassName("reply-comment");
const inputTemplate: HTMLTemplateElement = document.getElementsByTagName("template")[0];

for (let i = 0; i < replyButtons.length; i++) {
    replyButtons[i].addEventListener("click", () => {
         const commentId: string = replyButtons[i].getAttribute("data-comment");
         const post: HTMLElement = document.querySelector("li[data-comment=\""+commentId+"\"]");
         const commentForm: HTMLElement = (<HTMLElement>inputTemplate.content.cloneNode(true));
         const existingInput: boolean = post.querySelector("ul li:first-of-type form") != null;
         if (!existingInput) { // Only add input if there isn't already one
             const existingSubComments: boolean = post.querySelector("ul") != null;
             if (!existingSubComments) { // If there are no sub-comments yet, append a 'ul' first
                 post.append(document.createElement("ul"));
             }
             post.querySelector("ul").prepend(commentForm);
             (<HTMLInputElement>post.querySelector("ul li form input[name=\"comment-id\"]")).value = commentId;
         }
    });
}