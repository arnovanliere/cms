<?php

include_once __DIR__ . "/../config/classes.php";
include_once __DIR__ . "/pages/cal_page.php";
include_once __DIR__ . "/pages/gal_page.php";
include_once __DIR__ . "/pages/def_page.php";
include_once __DIR__ . "/pages/def_page_one_page.php";
include_once __DIR__ . "/pages/blog_overview_page.php";
include_once __DIR__ . "/pages/blog_detail_page.php";
include_once __DIR__ . "/forms/modules/blog/add_comment.php";

class Client extends Setup {

    /**
     * Client constructor.
     * @param Database $db       Database to connect to
     * @param string $lang       Constant representing language
     * @param string $site_title The title of the site
     * @throws SetupException    If unknown language is provided
     */
    public function __construct(Database $db, string $lang, string $site_title) {
        parent::__construct($db, $lang, $site_title);
    }

    /**
     * Load the page corresponding with 'alias'
     * @param string $alias      The alias of the page to load
     * @throws PagesException    If no pages were added
     * @throws CalendarException If something went wrong with the side-module
     * @throws SetupException    If unknown language is set
     */
    public function load_page(string $alias) {
        if ($this->get_pages()->get_site_mode() === Pages::MODE_ONE_PAGE) {
            echo get_one_page_site($this);
            die();
        }
        foreach ($this->get_pages()->get_pages() as $page) {
            $this->check_modules($page, $alias);
            if (isset($page->sub_pages)) {
                foreach ($page->sub_pages as $sub_page) {
                    $this->check_modules($sub_page, $alias);
                }
            }
        }
    }

    /**
     * Load the correct page (check if a module is placed on this page)
     * @param PageDetails $page  The page to load
     * @param string $alias      The alias of the active page
     * @throws CalendarException If something went wrong with the side-module
     * @throws PagesException    If no pages are added
     * @throws SetupException    If an unknown language is set
     */
    private function check_modules(PageDetails $page, string $alias) {
        if ($page->alias == $alias) {
            if ($page->calendar != null) echo get_calendar_page($this, $page);
            else if ($page->gallery != null) echo get_gallery_page($this, $page);
            else if ($page->blog != null) echo get_blog_page($this, $page);
            else echo get_default_page($this, $page);
            die();
        }
    }

    /**
     * Get the alias of the default page
     * @return string         The alias of the default page
     * @throws PagesException If no pages were added
     */
    public function get_default_page(): string {
        if (empty($this->get_pages()->get_pages())) {
            throw new NoPagesAdded();
        }
        return $this->get_pages()->get_pages()[0]->alias;
    }

    /**
     * Load the default page when no alias is provided
     * @throws PagesException    If no pages were added
     * @throws CalendarException If something went wrong with the side-module
     * @throws SetupException    If unknown language is set
     */
    public function load_default_page() {
        $pages_mod = $this->get_pages();
        if ($pages_mod->get_site_mode() === Pages::MODE_ONE_PAGE)
            echo get_one_page_site($this);
        else
            echo get_default_page($this, $this->get_pages()->get_page($this->get_default_page()));
    }

    /**
     * Render the menu
     * @param array $pages   Pages of the menu to render
     * @param string $active Alias of the active page
     */
    private function render_menu(array $pages, string $active) {
        echo '<ul>';
        foreach ($pages as $page) {
            echo '<li';
            if ($page->alias == $active) {
                echo ' class="active"';
            }
            $indicator = ($this->get_pages()->get_site_mode() === Pages::MODE_ONE_PAGE) ? "#" : "?p=";
            echo '><a href="/'.$indicator.$page->alias.'">'.$page->title.'</a>';
            if (isset($page->sub_pages)) {
                $this->render_menu($page->sub_pages, $active);
            }
            echo '</li>';
        }
        echo '</ul>';
    }

    /**
     * Render complete menu
     * @param string $active Alias of the active page
     */
    public function get_menu(string $active) {
        $this->render_menu($this->get_pages()->get_pages(), $active);
    }

    /**
     * Get the default header for all client-side pages
     * @return string The standard header
     */
    public function get_header(): string {
        ob_start(); ?>
        <script src="https://kit.fontawesome.com/6dfbda6995.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;900&display=swap">
        <link rel="stylesheet" href="/client/assets/css/main.css">
        <style>
        <?php if ($this->get_style() != null) echo $this->get_style()->retrieve_style($this->get_db()) ?>
        </style>
        <script src="/client/assets/js/blog.min.js" defer></script>
        <?php return ob_get_clean();
    }

}