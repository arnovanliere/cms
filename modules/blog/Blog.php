<?php

include_once __DIR__ . "/pages/add_post.php";
include_once __DIR__ . "/pages/added_posts.php";
include_once __DIR__ . "/pages/comments.php";
include_once __DIR__ . "/forms/add_post.php";
include_once __DIR__ . "/forms/delete_post.php";
include_once __DIR__ . "/forms/delete_comment.php";
include_once __DIR__ . "/forms/update_post.php";

class Blog extends Module {
    /**
     * ID of the module
     */
    public const ID = "blog";

    /**
     * Tags for the pages of this module
     */
    public const ADD_POST_PAGE_TAG = "add-post";
    public const ADDED_POSTS_PAGE_TAG = "added-posts";
    public const COMMENTS_PAGE_TAG = "comments";

    /**
     * Constants to save results of database-transactions
     */
    public const SESS_ADD_POST_RES = "add-post-res";
    public const SESS_DELETE_POST_RES = "delete-post-res";
    public const SESS_UPDATE_POST_RES = "update-post-res";
    public const SESS_DELETE_COMMENT_RES = "delete-comment-res";

    /**
     * Blog constructor.
     */
    public function __construct() {
        parent::__construct(self::ID, BLOG_MOD_NAME, [
            self::ADD_POST_PAGE_TAG => BLOG_PAG_NAME_ADD_POST,
            self::ADDED_POSTS_PAGE_TAG => BLOG_PAG_NAME_ADDED_POSTS,
            self::COMMENTS_PAGE_TAG => BLOG_PAG_NAME_COMMENTS
        ]);
    }

    /**
     * Load the module
     * @param Dashboard $app  The parent from which this module is loaded
     * @throws SetupException If unknown language is set
     */
    public function load(Dashboard $app) {
        if (!isset($_GET["p"])) {
            $this->load_default_page();
        } else {
            switch ($_GET["p"]) {
                case self::ADD_POST_PAGE_TAG: {
                    echo get_add_post_page($app);
                    break;
                }
                case self::ADDED_POSTS_PAGE_TAG: {
                    echo get_added_posts_page($app);
                    break;
                }
                case self::COMMENTS_PAGE_TAG: {
                    echo get_comments_page($app);
                    break;
                }
                default: $this->load_default_page();
            }
        }
    }

    /**
     * Retrieve the posts
     * @param Database $db Database-instance to retrieve posts from
     * @return array|null  'null' if no posts are added, otherwise array with posts
     */
    public function get_posts(Database $db): ?array {
        $posts = $db->exec_fetch("SELECT * FROM " . Database::TABLE_BLOG);
        if (!empty($posts))
            return $posts;
        return null;
    }

    /**
     * Count the number of comments on a post
     * @param string|null $comments JSON as string
     * @return int                  The number of comments
     */
    public static function count_comments(?string $comments): int {
        if ($comments == null || empty($comments)) return 0;
        $array = json_decode($comments, true);
        $count = 0;
        foreach ($array as $key => $value) {
            $count += count($array[$key]["comments"]); // Sub-comments
            $count++; // Current comment
        }
        return $count;
    }

}