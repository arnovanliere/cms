<?php
/**
 * Render the "Comments"-page
 * @param Dashboard $app  The app to which this page is connected
 * @return string         The HTML-code of the page
 * @throws SetupException If unknown language is set
 * @throws Exception      If Popup throws exception
 */
function get_comments_page(Dashboard $app): string {
    $app->require_login();
    $posts = $app->get_blog()->get_posts($app->get_db());
    if (isset($_POST["delete-comment"])) delete_comment($app);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(BLOG_PAG_NAME_COMMENTS) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
    <?php echo $app->get_menu() ?>
    <div class="page-content" id="pag-edit-blog-post">
        <h1><?php echo $app->get_string(BLOG_PAG_NAME_COMMENTS) ?></h1>
        <?php
        if (!isset($posts)) {
            echo $app->get_string(BLOG_MSG_NO_POSTS_ADDED);
        } else {
            foreach ($posts as $post) {
                $comments_enabled = $post["comments_enabled"] == 1;
                $comments = ($comments_enabled) ? json_decode($post["comments"]) : null;
                $index_comments = 0;
                ob_start(); ?>
                <div class="post-wrapper">
                    <div class="title"><?php echo $app->get_string(BLOG_TITLE_POST).$post["title"] ?></div>
                    <?php
                    if (!$comments_enabled) {
                        echo "<div class='italic'>Comments are disabled on this post</div>";
                    } else if (!isset($comments)) { // json_decode == null if string could not be decoded
                        echo "<div class='italic'>{$app->get_string(BLOG_MSG_NO_COMMENTS_ON_POST)}</div>";
                    } else {
                        echo "<ul class='comments'>";
                        foreach ($comments as $comment) {
                            ob_start(); ?>
                            <li>
                                <div class="name-email-time-wrapper">
                                    <span class="name"><?php echo $comment->name ?></span>
                                    <span class="email"><?php echo $comment->email ?></span>
                                    <span class="time"><?php echo $comment->time ?></span>
                                    <form method="post">
                                        <input type="hidden" name="post-id" value="<?php echo $post["id"] ?>">
                                        <input type="hidden" name="comment-id" value="<?php echo $index_comments ?>">
                                        <button type="submit" name="delete-comment" class="warning"><i class='far fa-trash-alt'></i></button>
                                    </form>
                                </div>
                                <span class="comment"><?php echo $comment->comment ?></span>
                                <?php
                                if (isset($comment->comments)) {
                                    $index_sub_comments = 0;
                                    echo "<ul>";
                                    foreach ($comment->comments as $sub_comment) {
                                        ob_start(); ?>
                                        <li>
                                            <div class="name-email-time-wrapper">
                                                <span class="name"><?php echo $sub_comment->name ?></span>
                                                <span class="email"><?php echo $sub_comment->email ?></span>
                                                <span class="time"><?php echo $sub_comment->time ?></span>
                                                <form method="post">
                                                    <input type="hidden" name="post-id" value="<?php echo $post["id"] ?>">
                                                    <input type="hidden" name="comment-id" value="<?php echo $index_comments ?>">
                                                    <input type="hidden" name="sub-comment-id" value="<?php echo $index_sub_comments ?>">
                                                    <button type="submit" name="delete-comment" class="warning"><i class='far fa-trash-alt'></i></button>
                                                </form>
                                            </div>
                                            <span class="comment"><?php echo $sub_comment->comment ?></span>
                                        </li>
                                        <?php echo ob_get_clean();
                                        $index_sub_comments++;
                                    }
                                    echo "</ul>";
                                }
                                ?>
                            </li>
                            <?php echo ob_get_clean();
                            $index_comments++;
                        }
                        echo "</ul>";
                    }
                    ?>
                </div>
                <?php echo ob_get_clean();
            }
        }
        if (isset($_SESSION[Blog::SESS_DELETE_COMMENT_RES])) {
            if ($_SESSION[Blog::SESS_DELETE_COMMENT_RES]) {
                Popup::show_popup($app, Popup::SUCCESS, $app->get_string(BLOG_MSG_DEL_COMMENT_SUCCESS));
            } else {
                Popup::show_popup($app, Popup::ERROR, $app->get_string(BLOG_MSG_DEL_COMMENT_ERROR));
            }
            unset($_SESSION[Blog::SESS_DELETE_COMMENT_RES]);
        }
        ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}