<?php
/**
 * Render the "Added Posts"-page
 * @param Dashboard $app  The app to which this page is connected
 * @return string         The HTML-code of the page
 * @throws SetupException If unknown language is set
 * @throws Exception      If Popup throws exception
 */
function get_added_posts_page(Dashboard $app): string {
    $app->require_login();
    $posts = $app->get_blog()->get_posts($app->get_db());
    if (isset($_POST["delete-post"])) delete_post($app);
    if (isset($_POST["save-post"])) update_post($app);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(BLOG_PAG_NAME_ADDED_POSTS) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
    <?php echo $app->get_menu() ?>
    <div class="page-content" id="pag-edit-blog-post">
        <h1><?php echo $app->get_string(BLOG_PAG_NAME_ADDED_POSTS) ?></h1>
        <?php
        if (!isset($posts)) {
            echo $app->get_string(BLOG_MSG_NO_POSTS_ADDED);
        } else {
            foreach ($posts as $post) {
                $checked = ($post["comments_enabled"] == 1) ? " checked" : "";
                ob_start(); ?>
                <div class="edit-post-wrapper">
                    <form method="post">
                        <div class="input-button-wrapper">
                            <input type="text" name="edit-title" value="<?php echo $post["title"] ?>" required>
                            <button type="submit" name="delete-post" class="warning"><i class='far fa-trash-alt'></i></button>
                            <button type="submit" name="save-post"><?php echo $app->get_string(BLOG_BTN_SAVE_CHANGES) ?></button>
                        </div>
                        <input type="hidden" name="post-id" value="<?php echo $post["id"] ?>">
                        <textarea name="edit-content" required><?php echo $post["post"] ?></textarea>
                        <label for="comments"><?php echo $app->get_string(BLOG_INPUT_COMMENTS) ?></label>
                        <input type="checkbox" name="edit-comments"<?php echo $checked ?>>
                    </form>
                </div>
                <?php echo ob_get_clean();
                if (isset($_SESSION[Blog::SESS_DELETE_POST_RES])) {
                    if ($_SESSION[Blog::SESS_DELETE_POST_RES]) {
                        Popup::show_popup($app, Popup::SUCCESS, $app->get_string(BLOG_MSG_DELETE_POST_SUCCESS));
                    } else {
                        Popup::show_popup($app, Popup::ERROR, $app->get_string(BLOG_MSG_DELETE_POST_ERROR));
                    }
                    unset($_SESSION[Blog::SESS_DELETE_POST_RES]);
                } else if (isset($_SESSION[Blog::SESS_UPDATE_POST_RES])) {
                    if ($_SESSION[Blog::SESS_UPDATE_POST_RES]) {
                        Popup::show_popup($app, Popup::SUCCESS, $app->get_string(BLOG_MSG_UPDATE_POST_SUCCESS));
                    } else {
                        Popup::show_popup($app, Popup::ERROR, $app->get_string(BLOG_MSG_UPDATE_POST_ERROR));
                    }
                    unset($_SESSION[Blog::SESS_UPDATE_POST_RES]);
                }
            }
        }
        ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}