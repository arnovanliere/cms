<?php
/**
 * Render client-side content for the blog-page
 * @param Client $client       Client-instance to which this is connected
 * @param PageDetails $details PageDetails of the page to render
 * @return string              HTML-code of the page
 * @throws PagesException      If no pages were added
 * @throws PagesException      If no pages were added
 * @throws SetupException      When get_string() throws an exception
 */
function get_client_content_blog(Client $client, PageDetails $details): string {
    $alias = (isset($_GET["p"])) ? $_GET["p"] : $client->get_default_page();
    if (isset($_GET["pid"])) { echo get_blog_detail_page($client, $details); die; }
    $posts = $client->get_blog()->get_posts($client->get_db());
    ob_start();
    foreach ($posts as $post) {
        ob_start(); ?>
        <a href="/?p=<?php echo $alias ?>&pid=<?php echo $post["id"] ?>">
            <div class="post">
                <div class="header">
                    <span><?php echo $post["title"] ?></span>
                    <span><?php echo $post["creation_date"] ?></span>
                </div>
                <div class="post-content">
                    <?php echo str_replace("\n", "<br>", $post["post"]) ?>
                </div>
                <div class="footer">
                    <?php
                    if ($post["comments_enabled"] != 1) {
                        echo $client->get_string(BLOG_COMMENTS_DISABLED);
                    } else {
                        echo $client->get_string(BLOG_NR_COMMENTS, strval(Blog::count_comments($post["comments"])));
                    }
                    ?>
                </div>
            </div>
        </a>
        <?php echo ob_get_clean();
    }
    return ob_get_clean();
}