<?php
/**
 * Render the "Add Post"-page
 * @param Dashboard $app  The app to which this page is connected
 * @return string         The HTML-code of the page
 * @throws SetupException If unknown language is set
 * @throws Exception      If Popup throws exception
 */
function get_add_post_page(Dashboard $app): string {
    $app->require_login();
    if (isset($_POST["create-post"])) add_post($app);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(BLOG_PAG_NAME_ADD_POST) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
    <?php echo $app->get_menu() ?>
    <div class="page-content" id="pag-add-blog-post">
        <h1><?php echo $app->get_string(BLOG_PAG_NAME_ADD_POST) ?></h1>
        <form method="post">
            <input type="text" name="post-title" placeholder="<?php echo $app->get_string(BLOG_INPUT_TITLE) ?>" required>
            <textarea name="post-content" placeholder="<?php echo $app->get_string(BLOG_INPUT_POST) ?>" required></textarea>
            <label for="comments"><?php echo $app->get_string(BLOG_INPUT_COMMENTS) ?></label>
            <input type="checkbox" name="comments" id="comments" checked>
            <button type="submit" name="create-post"><?php echo $app->get_string(BLOG_BTN_ADD_POST) ?></button>
        </form>
        <?php
        if (isset($_SESSION[Blog::SESS_ADD_POST_RES])) {
            if ($_SESSION[Blog::SESS_ADD_POST_RES]) {
                Popup::show_popup($app, Popup::SUCCESS, $app->get_string(BLOG_MSG_ADD_POST_SUCCESS));
            } else {
                Popup::show_popup($app, Popup::ERROR, $app->get_string(BLOG_MSG_ADD_POST_ERROR));
            }
            unset($_SESSION[Blog::SESS_ADD_POST_RES]);
        }
        ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}