<?php

class BlogException extends ModuleException {
    public function __construct($message, $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class BlogRedefinition extends BlogException {
    public function __construct($message = "Redefinition of Blog-module", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}