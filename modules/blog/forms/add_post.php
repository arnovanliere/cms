<?php
/**
 * Add a post
 * @param Dashboard $app Dashboard-instance to which the Blog-module is loaded
 */
function add_post(Dashboard $app) {
    if (!isset($_POST["create-post"]) || !isset($_POST["post-title"]) ||
        !isset($_POST["post-content"])) return;

    $comments_enabled = (isset($_POST["comments"])) ? 1 : 0;
    $query = "INSERT INTO " . Database::TABLE_BLOG . " (title, post, comments_enabled) VALUES(:title, :post, :comments)";
    $params = ["title" => $_POST["post-title"], "post" => $_POST["post-content"], "comments" => $comments_enabled];
    $_SESSION[Blog::SESS_ADD_POST_RES] = $app->get_db()->exec($query, $params);
}