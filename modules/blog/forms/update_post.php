<?php
/**
 * Update a post
 * @param Dashboard $app Dashboard-instance to which the Blog-module is loaded
 */
function update_post(Dashboard $app) {
    if (!isset($_POST["save-post"]) || !isset($_POST["edit-title"]) ||
        !isset($_POST["edit-content"]) || !isseT($_POST["post-id"]))
        return;

    $comments_enabled = (isset($_POST["edit-comments"])) ? 1 : 0;
    $query = "UPDATE " . Database::TABLE_BLOG . " SET title = :title, post = :post, comments_enabled = :comments WHERE id = :id";
    $params = [
        "title" => $_POST["edit-title"],
        "post" => $_POST["edit-content"],
        "comments" => $comments_enabled,
        "id" => $_POST["post-id"]
    ];
    $_SESSION[Blog::SESS_UPDATE_POST_RES] = $app->get_db()->exec($query, $params);
}