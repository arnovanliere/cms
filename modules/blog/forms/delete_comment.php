<?php
function delete_comment(Dashboard $app) {
    if (!isset($_POST["delete-comment"]) || !isset($_POST["post-id"]) ||
        !isset($_POST["comment-id"])) return;

    $post_info = $app->get_db()->exec_fetch("SELECT * FROM " . Database::TABLE_BLOG . " WHERE id = :id",
                                            ["id" => $_POST["post-id"]]);

    if ((is_bool($post_info) && !$post_info) || count($post_info) != 1) {
        // Query failed or the post is not found
        $_SESSION[Blog::SESS_DELETE_COMMENT_RES] = false;
        return;
    }

    $comments = json_decode($post_info[0]["comments"], true);
    if (!isset($comments)) {
        // JSON could not be decoded
        $_SESSION[Blog::SESS_DELETE_COMMENT_RES] = false;
        return;
    }

    if (isset($_POST["sub-comment-id"])) {
        array_splice($comments[$_POST["comment-id"]]["comments"], $_POST["sub-comment-id"], 1);
    } else {
        array_splice($comments, $_POST["comment-id"], 1);
    }

    $json = json_encode($comments);
    if (is_bool($json) && !$json) {
        // JSON could not be encoded
        $_SESSION[Blog::SESS_DELETE_COMMENT_RES] = false;
        return;
    }

    $query = "UPDATE " . Database::TABLE_BLOG . " SET comments = :comments WHERE id = :id";
    $_SESSION[Blog::SESS_DELETE_COMMENT_RES] = $app->get_db()->exec($query, ["comments" => $json, "id" => $_POST["post-id"]]);
    if (!$_SESSION[Blog::SESS_DELETE_COMMENT_RES]) {
        // Query failed
        $_SESSION[Blog::SESS_DELETE_COMMENT_RES] = false;
    }
}