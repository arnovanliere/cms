<?php
/**
 * Delete a post
 * @param Dashboard $app Dashboard-instance to which the Blog-module is loaded
 */
function delete_post(Dashboard $app) {
    if (!isset($_POST["delete-post"]) || !isset($_POST["post-id"]))
        return;

    $query = "DELETE FROM " . Database::TABLE_BLOG . " WHERE id = :id";
    $_SESSION[Blog::SESS_DELETE_POST_RES] = $app->get_db()->exec($query, ["id" => $_POST["post-id"]]);
}