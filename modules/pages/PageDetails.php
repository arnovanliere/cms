<?php

class PageDetails {
    /**
     * @var string Title of the page
     */
    public string $title;

    /**
     * @var string Alias of the page
     */
    public string $alias;

    /**
     * @var array|null Possible sub-pages
     */
    public ?array $sub_pages = null;

    /**
     * @var bool Whether page has side-bar content or not
     */
    public bool $side_bar = false;

    /**
     * @var bool Whether page has a footer or not
     */
    public bool $footer = false;

    /**
     * @var Calendar|null Possible Calendar-module assigned to this page
     */
    public ?Calendar $calendar = null;

    /**
     * @var CalendarSideModule|null Possible Calendar side-module assigned to this page
     */
    public ?CalendarSideModule $side_mod = null;

    /**
     * @var Gallery|null Possible Gallery-module assigned to this page
     */
    public ?Gallery $gallery = null;

    /**
     * @var Blog|null Possible Blog-module assigned to this page
     */
    public ?Blog $blog = null;

    /**
     * PageDetails constructor.
     * @param string $title          Title of the page
     * @param string $alias          Alias of the page (used in URL)
     * @param Module|null $module    Possible module to add to this page
     * @param bool $side_bar         Whether this page has side-bar content or not
     * @param bool $footer           Whether this page has a footer or not
     * @param array|null $sub_pages  Sub-pages of this pages (if there are any)
     * @throws UnsupportedModuleType If a Module is assigned to this page which is not supported
     */
    public function __construct(string $title, string $alias, ?Module $module = null, bool $side_bar = false, $footer = false, ?array $sub_pages = null) {
        $this->title = $title;
        $this->alias = $alias;
        $this->side_bar = $side_bar;
        $this->footer = $footer;
        if (isset($module)) {
            if ($module instanceof CalendarSideModule) {
                $this->side_mod = $module;
            } else if ($module instanceof Calendar) {
                $this->calendar = $module;
            } else if ($module instanceof Gallery) {
                $this->gallery = $module;
            } else if ($module instanceof Blog) {
                $this->blog = $module;
            } else {
                throw new UnsupportedModuleType($title);
            }
        }
        $this->sub_pages = $sub_pages;
    }

}