<?php

include_once  __DIR__ . "/../../../config/config.php";

global $db;
global $pages;

if (isset($_POST["save-page"]) &&
    isset($_POST["page-alias"]) &&
    isset($_POST["page-content"])) {
    $alias = $_POST["page-alias"];
    $content = $_POST["page-content"];
    $sidebar = isset($_POST["side-bar-content"]) ? $_POST["side-bar-content"] : null;
    $footer = isset($_POST["footer-content"]) ? $_POST["footer-content"] : null;
    $_SESSION[Pages::SESSION_SAVE_PAGE_RES] = $pages->set_content($db, $alias, $content, $sidebar, $footer);
} else {
    $_SESSION[Pages::SESSION_SAVE_PAGE_RES] = false;
    header("Location:/admin/?m=".Pages::ID."&p=".Pages::EDIT_PAGE_TAG."&e=".$pages->get_default_page()->alias);
    die();
}
header("Location:/admin/?m=".Pages::ID."&p=".Pages::EDIT_PAGE_TAG."&e=".$_POST["page-alias"]);