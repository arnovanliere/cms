<?php
/**
 * Render the "Edit Page"-page
 * @param Pages $pages   The pages which can be edited
 * @param Dashboard $app The app to which this page is connected
 * @return string        The HTML-code of the page
 * @throws Exception     If Popup throws exception
 */
function get_edit_page_page(Pages $pages, Dashboard $app): string {
    $app->require_login();
    if (!isset($_GET["e"])) {
        header("Location:/admin/?m=".Pages::ID."&p=".Pages::EDIT_PAGE_TAG."&e=".$pages->get_default_page()->alias);
        die();
    }
    $page_details = $app->get_pages()->get_page($_GET["e"]);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(PAG_PAG_NAME_EDIT_PAGE) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
        <?php echo $app->get_menu() ?>
        <nav id="edit-pages">
            <ul>
            <?php
            foreach ($pages->get_pages() as $page) { // Generate menu for the pages which can be edited.
                echo '<li';
                if ($_GET["e"] == $page->alias) {
                    echo ' class="active"';
                }
                echo '><a href="/admin/?m='.Pages::ID.'&p='.Pages::EDIT_PAGE_TAG.'&e='.$page->alias.'">'.$page->title.'</a>';
                if (isset($page->sub_pages)) {
                    echo '<ul>';
                    foreach ($page->sub_pages as $sub_page) {
                        echo '<li';
                        if ($_GET["e"] == $sub_page->alias) {
                            echo ' class="active"';
                        }
                        echo '><a href="/admin/?m='.Pages::ID.'&p='.Pages::EDIT_PAGE_TAG.'&e='.$sub_page->alias.'">'.$sub_page->title.'</a></li>';
                    }
                    echo '</ul>';
                }
                echo '</li>';
            }
            ?>
            </ul>
        </nav>
        <div class="page-content" id="edit-pages-content">
            <h1><?php echo $app->get_string(PAG_PAG_NAME_EDIT_PAGE) ?></h1>
            <div>
                <?php
                $mod_pages = $app->get_pages();
                $cal_page = $mod_pages->get_mod_page(Calendar::class);
                $gal_page = $mod_pages->get_mod_page(Gallery::class);
                $blog_page = $mod_pages->get_mod_page(Blog::class);
                if (isset($cal_page) && $cal_page->alias == $page_details->alias) { // Calendar-module is added to this page
                    echo $app->get_string(CAL_MSG_CAL_PAGE);
                } else if (isset($gal_page) && $gal_page->alias == $page_details->alias) { // Gallery-module is added to this page
                    echo $app->get_string(GAL_MSG_GAL_PAGE);
                } else if (isset($blog_page) && $blog_page->alias == $page_details->alias) { // Blog-module is added to this page
                    echo $app->get_string(BLOG_MSG_BLOG_PAGE);
                } else { // It is a default page, show form to edit content.
                    ob_start(); ?>
                    <form method="post" action="/modules/pages/forms/save_page.php" class="pages-form">
                        <button class="save-page" type="submit" name="save-page"><?php echo $app->get_string(PAG_BTN_SAVE_PAGE) ?></button>
                        <input type="hidden" name="page-alias" value="<?php echo $page_details->alias ?>">
                        <div class="page-layout-row <?php if ($page_details->footer) echo 'with-below' ?>">
                            <textarea class="page-content-textarea" name="page-content"><?php
                                echo $app->get_pages()->get_content($app->get_db(), $page_details->alias)
                            ?></textarea>
                            <?php
                            if ($page_details->side_bar) {
                                ob_start(); ?>
                                <textarea class="page-content-textarea aside" name="side-bar-content"><?php
                                    echo $app->get_pages()->get_sidebar($app->get_db(), $page_details->alias);
                                ?></textarea>
                                <?php echo ob_get_clean();
                            }
                        echo "</div>";
                        if ($page_details->footer) {
                            ob_start(); ?>
                            <div class="page-layout-row">
                                <textarea class="page-content-textarea below" name="footer-content"><?php
                                    echo $app->get_pages()->get_footer($app->get_db(), $page_details->alias);
                                ?></textarea>
                            </div>
                            <?php echo ob_get_clean();
                        }
                        ?>
                    </form>
                    <?php echo ob_get_clean();
                    if (isset($_SESSION[Pages::SESSION_SAVE_PAGE_RES])) {
                        if ($_SESSION[Pages::SESSION_SAVE_PAGE_RES]) {
                            Popup::show_popup($app, Popup::SUCCESS, $app->get_string(PAG_PAG_MSG_CHANGES_SUCCESS));
                        } else {
                            Popup::show_popup($app, Popup::ERROR, $app->get_string(PAG_PAG_MSG_CHANGES_ERROR));
                        }
                        unset($_SESSION[Pages::SESSION_SAVE_PAGE_RES]);
                    }
                }
                ?>
            </div>
        </div>
    </body>
    </html>
    <?php return ob_get_clean();
}