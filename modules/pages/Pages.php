<?php

include_once __DIR__ . "/page/edit_page.php";

class Pages extends Module {
    /**
     * @var array Pages added to this module
     */
    private array $pages;

    /**
     * @var string The mode of displaying this module
     */
    private string $mode;

    /**
     * ID of the module
     */
    public const ID = "page";

    /**
     * Tag of the default page of this module
     */
    public const EDIT_PAGE_TAG = "edit";

    /**
     * Constant to save result of form in $_SESSION
     */
    public const SESSION_SAVE_PAGE_RES = "save-page-res";

    /**
     * Constants for different modes of showing the pages.
     */
    public const MODE_ONE_PAGE = "one-page";
    public const MODE_MULTIPLE_PAGES = "multiple-pages";

    /**
     * Pages constructor.
     * @param array $pages    Pages to add
     * @param string $mode    Mode of showing the pages
     * @throws PagesException If there are duplicate aliases, an invalid mode or
     *                        the mode is MODE_ONE_PAGE and sidebars and / or
     *                        footers and / or sub-pages are provided.
     */
    public function __construct(array $pages, string $mode) {
        if ($mode !== self::MODE_MULTIPLE_PAGES && $mode !== self::MODE_ONE_PAGE) {
            throw new PagesUnknownMode($mode);
        }
        $this->mode = $mode;
        $aliases = [];
        foreach ($pages as $page) {
            array_push($aliases, $page->alias);
            if ($mode === self::MODE_ONE_PAGE) {
                if ($page->side_bar) throw new SideBarOnePage($page->title);
                if ($page->footer) throw new FooterOnePage($page->title);
                if ($page->sub_pages) throw new SubPagesOnePage($page->title);
            }
            if (isset($page->sub_pages)) {
                foreach ($page->sub_pages as $sub_page) {
                    array_push($aliases, $sub_page->alias);
                    if ($mode === self::MODE_ONE_PAGE) {
                        if ($sub_page->side_bar) throw new SideBarOnePage($sub_page->title);
                        if ($sub_page->footer) throw new FooterOnePage($sub_page->title);
                        if ($sub_page->sub_pages) throw new SubPagesOnePage($sub_page->title);
                    }
                }
            }
        }
        if (count(array_unique($aliases)) < count($aliases)) {
            throw new PagesDuplicateAlias();
        }
        $this->pages = $pages;
        parent::__construct(self::ID, PAG_MOD_NAME, [
            self::EDIT_PAGE_TAG => PAG_PAG_NAME_EDIT_PAGE
        ]);
    }

    /**
     * Add columns necessary for storing the pages and delete redundant ones
     * Should be called after constructor and before doing anything else with this class
     * @param Database $db Database instance to perform the queries with
     */
    public function init(Database $db) {
        // Get aliases from database
        $db_aliases = [];
        $ex_aliases = $db->exec_fetch("SELECT alias FROM " . Database::TABLE_PAGES);
        foreach ($ex_aliases as $alias) {
            array_push($db_aliases, $alias["alias"]);
        }

        // Get aliases from added pages
        $pag_aliases = [];
        foreach ($this->pages as $page) {
            array_push($pag_aliases, $page->alias);
            if (isset($page->sub_pages)) {
                foreach ($page->sub_pages as $sub_page) {
                    array_push($pag_aliases, $sub_page->alias);
                }
            }
        }

        // Delete redundant pages from database
        foreach ($db_aliases as $alias) {
            $found = false;
            foreach ($pag_aliases as $p_alias) {
                if ($alias == $p_alias) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $db->exec("DELETE FROM " . Database::TABLE_PAGES . " WHERE alias = :alias", ["alias" => $alias]);
            }
        }

        // Add non-existing columns
        foreach ($pag_aliases as $p_alias) {
            $found = false;
            foreach ($db_aliases as $alias) {
                if ($alias == $p_alias) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $page = $this->get_page($p_alias);
                $db->exec("INSERT INTO " . Database::TABLE_PAGES . " (title, alias, sidebar, footer, content)"
                         ." VALUES(:title, :alias, \"\", \"\", \"\")", ["title" => $page->title, "alias" => $page->alias]);
            }
        }
    }

    /**
     * Load the module
     * @param Dashboard $app The parent from which this module is loaded
     * @throws Exception     If Popup throws exception
     */
    public function load(Dashboard $app) {
        if (!isset($_GET["p"])) {
            $this->load_default_page();
        } else {
            switch ($_GET["p"]) {
                case self::EDIT_PAGE_TAG: {
                    echo get_edit_page_page($this, $app);
                    break;
                }
                default: $this->load_default_page();
            }
        }
    }

    /**
     * Retrieve the added page
     * @return array Array of PageDetails with the added page
     */
    public function get_pages(): array {
        return $this->pages;
    }

    /**
     * Retrieve the title of a page
     * @param string $alias Alias of the page to retrieve the title from
     * @return null|string  Title of the page
     */
    public function get_page_title(string $alias): ?string {
        foreach ($this->pages as $page) {
            if ($page->alias == $alias) {
                return $page->title;
            }
        }
        return null;
    }

    /**
     * Get the default page
     * @return null|PageDetails The details of the page
     */
    public function get_default_page(): ?PageDetails {
        if ($this->pages != null) {
            return $this->pages[0];
        }
        return null;
    }

    /**
     * Retrieve page-details corresponding with 'alias'
     * @param string $alias     The alias of page to retrieve the details from
     * @return PageDetails|null The details of the page
     */
    public function get_page(string $alias): ?PageDetails {
        foreach ($this->pages as $page) {
            if ($page->alias == $alias) {
                return $page;
            }
            if (isset($page->sub_pages)) {
                foreach ($page->sub_pages as $sub_page) {
                    if ($sub_page->alias == $alias) {
                        return $sub_page;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Retrieve the details of the page on which a module is placed
     * @param string $module    The name of the module to check
     * @return PageDetails|null The details of the page
     */
    public function get_mod_page(string $module): ?PageDetails {
        foreach ($this->pages as $page) {
            if (isset($page->blog) && $module == Blog::class ||
                isset($page->calendar) && $module == Calendar::class ||
                isset($page->gallery) && $module == Gallery::class) {
                return $page;
            }
            if (isset($page->sub_pages)) {
                foreach ($page->sub_pages as $sub_page) {
                    if (isset($page->blog) && $module == Blog::class ||
                        isset($page->calendar) && $module == Calendar::class ||
                        isset($page->gallery) && $module == Gallery::class) {
                        return $sub_page;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Retrieve the details of the page on which the Calendar-side-module is placed
     * @return PageDetails|null The details of the page
     */
    public function get_side_mod_page(): ?PageDetails {
        foreach ($this->pages as $page) {
            if (isset($page->side_mod)) {
                return $page;
            }
        }
        return null;
    }

    /**
     * Retrieve the side-bar content of the page corresponding with 'alias'
     * @param Database $db  Database instance to connect to database
     * @param string $alias The alias of the page to retrieve the side-bar from
     * @return string|null  Side-bar as string if page exists, otherwise null
     */
    public function get_sidebar(Database $db, string $alias) {
        $result = $db->exec_fetch("SELECT sidebar FROM " . Database::TABLE_PAGES . " WHERE alias = :alias",
            ["alias" => $alias]);
        if (sizeof($result) == 1 && isset($result[0]["sidebar"])) {
            return $result[0]["sidebar"];
        }
        return null;
    }

    /**
     * Retrieve the footer content of the page corresponding with 'alias'
     * @param Database $db  Database instance to connect to database
     * @param string $alias The alias of the page to retrieve the footer from
     * @return string|null  Footer as string if page exists, otherwise null
     */
    public function get_footer(Database $db, string $alias) {
        $result = $db->exec_fetch("SELECT footer FROM " . Database::TABLE_PAGES . " WHERE alias = :alias",
                                  ["alias" => $alias]);
        if (sizeof($result) == 1 && isset($result[0]["footer"])) {
            return $result[0]["footer"];
        }
        return null;
    }

    /**
     * Get the content of page corresponding with 'alias'
     * @param Database $db  Database instance to connect to database
     * @param string $alias The alias of the page to retrieve the content from
     * @return string|null  Content as string if page exists, otherwise null
     */
    public function get_content(Database $db, string $alias): ?string {
        $result = $db->exec_fetch("SELECT content FROM ". Database::TABLE_PAGES . " WHERE alias = :alias",
                                  ["alias" => $alias]);
        if (sizeof($result) == 1 && isset($result[0]["content"])) {
            return $result[0]["content"];
        }
        return null;
    }

    /**
     * Update the content of the page corresponding with 'alias'
     * @param Database $db          Database instance to connect to database
     * @param string $alias         The alias of the page to update
     * @param string $content       The new content of the page
     * @param string|null $side_bar Possible content of the side-bar
     * @param string|null $footer   Possible content of the footer
     * @return bool                 Whether update succeeded
     */
    public function set_content(Database $db, string $alias, string $content, ?string $side_bar = null, ?string $footer = null): bool {
        return $db->exec("UPDATE " . Database::TABLE_PAGES . " SET content = :content, sidebar = :side_bar, footer = :footer WHERE alias = :alias",
                         ["content" => $content, "side_bar" => $side_bar, "footer" => $footer, "alias" => $alias]);
    }

    /**
     * Retrieve the mode of this site
     * @return string The mode of the site
     */
    public function get_site_mode(): string {
        return $this->mode;
    }

}