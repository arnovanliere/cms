<?php

class PagesException extends ModuleException {
    public function __construct($message = "", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class PagesRedefinition extends PagesException {
    public function __construct($message = "Redefinition of Pages-module", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class PagesDuplicateAlias extends PagesException {
    public function __construct($message = "Duplicate aliases found", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class NoPagesAdded extends PagesException {
    public function __construct($message = "No pages added", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class UnsupportedModuleType extends PagesException {
    public function __construct($page, $code = 0, Throwable $previous = null) {
        parent::__construct("Unsupported module assigned to page $page", $code, $previous);
    }
}

class PagesUnknownMode extends PagesException {
    public function __construct($mode, $code = 0, Throwable $previous = null) {
        parent::__construct("Unsupported mode provided '$mode'", $code, $previous);
    }
}

class SideBarOnePage extends PagesException {
    public function __construct($page, $code = 0, Throwable $previous = null) {
        parent::__construct("Sidebars are not supported for one-page sites. Sidebar is added on page '$page'.", $code, $previous);
    }
}

class FooterOnePage extends PagesException {
    public function __construct($page, $code = 0, Throwable $previous = null) {
        parent::__construct("Footers are not supported for one-page sites. Footer is added on page '$page'.", $code, $previous);
    }
}

class SubPagesOnePage extends PagesException {
    public function __construct($page, $code = 0, Throwable $previous = null) {
        parent::__construct("Sub-pages are not supported for one-page sites. Sub-pages are added on page '$page'.", $code, $previous);
    }
}