<?php

class StyleException extends ModuleException {
    public function __construct($message, $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class StyleRedefinition extends StyleException {
    public function __construct($message = "Redefinition of Style-module", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}