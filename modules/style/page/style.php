<?php
/**
 * Render the "Style"-page
 * @param Dashboard $app  The app to which this page is connected
 * @return string         The HTML-code of the page
 * @throws SetupException If unknown language is set
 */
function get_style_page(Dashboard $app): string {
    $app->require_login();
    $active_styles = $app->get_style()->get_active_styles($app->get_db());
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(STYLE_PAG_NAME_STYLE) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
        <?php echo $app->get_menu() ?>
        <div class="page-content">
            <h1><?php echo $app->get_string(STYLE_PAG_NAME_STYLE) ?></h1>
            <div>
                <form action="/modules/style/forms/save_styles.php" method="post">
                    <?php
                    foreach ($active_styles as $string_key => $style_value) {
                        echo "<label>{$app->get_string($string_key)}<input name='$string_key' value='$style_value' /></label>";
                    }
                    ?>
                    <button name="save-style"><?php echo $app->get_string(STYLE_BTN_SAVE_CHANGES) ?></button>
                </form>
                <?php
                if (isset($_SESSION[Style::SESSION_SAVE_STYLES_RES])) {
                    if ($_SESSION[Style::SESSION_SAVE_STYLES_RES]) {
                        Popup::show_popup($app, Popup::SUCCESS, $app->get_string(STYLE_MSG_CHANGES_SUCCESS));
                    } else {
                        Popup::show_popup($app, Popup::ERROR, $app->get_string(STYLE_MSG_CHANGES_ERROR));
                    }
                    unset($_SESSION[Style::SESSION_SAVE_STYLES_RES]);
                }
                ?>
            </div>
        </div>
    </body>
    </html>
    <?php return ob_get_clean();
}
