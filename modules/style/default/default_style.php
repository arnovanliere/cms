<?php

include_once __DIR__ . "/keys.php";

$default_style = [
    PRIMARY_COLOR => "blue",
    PRIMARY_COLOR_DARK => "darkblue",
    ACCENT_COLOR => "indianred",
    BACKGROUND_COLOR => "lightgrey",
    DEFAULT_FONT => "Roboto, sans-serif",
    TITLE_FONT => "Roboto Bold, sans-serif"
];