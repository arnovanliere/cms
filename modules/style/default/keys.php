<?php
// Each constant has a value corresponding to the index
// of the string which explains what the constant controls
const PRIMARY_COLOR      = STYLE_PRIMARY_COLOR;
const PRIMARY_COLOR_DARK = STYLE_PRIMARY_COLOR_DARK;
const ACCENT_COLOR       = STYLE_ACCENT_COLOR;
const BACKGROUND_COLOR   = STYLE_BACKGROUND_COLOR;
const DEFAULT_FONT       = STYLE_DEFAULT_FONT;
const TITLE_FONT         = STYLE_TITLE_FONT;
