<?php

/**
 * Generate CSS-stylesheet for client-side
 * @param array $style Associative array to retrieve the styles from
 * @return string      The CSS
 */
function get_style(array $style) {
    return "
    body {
        background: {$style[BACKGROUND_COLOR]};
        color: {$style[PRIMARY_COLOR]};
    }
    
    header {
        background: {$style[PRIMARY_COLOR_DARK]};
    }";
}