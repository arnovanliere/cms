<?php

include_once  __DIR__ . "/../../../config/config.php";

global $db;
global $style;

if (isset($_POST["save-style"])) {
    $success = true;
    foreach ($_POST as $key => $value) {
        if ($key === "save-style" || !isset($key) || !isset($value)) continue;
        $temp = $style->update_style($db, $key, $value);
        $success = (!$temp) ? false : $success;
    }
    $_SESSION[Style::SESSION_SAVE_STYLES_RES] = $success;
} else {
    $_SESSION[Style::SESSION_SAVE_STYLES_RES] = false;
}
header("Location:/admin/?m=".Style::ID."&p=".Style::STYLE_PAGE_TAG);