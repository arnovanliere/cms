<?php

include_once __DIR__ . "/page/style.php";
include_once __DIR__ . "/default/default_style.php";
include_once __DIR__ . "/default/style.php";

class Style extends Module {
    /**
     * ID of the module
     */
    public const ID = "style";

    /**
     * Tag of the default page of the module
     */
    public const STYLE_PAGE_TAG = "style";

    /**
     * Constant to save result of form in $_SESSION
     */
    public const SESSION_SAVE_STYLES_RES = "save-styles-res";

    /**
     * Style constructor.
     */
    public function __construct() {
        parent::__construct(self::ID, STYLE_MOD_NAME, [
            self::STYLE_PAGE_TAG => STYLE_PAG_NAME_STYLE
        ]);
    }

    /**
     * Load the module
     * @param Dashboard $app  The parent from which this module is loaded
     * @throws SetupException If unknown language is set
     */
    public function load(Dashboard $app) {
        if (!isset($_GET["p"])) {
            $this->load_default_page();
        } else {
            switch ($_GET["p"]) {
                case self::STYLE_PAGE_TAG: {
                    echo get_style_page($app);
                    break;
                }
                default: $this->load_default_page();
            }
        }
    }

    /**
     * Insert default style in the database
     * @param Database $db Database instance to insert values
     */
    public function init(Database $db) {
        global $default_style;
        foreach ($default_style as $key => $value) {
            $exists = count($db->exec_fetch("SELECT * FROM " . Database::TABLE_STYLE . " WHERE var_key = :var_key",
                                            ["var_key" => $key])) === 1;
            if ($exists) {
                $db->exec("UPDATE " . Database::TABLE_STYLE . " SET var_default = :var WHERE var_key = :var_key",
                          ["var" => $value, "var_key" => $key]);
            } else {
                $db->exec("INSERT INTO " . Database::TABLE_STYLE . "(var_key, var_default) VALUES(:var_key, :var_default)",
                          ["var_key" => $key, "var_default" => $value]);
            }
        }
    }

    /**
     * Retrieve an array with keys and the active values for those keys
     * ('var_default' if 'var_custom' is not set, otherwise 'var_custom')
     * @param Database $db Database instance to retrieve custom/default styles from
     * @return array       Associative array with the keys and values
     */
    public function get_active_styles(Database $db): array {
        $styles = [];
        $db_styles = $db->exec_fetch("SELECT * FROM " . Database::TABLE_STYLE);
        foreach ($db_styles as $style) {
            $styles[$style["var_key"]] =
                (isset($style["var_custom"]))
                    ? $style["var_custom"]
                    : $style["var_default"];
        }
        return $styles;
    }

    /**
     * Retrieve the stylesheet
     * @param Database $db Database instance to retrieve custom/default styles from
     * @return string      CSS-stylesheet as string
     */
    public function retrieve_style(Database $db): string {
        return get_style($this->get_active_styles($db));
    }

    /**
     * Update a specific style with a new value
     * @param Database $db  Database instance to update values with
     * @param string $key   The key of the value to update
     * @param string $value The new value of the style
     * @return bool         Whether style was successfully updated or not
     */
    public function update_style(Database $db, string $key, string $value): bool {
        return $db->exec("UPDATE " . Database::TABLE_STYLE . " SET var_custom = :val WHERE var_key = :key",
                         ["val" => $value, "key" => $key]);
    }

}