<?php

/**
 * Class Module. Base class for all modules (except 'Database').
 */
class Module {
    /**
     * @var string ID of the module
     */
    private string $id;

    /**
     * @var string The name of the module
     */
    private string $name;

    /**
     * @var string Tag of the default page of the module
     */
    private string $def_page;

    /**
     * @var array Associative array of 'alias => STRING_INDEX_PAGE_NAME' with all the pages of this module
     */
    private array $pages;

    /**
     * Module constructor.
     * @param string $id   The ID of the module
     * @param int $name    The string-index of the name of the module
     * @param array $pages Associative array with all the pages of the module
     */
    public function __construct(string $id, int $name, array $pages) {
        $this->id = $id;
        $this->name = $name;
        $this->def_page = array_keys($pages)[0];
        $this->pages = $pages;
    }

    /**
     * Redirect to default page of the module
     */
    public function load_default_page() {
        header("Location:/admin/?m={$this->id}&p={$this->def_page}");
        die();
    }

    /**
     * Retrieve the ID of the module
     * @return string The ID of the module
     */
    public function get_mod_id(): string {
        return $this->id;
    }

    public function get_mod_name(): string {
        return $this->name;
    }

    /**
     * Retrieve the tag of the default page of the module
     * @return string The tag of the default page
     */
    public function get_mod_def_page(): string {
        return $this->def_page;
    }

    /**
     * Retrieve the pages of the module
     * @return array The pages ('alias => name') of the module
     */
    public function get_mod_pages(): array {
        return $this->pages;
    }

}