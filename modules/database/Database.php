<?php

class Database {
    /**
     * @var PDO PDO-connection to communicate with database
     */
    private PDO $PDO;

    /**
     * Constants for different table-names
     */
    public const TABLE_USERS = "users";
    public const TABLE_PAGES = "pages";
    public const TABLE_CALENDAR = "calendar";
    public const TABLE_GALLERY = "gallery";
    public const TABLE_GALLERY_CATEGORIES = "gallery_categories";
    public const TABLE_BLOG = "blog";
    public const TABLE_STYLE = "style";

    /**
     * Database constructor.
     * @param string $db_host  Host of the database
     * @param string $dbname   Name of the database
     * @param string $username Username to login to the database
     * @param string $password Password to login to the database
     * @throws PDOException    If PDO could not be constructed
     */
    public function __construct(string $db_host, string $dbname, string $username, string $password) {
        $dsn = 'mysql:dbname=' . $dbname . ';host=' . $db_host;
        if (!CLI::has_option("--gitlab")) { // Skip this in the GitLab-pipeline
            $this->PDO = new PDO($dsn, $username, $password);
            $this->create_table_users();
            $this->create_table_pages();
            $this->create_table_calendar();
            $this->create_table_gallery();
            $this->create_table_blog();
            $this->create_table_style();
        }
    }

    /**
     * Create the table for users
     */
    private function create_table_users() {
        $query = "CREATE TABLE IF NOT EXISTS " . self::TABLE_USERS . " ( 
                  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
                  username VARCHAR(50) NOT NULL,
                  email VARCHAR(50) NOT NULL,
                  login_hash VARCHAR(100) DEFAULT NULL,
                  reset_hash VARCHAR(100) DEFAULT NULL,
                  password VARCHAR(100) NOT NULL)";
        $this->exec($query);
    }

    /**
     * Create the table for pages
     */
    private function create_table_pages() {
        $query = "CREATE TABLE IF NOT EXISTS " . self::TABLE_PAGES . " ( 
                  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
                  title VARCHAR(50) NOT NULL,
                  alias VARCHAR(50) NOT NULL,
                  sidebar VARCHAR(1000) DEFAULT NULL,
                  footer VARCHAR(1000) DEFAULT NULL,
                  content VARCHAR(1000) NOT NULL)";
        $this->exec($query);
    }

    /**
     * Create table for Calendar-module
     */
    private function create_table_calendar() {
        $this->exec("CREATE TABLE IF NOT EXISTS " . self::TABLE_CALENDAR .
                    " (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY)");
    }

    /**
     * Create table for Gallery-module
     */
    private function create_table_gallery() {
        // Create table for categories
        $this->exec("CREATE TABLE IF NOT EXISTS " . self::TABLE_GALLERY_CATEGORIES . " (
                    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                    category_title VARCHAR(50) UNIQUE NOT NULL)");
        // Create table for image-names with their categories.
        $this->exec("CREATE TABLE IF NOT EXISTS " . self::TABLE_GALLERY . " (
                    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                    file_name VARCHAR(50) NOT NULL,
                    cat_id INT(6) UNSIGNED,
                    CONSTRAINT FK_GalleryCategory FOREIGN KEY (cat_id)
                    REFERENCES " . self::TABLE_GALLERY_CATEGORIES . "(id))");
    }

    /**
     * Create table for Blog-module
     */
    private function create_table_blog() {
        $this->exec("CREATE TABLE IF NOT EXISTS " . self::TABLE_BLOG . " (
                    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                    title VARCHAR(100) NOT NULL,
                    post VARCHAR(3000) NOT NULL,
                    comments_enabled INT(1) NOT NULL,
                    comments JSON DEFAULT NULL,
                    creation_date DATETIME DEFAULT NOW()
        )");
    }

    /**
     * Create table for Style-module
     */
    private function create_table_style() {
        $this->exec("CREATE TABLE IF NOT EXISTS " . self::TABLE_STYLE . " (
                    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                    var_key INT(4) NOT NULL,
                    var_default VARCHAR(60) NOT NULL,
                    var_custom VARCHAR(60) DEFAULT NULL
        )");
    }

    /**
     * Execute a query and return whether it could be executed or not
     * @param string $query       Query to execute
     * @param array|null $options Array with options to bind
     * @return bool               'true' if query was executed, 'false' otherwise
     */
    public function exec(string $query, ?array $options = null): bool {
        $query = $this->PDO->prepare($query);
        if (isset($options)) {
            foreach ($options as $key => &$value) {
                $query->bindParam($key, $value);
            }
        }
        return $query->execute();
    }

    /**
     * Execute a query and fetch the results
     * @param string $query       Query to execute
     * @param array|null $options Array with options to bind
     * @return array|bool         'false' if query could not be executed, array with results if it could be executed
     */
    public function exec_fetch(string $query, ?array $options = null) {
        $prep_query = $this->PDO->prepare($query);
        if (isset($options)) {
            foreach ($options as $key => &$value) {
                if (is_numeric($value))
                    $prep_query->bindParam($key, $value, PDO::PARAM_INT);
                else
                    $prep_query->bindParam($key, $value);
            }
        }
        if (!$prep_query->execute()) {
            return false;
        }
        return $prep_query->fetchAll(PDO::FETCH_ASSOC);
    }

}