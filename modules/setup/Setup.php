<?php

class Setup {
    /**
     * Different languages with which the Dashboard() and Client() can be used
     */
    public const LANG_EN = "en";
    public const LANG_NL = "nl";

    /**
     * @var string Language of 'Dashboard'/'Client'
     */
    private string $lang = self::LANG_EN;

    /**
     * @var string The title of the site
     */
    private string $site_title;

    /**
     * @var Database Instance of 'Database' to add to 'Client'/'Dashboard'
     */
    private Database $db;

    /**
     * @var Calendar|null Instance of 'Calendar' to add to 'Client'/'Dashboard'
     */
    private ?Calendar $calendar = null;

    /**
     * @var Pages|null Instance of 'Pages' to add to 'Client'/'Dashboard'
     */
    private ?Pages $pages = null;

    /**
     * @var Style|null Instance of 'Style' to add to 'Client'/'Dashboard'
     */
    private ?Style $style = null;

    /**
     * @var Gallery|null Instance of 'Gallery' to add to 'Client'/'Dashboard'
     */
    private ?Gallery $gallery = null;

    /**
     * @var Blog|null Instance of 'Blog' to add to 'Client'/'Dashboard'
     */
    private ?Blog $blog = null;

    /**
     * Setup constructor.
     * @param Database $db       The database to connect
     * @param string $lang       Language of the dashboard/client-side (has to be one
     *                           of the constants of this class with prefix 'LANG_')
     * @param string $site_title The title of the site
     * @throws SetupException    If an unknown language is provided
     */
    public function __construct(Database $db, string $lang, string $site_title) {
        if ($lang != self::LANG_EN && $lang != self::LANG_NL) {
            throw new SetupUnknownLanguage($lang);
        }
        $this->db = $db;
        $this->lang = $lang;
        $this->site_title = $site_title;
    }

    /**
     * Add a module
     * @param Module $module   The module to add
     * @throws ModuleException If the module was already added
     */
    public function add_module(Module $module) {
        if ($module instanceof Calendar) {
            if ($this->calendar != null)
                throw new CalendarRedefinition();
            $this->calendar = $module;
        } else if ($module instanceof Pages) {
            if ($this->pages != null)
                throw new PagesRedefinition();
            $this->pages = $module;
        } else if ($module instanceof Style) {
            if ($this->style != null)
                throw new StyleRedefinition();
            $this->style = $module;
        } else if ($module instanceof Gallery) {
            if ($this->gallery != null)
                throw new GalleryRedefinition();
            $this->gallery = $module;
        } else if ($module instanceof Blog) {
            if ($this->blog != null)
                throw new BlogRedefinition();
            $this->blog = $module;
        }
    }

    /**
     * Retrieve Database instance
     * @return Database|null The Database instance
     */
    public function get_db(): ?Database {
        return $this->db;
    }

    /**
     * Retrieve Calendar-module (if added)
     * @return Calendar|null The Calendar-module (if added, otherwise null)
     */
    public function get_calendar(): ?Calendar {
        return $this->calendar;
    }

    /**
     * Retrieve Pages-module (if added)
     * @return Pages|null The Pages-module (if added, otherwise null)
     */
    public function get_pages(): ?Pages {
        return $this->pages;
    }

    /**
     * Retrieve Style-module (if added)
     * @return Style|null The Style-module (if added, otherwise null)
     */
    public function get_style(): ?Style {
        return $this->style;
    }

    /**
     * Retrieve Gallery-module (if added)
     * @return Gallery|null The Gallery-module (if added, otherwise null)
     */
    public function get_gallery(): ?Gallery {
        return $this->gallery;
    }

    /**
     * Retrieve Blog-module (if added)
     * @return Blog|null The Blog-module (if added, otherwise null)
     */
    public function get_blog(): ?Blog {
        return $this->blog;
    }

    /**
     * Retrieve the language with which this is instantiated
     * @return string The language
     */
    public function get_lang(): string {
        return $this->lang;
    }

    /**
     * Retrieve the title of the site
     * @return string The site-title
     */
    public function get_site_title(): string {
        return $this->site_title;
    }

    /**
     * Retrieve a string in the correct language
     * @param int $str_index       Constant representing the index of the string
     * @param array|string $params String(s) to replace '%#s' in strings (positionally)
     * @return string              The string
     * @throws SetupException      If an unknown language is set or '$params' is of a
     *                             different type than string/array
     */
    public function get_string(int $str_index, $params = ""): string {
        global $str_dutch, $str_english;

        if (!is_array($params) && !is_string($params) && !is_int($params))
            throw new SetupTypeOfParameters();

        // Retrieve base-string in correct language
        switch ($this->lang) {
            case self::LANG_EN: {
                $string = $str_english[$str_index];
                break;
            }
            case self::LANG_NL: {
                $string = $str_dutch[$str_index];
                break;
            }
            default: throw new SetupUnknownLanguage($this->lang);
        }

        // If no params are provided, return base-string
        if ((is_array($params) && empty($params)) || $params == "") return $string;

        // 1 parameter is provided so substitute it and return result
        if (is_string($params)) {
            return Strings::str_replace_first($string, $params);
        }

        // An array of parameters is provided, substitute them all
        if (is_array($params)) {
            foreach ($params as $param) {
                $string = Strings::str_replace_first($string, $param);
            }
        }
        return $string;
    }

}