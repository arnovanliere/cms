<?php

class SetupException extends Exception {
    public function __construct($message, $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class SetupUnknownLanguage extends SetupException {
    public function __construct($lang, $code = 0, Throwable $previous = null) {
        parent::__construct("Unknown language ".$lang, $code, $previous);
    }
}

class SetupTypeOfParameters extends SetupException {
    public function __construct($message = "Parameters can only be of type array or string.", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}