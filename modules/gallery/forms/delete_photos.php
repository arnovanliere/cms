<?php
/**
 * Delete a file
 * @param Dashboard $app  Dashboard-instance to which the Gallery-module is loaded
 */
function delete_photos(Dashboard $app) {
    if (!isset($_POST["btn-delete"]) || !isset($_POST["file-delete"])) {
        return;
    }
    $_SESSION[Gallery::SESS_DELETE_RES] =
        unlink(dirname(__FILE__) . "/../photos/" . $_POST["file-delete"]);
    if ($app->get_gallery()->get_var_categories()) {
        // Categories are enabled. Delete this photo from the database as well.
        $query = "DELETE FROM " . Database::TABLE_GALLERY . " WHERE file_name = :name";
        $params = ["name" => $_POST["file-delete"]];
        $app->get_db()->exec($query, $params);
    }
}