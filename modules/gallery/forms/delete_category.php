<?php
/**
 * Delete a category
 * @param Dashboard $app  Dashboard-instance to which the Gallery-module is loaded
 */
function delete_category(Dashboard $app) {
    if (!isset($_POST["delete-category"]) || !isset($_POST["cat-id"])) {
        return;
    }
    $query = "DELETE FROM " . Database::TABLE_GALLERY_CATEGORIES . " WHERE id = :id";
    $params = ["id" => $_POST["cat-id"]];
    $_SESSION[Gallery::SESS_DELETE_CATEGORY_RES] = $app->get_db()->exec($query, $params);
}