<?php
/**
 * Upload a file
 * @param Dashboard $app  Dashboard-instance to which the Gallery-module is loaded
 * @throws SetupException If unknown language is set
 */
function add_photos(Dashboard $app) {
    if (!isset($_FILES["photo"]) || !isset($_POST["upload"])) {
        $_SESSION[Gallery::SESS_UPLOAD_RES] = false;
        $_SESSION[Gallery::SESS_UPLOAD_MSG] = $app->get_string(GAL_MSG_ERR_GENERAL);
        return;
    }

    $file_name = $_FILES["photo"]["name"];
    $file_size = $_FILES["photo"]["size"];
    $file_tmp = $_FILES["photo"]["tmp_name"];
    $file_dir = dirname(__FILE__) . "/../photos/" . $file_name;
    $tmp = explode('.', $file_name);
    $file_ext = strtolower(end($tmp));

    $extensions = array("jpeg","jpg","png");

    if (in_array($file_ext, $extensions) === false) { // Incorrect extension
        $_SESSION[Gallery::SESS_UPLOAD_RES] = false;
        $_SESSION[Gallery::SESS_UPLOAD_MSG] = $app->get_string(GAL_MSG_ERR_EXT);
        return;
    }

    if ($file_size > 2097152) { // File too big
        $_SESSION[Gallery::SESS_UPLOAD_RES] = false;
        $_SESSION[Gallery::SESS_UPLOAD_MSG] = $app->get_string(GAL_MSG_ERR_SIZE);
        return;
    }

    $_SESSION[Gallery::SESS_UPLOAD_RES] = move_uploaded_file($file_tmp, $file_dir);
    if (!$_SESSION[Gallery::SESS_UPLOAD_RES]) { // Some general error occurred (move_uploaded_file returned false)
        $_SESSION[Gallery::SESS_UPLOAD_MSG] = $app->get_string(GAL_MSG_ERR_GENERAL);
    } else if ($app->get_gallery()->get_var_categories() && isset($_POST["category"])) {
        // Categories are enabled. Add this file-name with its category in the database
        $query = "INSERT INTO " . Database::TABLE_GALLERY . "(file_name, cat_id) VALUES
                  (:name, (SELECT id from " . Database::TABLE_GALLERY_CATEGORIES . " WHERE id = :id))";
        $params = ["name" => $file_name, "id" => intval($_POST["category"])];
        $app->get_db()->exec($query, $params);
    }
}