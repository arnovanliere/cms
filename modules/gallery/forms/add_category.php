<?php
/**
 * Add a category
 * @param Dashboard $app Dashboard-instance to which the Gallery-module is loaded
 */
function add_category(Dashboard $app) {
    if (!isset($_POST["create-category"]) ||
        !isset($_POST["category"]) ||
        $_POST["category"] == "") {
        $_SESSION[Gallery::SESS_ADD_CATEGORY_RES] = false;
        return;
    }
    $query = "INSERT INTO " . Database::TABLE_GALLERY_CATEGORIES . " (category_title) VALUES(:title)";
    $params = ["title" => $_POST["category"]];
    $_SESSION[Gallery::SESS_ADD_CATEGORY_RES] = $app->get_db()->exec($query, $params);
}