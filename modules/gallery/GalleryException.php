<?php

class GalleryException extends ModuleException {
    public function __construct($message = "", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class GalleryRedefinition extends GalleryException {
    public function __construct($message = "Redefinition of Gallery-module", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}