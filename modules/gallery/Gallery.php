<?php

include_once __DIR__ . "/pages/add_photos.php";
include_once __DIR__ . "/pages/added_photos.php";
include_once __DIR__ . "/pages/categories.php";
include_once __DIR__ . "/forms/add_photos.php";
include_once __DIR__ . "/forms/add_category.php";
include_once __DIR__ . "/forms/delete_photos.php";
include_once __DIR__ . "/forms/delete_category.php";

class Gallery extends Module {
    /**
     * @var bool Whether categories can be added to photos
     */
    private bool $categories;

    /**
     * ID of the module
     */
    public const ID = "gallery";

    /**
     * Aliases for pages in the dashboard
     */
    public const ADD_PHOTOS_PAGE_TAG = "add-photos";
    public const ADDED_PHOTOS_PAGE_TAG = "added-photos";
    public const CATEGORIES_PAGE_TAG = "categories";

    /**
     * Constants for saving results of forms in $_SESSION
     */
    public const SESS_UPLOAD_RES = "photo-upload-res";
    public const SESS_UPLOAD_MSG = "photo-upload-msg";
    public const SESS_DELETE_RES = "photo-delete-res";
    public const SESS_ADD_CATEGORY_RES = "add-category-res";
    public const SESS_DELETE_CATEGORY_RES = "delete-category-res";

    /**
     * Options to initialize this module with
     */
    public const OPT_CATEGORIES = 0;

    /**
     * Gallery constructor.
     * @param array|null $options Options with which the class can be instantiated
     */
    public function __construct(?array $options = null) {
        $this->categories = false;
        if (isset($options)) {
            foreach ($options as $option) {
                if ($option == self::OPT_CATEGORIES)
                    $this->categories = true;
            }
        }
        parent::__construct(self::ID, GAL_MOD_NAME, [
            self::ADD_PHOTOS_PAGE_TAG => GAL_PAG_NAME_ADD_PHOTOS,
            self::ADDED_PHOTOS_PAGE_TAG => GAL_PAG_NAME_ADDED_PHOTOS,
            self::CATEGORIES_PAGE_TAG => GAL_PAG_NAME_CATEGORIES
        ]);
    }

    /**
     * Load the module
     * @param Dashboard $app  The parent from which this module is loaded
     * @throws SetupException If unknown language is set
     */
    public function load(Dashboard $app) {
        if (!isset($_GET["p"])) {
            $this->load_default_page();
        } else {
            switch ($_GET["p"]) {
                case self::ADD_PHOTOS_PAGE_TAG: {
                    echo get_add_photos_page($app);
                    break;
                }
                case self::ADDED_PHOTOS_PAGE_TAG: {
                    echo get_added_photos_page($app);
                    break;
                }
                case self::CATEGORIES_PAGE_TAG: {
                    if ($this->categories) {
                        echo get_categories_page($app);
                        break;
                    }
                }
                default: $this->load_default_page();
            }
        }
    }

    /**
     * Get the names of the categories that exist
     * @param Database $db Database-instance to retrieve names of the categories from
     * @return array       Array with names of the categories
     */
    public function get_categories(Database $db): array {
        return $db->exec_fetch("SELECT * FROM " . Database::TABLE_GALLERY_CATEGORIES);
    }

    /**
     * Getters for private member-variables
     */
    public function get_var_categories(): bool {
        return $this->categories;
    }

}