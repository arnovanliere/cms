<?php
/**
 * Render the "Categories"-page
 * @param Dashboard $app  The app to which this page is connected
 * @return string         The HTML-code of the page
 * @throws SetupException If unknown language is set
 * @throws Exception      If Popup throws exception
 */
function get_categories_page(Dashboard $app): string {
    $app->require_login();
    if (!$app->get_gallery()->get_var_categories()) {
        $app->get_gallery()->load($app);
        die();
    }
    if (isset($_POST["create-category"])) add_category($app);
    else if (isset($_POST["delete-category"])) delete_category($app);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(GAL_PAG_NAME_CATEGORIES) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
    <?php echo $app->get_menu() ?>
    <div class="page-content" id="gal-add-category">
        <h1><?php echo $app->get_string(GAL_PAG_NAME_CATEGORIES) ?></h1>
        <form method="post">
            <input type="text" name="category" placeholder="<?php echo $app->get_string(GAL_TITLE_CATEGORY_NAME) ?>"/>
            <button type="submit" name="create-category">
                <?php echo $app->get_string(GAL_BTN_CREATE_CATEGORY) ?>
            </button>
        </form>
        <?php
        $categories = $app->get_gallery()->get_categories($app->get_db());
        if (empty($categories)) { // No categories added
            echo $app->get_string(GAL_MSG_NO_CATEGORIES_FOUND);
        } else {
            ob_start(); ?>
            <table>
                <tbody>
                    <tr>
                        <th><?php echo $app->get_string(GAL_TITLE_DELETE_CATEGORY) ?></th>
                        <th><?php echo $app->get_string(GAL_TITLE_CATEGORY_NAME) ?></th>
                    </tr>
                    <?php
                    foreach ($categories as $category) {
                        ob_start(); ?>
                        <tr>
                            <td>
                                <form method="post">
                                    <input type="hidden" name="cat-id" value='<?php echo $category["id"] ?>'>
                                    <button type="submit" name="delete-category" class="warning">
                                        <?php echo $app->get_string(GAL_BTN_DELETE_CATEGORY) ?>
                                    </button>
                                </form>
                            </td>
                            <td><?php echo $category["category_title"] ?></td>
                        </tr>
                        <?php echo ob_get_clean();
                    }
                    ?>
                </tbody>
            </table>
            <?php echo ob_get_clean();
            if (isset($_SESSION[Gallery::SESS_ADD_CATEGORY_RES])) {
                if ($_SESSION[Gallery::SESS_ADD_CATEGORY_RES]) {
                    Popup::show_popup($app, Popup::SUCCESS, $app->get_string(GAL_MSG_ADD_CATEGORY_SUCCESS));
                } else {
                    Popup::show_popup($app, Popup::ERROR,
                                      "<b>{$app->get_string(ERROR_MSG)}</b>{$app->get_string(GAL_MSG_ADD_CATEGORY_ERROR)}");
                }
                unset($_SESSION[Gallery::SESS_ADD_CATEGORY_RES]);
            } else if (isset($_SESSION[Gallery::SESS_DELETE_CATEGORY_RES])) {
                if ($_SESSION[Gallery::SESS_DELETE_CATEGORY_RES]) {
                    Popup::show_popup($app, Popup::SUCCESS, $app->get_string(GAL_MSG_DELETE_CATEGORY_SUCCESS));
                } else {
                    Popup::show_popup($app, Popup::ERROR, $app->get_string(GAL_MSG_DELETE_CATEGORY_ERROR));
                }
                unset($_SESSION[Gallery::SESS_DELETE_CATEGORY_RES]);
            }
        }
        ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}