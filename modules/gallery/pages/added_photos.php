<?php
/**
 * Render the "Added Photos"-page
 * @param Dashboard $app  The app to which this page is connected
 * @return string         The HTML-code of the page
 * @throws SetupException If unknown language is set
 * @throws Exception      If Popup throws exception
 */
function get_added_photos_page(Dashboard $app): string {
    $app->require_login();
    if (isset($_POST["btn-delete"])) delete_photos($app);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(GAL_PAG_NAME_ADDED_PHOTOS) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
    <?php echo $app->get_menu() ?>
    <div class="page-content">
        <h1><?php echo $app->get_string(GAL_PAG_NAME_ADDED_PHOTOS) ?></h1>
        <?php
        $files = array_diff(scandir(__DIR__ . "/../photos"), array('.', '..'));
        if (empty($files)) { // No photos found
            echo "<span>{$app->get_string(GAL_MSG_NO_PHOTOS_UPLOADED)}</span>";
        } else {
            echo "<div class='uploaded-photos'>";
            foreach ($files as $file) {
                echo "<div class='img-wrapper'>
                          <img src='/modules/gallery/photos/$file' alt='$file'>
                          <div class='fullscreen-wrapper' id='$file'>
                              <div class='toolbar'>
                                  <form method='post'>
                                      <input type='hidden' name='file-delete' value='$file'/>
                                      <button type='submit' class='warning' name='btn-delete' class='warning'>
                                          <i class='far fa-trash-alt'></i>
                                      </button>
                                  </form>
                                  <button type='button' 
                                          onclick='document.getElementById(\"$file\").classList.remove(\"visible\")'
                                  ><i class='fas fa-compress-arrows-alt'></i></button>
                              </div>
                              <img src='/modules/gallery/photos/$file' alt='$file'/>
                          </div>
                          <div class='overlay-wrapper'>
                              <div class='actions'>
                                  <form method='post'>
                                      <input type='hidden' name='file-delete' value='$file'/>
                                      <button type='submit' name='btn-delete' class='warning'>
                                          <i class='far fa-trash-alt'></i>
                                      </button>
                                  </form>
                                  <button type='button' 
                                          onclick='document.getElementById(\"$file\").classList.add(\"visible\")'
                                  ><i class='fas fa-expand-arrows-alt'></i></button>
                              </div>
                          </div>
                      </div>";
            }
            echo "</div>";
            if (isset($_SESSION[Gallery::SESS_DELETE_RES])) {
                if ($_SESSION[Gallery::SESS_DELETE_RES]) {
                    Popup::show_popup($app, Popup::SUCCESS, $app->get_string(GAL_MSG_DELETE_PHOTO_SUCCESS));
                } else {
                    Popup::show_popup($app, Popup::ERROR, $app->get_string(GAL_MSG_DELETE_PHOTO_ERROR));
                }
                unset($_SESSION[Gallery::SESS_DELETE_RES]);
            }
        }
        ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}