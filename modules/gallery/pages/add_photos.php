<?php
/**
 * Render the "Add Photos"-page
 * @param Dashboard $app  The app to which this page is connected
 * @return string         The HTML-code of the page
 * @throws SetupException If unknown language is set
 * @throws Exception      If Popup throws exception
 */
function get_add_photos_page(Dashboard $app): string {
    $app->require_login();
    if (isset($_POST["upload"])) add_photos($app);
    $categories = $app->get_gallery()->get_categories($app->get_db());
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(GAL_PAG_NAME_ADD_PHOTOS) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
    <?php echo $app->get_menu() ?>
    <div class="page-content" id="gal-add-photos">
        <h1><?php echo $app->get_string(GAL_PAG_NAME_ADD_PHOTOS) ?></h1>
        <?php
        if ($app->get_gallery()->get_var_categories() && empty($categories)) {
            // Categories are enabled, but no category is added yet.
            echo $app->get_string(GAL_MSG_ERROR_ADDING_PHOTOS);
        } else {
            ob_start(); ?>
            <form method="post" enctype="multipart/form-data">
                <?php
                if ($app->get_gallery()->get_var_categories()) {
                    // Categories are enabled, show dropdown so user can select corresponding category for the photo.
                    echo "<select name='category'>";
                    foreach ($categories as $category) {
                        echo "<option value='{$category["id"]}'>{$category["category_title"]}</option>";
                    }
                    echo "</select>";
                }
                ?>
                <input type="file" name="photo">
                <button name="upload" type="submit"><?php echo $app->get_string(GAL_BTN_UPLOAD) ?></button>
            </form>
            <?php echo ob_get_clean();
        }
        if (isset($_SESSION[Gallery::SESS_UPLOAD_RES])) {
            if ($_SESSION[Gallery::SESS_UPLOAD_RES]) {
                Popup::show_popup($app, Popup::SUCCESS, $app->get_string(GAL_MSG_UPLOAD_SUCCESS));
            } else {
                Popup::show_popup($app, Popup::ERROR,
                                  "{$app->get_string(GAL_MSG_UPLOAD_ERROR)}<br>
                                   <b>{$app->get_string(ERROR_MSG)}</b>
                                   {$_SESSION[Gallery::SESS_UPLOAD_MSG]}");
            }
            unset($_SESSION[Gallery::SESS_UPLOAD_RES]);
            unset($_SESSION[Gallery::SESS_UPLOAD_MSG]);
        }
        ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}