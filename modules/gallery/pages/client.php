<?php
/**
 * Render client-side content for the gallery-page
 * @param Client $client       Client-instance to which this is connected
 * @param PageDetails $details PageDetails of the page to render
 * @return string              HTML-code of the page
 * @throws CalendarException   If default-page contains Calendar and throws exception
 * @throws PagesException      If no pages are added
 * @throws SetupException      If unknown language is set
 */
function get_client_content_gallery(Client $client, PageDetails $details): string {
    if ($client->get_pages()->get_page($details->alias)->gallery == null) {
        $client->load_default_page();
        die();
    }
    $files = array_diff(scandir(__DIR__ . "/../photos"), array('.', '..'));
    $categories = [];
    if ($client->get_gallery()->get_var_categories()) {
        // Categories are enabled
        $categories = $client->get_db()->exec_fetch("SELECT * FROM " . Database::TABLE_GALLERY_CATEGORIES);
    }
    if (isset($_POST["filter-photos"]) && isset($_POST["category"])) {
        // User has selected a category to filter photos
        $query = "SELECT * FROM " . Database::TABLE_GALLERY . " WHERE cat_id = :id";
        $params = ["id" => $_POST["category"]];
        $results = $client->get_db()->exec_fetch($query, $params);
        $files = [];
        foreach ($results as $result) { // Add the file-names from the database
            array_push($files, $result["file_name"]);
        }
    }
    ob_start();
    if (!empty($categories)) { // There are categories added. Show a form to filter them.
        echo "<form method='post'><select name='category'>";
        foreach ($categories as $category) {
            echo "<option value='{$category["id"]}'>{$category["category_title"]}</option>";
        }
        echo "</select>
                      <button type='submit' name='filter-photos'>{$client->get_string(GAL_BTN_FILTER)}</button>
                      <button type='submit' name='reset'>{$client->get_string(GAL_BTN_RESET)}</button>
                      </form>";
    }
    ?>
    <div class="thumbs">
        <?php
        foreach ($files as $file) { // Generate a thumbnail for each photo.
            echo "<div class='thumb'>
                              <img src='/modules/gallery/photos/$file' alt=''/>
                          </div>";
        }
        ?>
    </div>
    <div id="lightboxes" class="lightboxes">
        <?php
        foreach ($files as $file) { // Generate a lightbox for each photo.
            echo "<div class='lightbox-item'>
                              <img src='/modules/gallery/photos/$file' alt=''/>
                          </div>";
        }
        ?>
        <div id="indicator"></div>
    </div>
    <div id="lightboxes-nav" class="lightboxes-nav">
        <div id="nav-prev" class="nav-prev">
            <span></span>
            <span></span>
        </div>
        <div id="nav-next" class="nav-next">
            <span></span>
            <span></span>
        </div>
        <div id="close-lightbox" class="close">
            <span></span>
            <span></span>
        </div>
    </div>
    <?php return ob_get_clean();
}