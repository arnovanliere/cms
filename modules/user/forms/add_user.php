<?php
/**
 * Add a new user
 * @param Dashboard $app  Dashboard-instance to which the User-module is loaded
 * @throws SetupException If unknown language is set
 */
function add_user(Dashboard $app) {
    if (!isset($_POST["add-user"]) || !isset($_POST["username"]) || !isset($_POST["email"]) ||
        !isset($_POST["password"]) || !isset($_POST["conf-password"])) {
        return;
    }

    // Check if a user with this username already exists
    $res = $app->get_db()->exec_fetch("SELECT * FROM " . Database::TABLE_USERS . " WHERE username = :name",
                                      ["name" => $_POST["username"]]);
    if (!empty($res)) {
        $_SESSION[User::SESS_ADD_USER_RES] = false;
        $_SESSION[User::SESS_ADD_USER_MSG] = $app->get_string(USER_ADD_ERROR_USERNAME);
        return;
    }

    // Check if a user with this email already exists
    $res = $app->get_db()->exec_fetch("SELECT * FROM " . Database::TABLE_USERS . " WHERE email = :email",
                                      ["email" => $_POST["email"]]);
    if (!empty($res)) {
        $_SESSION[User::SESS_ADD_USER_RES] = false;
        $_SESSION[User::SESS_ADD_USER_MSG] = $app->get_string(USER_ADD_ERROR_EMAIL);
        return;
    }

    // Check if passwords are the same
    if ($_POST["password"] != $_POST["conf-password"]) {
        $_SESSION[User::SESS_ADD_USER_RES] = false;
        $_SESSION[User::SESS_ADD_USER_MSG] = $app->get_string(USER_ADD_ERROR_PW_MATCH);
        return;
    }

    // Check if passwords are long enough
    if (strlen($_POST["password"]) < 8) {
        $_SESSION[User::SESS_ADD_USER_RES] = false;
        $_SESSION[User::SESS_ADD_USER_MSG] = $app->get_string(USER_ADD_ERROR_PW_LENGTH);
        return;
    }

    // All checks passed, add user
    $hashed_pw = password_hash($_POST["password"], PASSWORD_DEFAULT);
    $query = "INSERT INTO " . Database::TABLE_USERS . "(username, email, password) VALUES(:name, :email, :password)";
    $params = ["name" => $_POST["username"], "email" => $_POST["email"], "password" => $hashed_pw];
    $_SESSION[User::SESS_ADD_USER_RES] = $app->get_db()->exec($query, $params);
    if (!$_SESSION[User::SESS_ADD_USER_RES]) {
        $_SESSION[User::SESS_ADD_USER_MSG] = $app->get_string(USER_ADD_ERROR_GENERAL);
    }
}