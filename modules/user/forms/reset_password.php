<?php
/**
 * Reset password of a user
 * @param Dashboard $app  Dashboard-instance to which the User-module is loaded
 * @param array $user     Results from database with details of the user to update password
 * @throws SetupException If unknown language is set
 */
function reset_password(Dashboard $app, array $user) {
    if (!isset($_POST["reset-pw"]) || !isset($_POST["password"]) || !isset($_POST["conf-password"])) {
        return;
    }

    // New passwords do not match
    if ($_POST["password"] != $_POST["conf-password"]) {
        $_SESSION[User::SESS_RESET_PW_RES] = false;
        $_SESSION[User::SESS_RESET_PW_MSG] = $app->get_string(USER_UPDATE_PW_NON_MATCH);
        return;
    }

    // New password too short
    if (strlen($_POST["password"]) < 8) {
        $_SESSION[User::SESS_RESET_PW_RES] = false;
        $_SESSION[User::SESS_RESET_PW_MSG] = $app->get_string(USER_UPDATE_PW_TOO_SHORT);
        return;
    }

    // Set new password in database
    $password = password_hash($_POST["password"], PASSWORD_DEFAULT);
    $query = "UPDATE " . Database::TABLE_USERS . " SET password = :password WHERE id = :id";
    $params = ["password" => $password, "id" => $user["id"]];
    $_SESSION[User::SESS_RESET_PW_RES] = $app->get_db()->exec($query, $params);
    // Remove reset-hash
    $app->get_db()->exec("UPDATE " . Database::TABLE_USERS . " SET reset_hash = NULL WHERE id = :id",
                         ["id" => $user["id"]]);
    if (!$_SESSION[User::SESS_RESET_PW_RES]) {
        $_SESSION[User::SESS_RESET_PW_MSG] = $app->get_string(USER_UPDATE_PW_GENERAL_ERR);
    }
}