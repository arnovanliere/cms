<?php
/**
 * Change password of current user
 * @param Dashboard $app  Dashboard-instance to which the User-module is loaded
 * @param array $user     Details of the logged in user
 * @throws SetupException If unknown language is set
 */
function change_password(Dashboard $app, array $user) {
    if (!isset($_POST["change-password"]) || !isset($_POST["old-password"]) ||
        !isset($_POST["new-password"]) || !isset($_POST["conf-new-password"])) {
        return;
    }

    // New passwords do not match
    if ($_POST["new-password"] != $_POST["conf-new-password"]) {
        $_SESSION[User::SESS_CHANGE_PW_RES] = false;
        $_SESSION[User::SESS_CHANGE_PW_MSG] = $app->get_string(USER_UPDATE_PW_NON_MATCH);
        return;
    }

    // New password too short
    if (strlen($_POST["new-password"]) < 8) {
        $_SESSION[User::SESS_CHANGE_PW_RES] = false;
        $_SESSION[User::SESS_CHANGE_PW_MSG] = $app->get_string(USER_UPDATE_PW_TOO_SHORT);
        return;
    }

    // Old password not correct
    if (!password_verify($_POST["old-password"], $user["password"])) {
        $_SESSION[User::SESS_CHANGE_PW_RES] = false;
        $_SESSION[User::SESS_CHANGE_PW_MSG] = $app->get_string(USER_UPDATE_PW_OLD_PW_ERR);
        return;
    }

    $new_pw = password_hash($_POST["new-password"], PASSWORD_DEFAULT);
    $query = "UPDATE " . Database::TABLE_USERS . " SET password = :password WHERE id = :id";
    $params = ["password" => $new_pw, "id" => $user["id"]];
    $_SESSION[User::SESS_CHANGE_PW_RES] = $app->get_db()->exec($query, $params);
    if (!$_SESSION[User::SESS_CHANGE_PW_RES]) {
        $_SESSION[User::SESS_CHANGE_PW_MSG] = $app->get_string(USER_UPDATE_PW_GENERAL_ERR);
    }
}