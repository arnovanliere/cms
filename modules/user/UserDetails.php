<?php

class UserDetails {
    /**
     * @var string Username of the user
     */
    public string $username;

    /**
     * @var string Password of the user
     */
    public string $password;

    /**
     * @var string Email of the user
     */
    public string $email;

    /**
     * UserDetails constructor.
     * @param string $username Username of the logged in user
     * @param string $password Password of the logged in user
     * @param string $email    Email of the logged in user
     */
    public function __construct(string $username, string $password, string $email) {
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
    }

}