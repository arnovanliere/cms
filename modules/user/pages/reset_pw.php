<?php
/**
 * Render the "Reset Password"-page
 * @param Dashboard $app  The app to which this page is connected
 * @return string         The HTML-code of this page
 * @throws SetupException If unknown language is set
 * @throws Exception      If Popup throws exception
 */
function get_reset_password_page(Dashboard $app): string {
    if (!isset($_GET["h"]) || $_GET["h"] == "") { echo get_login_page($app); die; }
    $user = $app->get_db()->exec_fetch("SELECT * FROM " . Database::TABLE_USERS . " WHERE reset_hash = :hash",
                                       ["hash" => $_GET["h"]]);
    if (!$user || count($user) != 1) { echo get_login_page($app); die; } // No user found with this reset_hash
    $user = $user[0];
    if (isset($_POST["reset-pw"])) reset_password($app, $user);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang(); ?>">
    <head>
        <title><?php echo $app->get_string(DASH_PAG_NAME_RECOVER_PW) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
        <div id="login-form">
            <h1><?php echo $app->get_string(DASH_PAG_NAME_RECOVER_PW) ?></h1>
            <form method="post">
                <label for="password"><?php echo $app->get_string(USER_INPUT_NEW_PW) ?></label>
                <input id="password" type="password" name="password" required>
                <label for="conf-password"><?php echo $app->get_string(USER_INPUT_CONF_NEW_PW) ?></label>
                <input id="conf-password" type="password" name="conf-password" required>
                <button name="reset-pw" type="submit"><?php echo $app->get_string(DASH_BTN_RESET_PW) ?></button>
            </form>
        </div>
        <?php
        if (isset($_SESSION[User::SESS_RESET_PW_RES])) {
            if ($_SESSION[User::SESS_RESET_PW_RES]) {
                Popup::show_popup($app, Popup::SUCCESS, $app->get_string(USER_RESET_PW_SUCCESS));
            } else {
                Popup::show_popup($app, Popup::ERROR,
                          "{$app->get_string(USER_RESET_PW_ERROR)}<br>
                                   <b>{$app->get_string(ERROR_MSG)}</b>
                                   {$_SESSION[User::SESS_RESET_PW_MSG]}");
            }
            unset($_SESSION[User::SESS_RESET_PW_RES]);
            unset($_SESSION[User::SESS_RESET_PW_MSG]);
        }
        ?>
    </body>
    </html>
    <?php return ob_get_clean();
}