<?php
/**
 * Render the "Profile"-page
 * @param Dashboard $app  The app to which this page is connected
 * @return string         The HTML-code of this page
 * @throws SetupException If unknown language is set
 * @throws Exception      If popup throws exception
 */
function get_profile_page(Dashboard $app): string {
    $app->require_login();
    $user = $_SESSION["user"];
    if (isset($_POST["change-password"])) change_password($app, $_SESSION["user"]);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(USER_PAG_NAME_PROFILE) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
    <?php echo $app->get_menu() ?>
    <div class="page-content">
        <h1><?php echo $app->get_string(USER_PAG_NAME_PROFILE) ?></h1>
        <div id="user-info">
            <div>
                <span class="title"><?php echo $app->get_string(USER_TITLE_USERNAME) ?></span>
                <span><?php echo $user["username"] ?></span>
            </div>
            <div>
                <span class="title"><?php echo $app->get_string(USER_TITLE_EMAIL) ?></span>
                <span><?php echo $user["email"] ?></span>
            </div>
            <div>
                <span class="title"><?php echo $app->get_string(USER_TITLE_PASSWORD) ?></span>
                <span>&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;</span>
                <button id="change-password"><?php echo $app->get_string(USER_BTN_CHANGE_PW) ?></button>
            </div>
            <form method="post" id="change-password-form">
                <input type="password" id="old-password" name="old-password"
                       placeholder="<?php echo $app->get_string(USER_INPUT_OLD_PW) ?>" required>
                <input type="password" id="new-password" name="new-password"
                       placeholder="<?php echo $app->get_string(USER_INPUT_NEW_PW) ?>" required>
                <input type="password" id="conf-new-password" name="conf-new-password"
                       placeholder="<?php echo $app->get_string(USER_INPUT_CONF_NEW_PW) ?>" required>
                <button type="submit" name="change-password">Change password</button>
            </form>
        </div>
        <?php
        if (isset($_SESSION[User::SESS_CHANGE_PW_RES])) {
            if ($_SESSION[User::SESS_CHANGE_PW_RES]) {
                Popup::show_popup($app, Popup::SUCCESS, $app->get_string(USER_UPDATE_PW_SUCCESS));
            } else {
                Popup::show_popup($app, Popup::ERROR,
                          "{$app->get_string(USER_UPDATE_PW_ERROR)}<br>
                                   <b>{$app->get_string(ERROR_MSG)}</b>
                                   {$_SESSION[User::SESS_CHANGE_PW_MSG]}");
            }
            unset($_SESSION[User::SESS_CHANGE_PW_RES]);
            unset($_SESSION[User::SESS_CHANGE_PW_MSG]);
        }
        ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}