<?php
/**
 * Render the "Add User"-page
 * @param Dashboard $app  The app to which this page is connected
 * @return string         The HTML-code of this page
 * @throws SetupException If unknown language is set
 * @throws Exception      If popup throws exception
 */
function get_add_user_page(Dashboard $app): string {
    $app->require_login();
    if (isset($_POST["add-user"])) add_user($app);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(USER_PAG_NAME_ADD_USER) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
    <?php echo $app->get_menu() ?>
    <div class="page-content" id="pag-add-user">
        <h1><?php echo $app->get_string(USER_PAG_NAME_ADD_USER) ?></h1>
        <form method="post">
            <input type="text" name="username" placeholder="<?php echo $app->get_string(USER_INPUT_USERNAME)?>" required>
            <input type="email" name="email" placeholder="<?php echo $app->get_string(USER_INPUT_EMAIL)?>" required>
            <input type="password" name="password" placeholder="<?php echo $app->get_string(USER_INPUT_PASSWORD)?>" required>
            <input type="password" name="conf-password" placeholder="<?php echo $app->get_string(USER_INPUT_CONF_PASSWORD)?>" required>
            <button type="submit" name="add-user"><?php echo $app->get_string(USER_BTN_ADD_USER) ?></button>
        </form>
        <?php
        if (isset($_SESSION[User::SESS_ADD_USER_RES])) {
            if ($_SESSION[User::SESS_ADD_USER_RES]) {
                Popup::show_popup($app, Popup::SUCCESS, "User successfully added");
            } else {
                Popup::show_popup($app, Popup::ERROR,
                                  "<b>{$app->get_string(ERROR_MSG)}</b>
                                  {$_SESSION[User::SESS_ADD_USER_MSG]}");
            }
        }
        ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}