<?php

include_once __DIR__ . "/pages/add_user.php";
include_once __DIR__ . "/pages/profile.php";
include_once __DIR__ . "/pages/reset_pw.php";
include_once __DIR__ . "/forms/add_user.php";
include_once __DIR__ . "/forms/change_password.php";
include_once __DIR__ . "/forms/reset_password.php";

class User extends Module {
    /**
     * ID of the module
     */
    public const ID = "user";

    /**
     * Tags of the pages for this module
     */
    public const PROFILE_PAGE_TAG = "profile";
    public const RESET_PW_PAGE_TAG = "reset-pw";
    public const ADD_USER_PAGE_TAG = "add-user";

    /**
     * Constants for saving results of forms in $_SESSION
     */
    public const SESS_CHANGE_PW_RES = "change-pw-res";
    public const SESS_CHANGE_PW_MSG = "change-pw-msg";
    public const SESS_RESET_PW_RES = "reset-pw-res";
    public const SESS_RESET_PW_MSG = "reset-pw-msg";
    public const SESS_ADD_USER_RES = "add-user-res";
    public const SESS_ADD_USER_MSG = "add-user-msg";

    /**
     * User constructor.
     */
    public function __construct() {
        parent::__construct(self::ID, USER_MOD_NAME, [
            self::PROFILE_PAGE_TAG => USER_PAG_NAME_PROFILE,
            self::ADD_USER_PAGE_TAG => USER_PAG_NAME_PROFILE
        ]);
    }

    /**
     * Load the module
     * @param Dashboard $app  The parent from which this module is loaded
     * @throws SetupException If unknown language is set
     */
    public function load(Dashboard $app) {
        if (!isset($_GET["p"])) {
            $this->load_default_page();
        } else {
            switch ($_GET["p"]) {
                case self::PROFILE_PAGE_TAG: {
                    echo get_profile_page($app);
                    break;
                }
                case self::RESET_PW_PAGE_TAG: {
                    echo get_reset_password_page($app);
                    break;
                }
                case self::ADD_USER_PAGE_TAG: {
                    echo get_add_user_page($app);
                    break;
                }
                default: $this->load_default_page();
            }
        }
    }

}