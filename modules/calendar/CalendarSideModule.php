<?php

class CalendarSideModule extends Calendar {

    /**
     * @var int The number of events in the side-module
     */
    private int $nr_options;

    /**
     * CalendarSideModule constructor.
     * @param array $options     Options to construct parent
     * @param int $nr_options    Number of options to add to side-module
     * @throws CalendarException If parent could not be constructed
     */
    public function __construct(array $options, int $nr_options) {
        parent::__construct($options);
        $this->nr_options = $nr_options;
    }

    /**
     * Render the side-module
     * @param Client $client     The Client from which this module is rendered
     * @throws CalendarException If 'init()' is not called on 'Calendar()'
     * @throws SetupException    If unknown language is set
     */
    public function render_module(Client $client) {
        ob_start(); ?>
        <div id="cal-side-mod-wrapper">
            <?php
            $sort = $this->get_option("date")->column;
            $sort_dir = Calendar::SORT_DESC;
            $events = $this->get_events($client->get_db(), $this->nr_options, $sort, $sort_dir);
            echo '<div id="cal-side-mod">';
            if (!isset($events) || !$events || count($events) == 0) {
                echo '<div class="no-events-found">'.$client->get_string(CAL_MSG_NO_EVENTS_FOUND).'</div>';
            } else {
                foreach ($events as $event) {
                    echo "<div class='cal-event'>";
                    foreach ($event as $field => $value) {
                        if (isset($value) && $value != "")
                            echo "<div class='cal-$field'>$value</div>";
                    }
                    echo "</div>";
                }
            }
            echo '</div>';
            ?>
        </div>
        <?php echo ob_get_clean();
    }

}