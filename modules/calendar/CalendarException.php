<?php

class CalendarException extends ModuleException {
    public function __construct($message, $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class CalendarRedefinition extends CalendarException {
    public function __construct($message = "Redefinition of Calendar-module", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class CalendarUnknownType extends CalendarException {
    public function __construct($message = "Unknown type of event-option", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class IncompatibleCalendarOption extends CalendarException {
    public function __construct($message = "Dependent options can only be added to a checkbox-input", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class CalendarDuplicateColumn extends CalendarException {
    public function __construct($message = "Duplicate column name found in Calendar-options", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class CalendarSideModuleUninitialized extends CalendarException {
    public function __construct($message = "No side-module added, yet trying to access it.", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class CalendarUnknownSortDirection extends CalendarException {
    public function __construct($message = "Unknown sort-direction provided. Only 'ASC' and 'DESC' allowed.", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}