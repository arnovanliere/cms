<?php

class CalendarOption {
    /**
     * @var string Name of the option. Used as placeholder/label in forms.
     */
    public string $name;

    /**
     * @var string Name of the column in which to save the value
     */
    public string $column;

    /**
     * @var int Type of option (one of the constants of this class starting with 'INPUT_')
     */
    public int $type;

    /**
     * @var bool Whether or not this option is required (add 'required' in forms and an '*' to placeholders/labels)
     */
    public bool $required;

    /**
     * @var bool Whether this option should be filterable on client-side
     */
    public bool $filterable;

    /**
     * @var bool Whether this option should be searchable on client-side
     */
    public bool $searchable;

    /**
     * @var array|null Options dependent on this one (this one has to be a checkbox)
     */
    public ?array $dependent_options = null;

    /**
     * Constants to represent different input-types of HTML inputs
     */
    public const INPUT_CHECKBOX = 0;
    public const INPUT_TEXT = 1;
    public const INPUT_DATE = 2;
    public const INPUT_DATETIME = 3;
    public const INPUT_NUMBER = 4;
    public const INPUT_FLOAT = 5;

    /**
     * Different options which can be applied when instancing this class
     */
    public const REQUIRED = 0;
    public const FILTERABLE = 1;
    public const SEARCHABLE = 2;

    /**
     * CalendarOption constructor.
     * @param string $name                  Name of the option (used as label for the input in adding/editing calendar-items)
     * @param string $col_name              Name of the column in the database to save this in
     * @param int $type                     Type of input
     * @param array $options                Options for this CalendarOption. Possible:
     *                                      - CalendarOption::REQUIRED
     *                                      - CalendarOption::FILTERABLE
     *                                      - CalendarOption::SEARCHABLE
     * @param array|null $dependent_options Options that depend on this option (this option needs to be a checkbox)
     * @throws CalendarUnknownType          If there is an unknown type of input defined
     * @throws IncompatibleCalendarOption   If there are dependent options added but the input is not a checkbox
     */
    public function __construct(string $name, string $col_name, int $type, array $options = [], ?array $dependent_options = null) {
        if ($type != self::INPUT_CHECKBOX && $type != self::INPUT_TEXT &&
            $type != self::INPUT_DATE && $type != self::INPUT_DATETIME &&
            $type != self::INPUT_NUMBER && $type != self::INPUT_FLOAT) {
            throw new CalendarUnknownType();
        }
        $this->name = $name;
        $this->column = $col_name;
        $this->type = $type;
        $this->required = $this->array_contains($options, self::REQUIRED);
        $this->filterable = $this->array_contains($options, self::FILTERABLE);
        $this->searchable = $this->array_contains($options, self::SEARCHABLE);
        if (isset($dependent_options) && $type != self::INPUT_CHECKBOX) {
            throw new IncompatibleCalendarOption();
        }
        $this->dependent_options = $dependent_options;
    }

    /**
     * Check if a value is present in an array
     * @param array $array The array to check
     * @param int $value   The value to check for
     * @return bool        Whether value is present or not
     */
    private function array_contains(array $array, int $value): bool {
        foreach ($array as $key => $val) {
            if ($val == $value)
                return true;
        }
        return false;
    }

}