<?php
/**
 * Delete an event
 * @param Dashboard $app Dashboard-instance to which the Calendar-module is loaded
 */
function delete_event(Dashboard $app) {
    if (!isset($_POST["delete-event"]) || !isset($_POST["event-id"])) {
        $_SESSION[Calendar::SESS_DELETE_EVENT_RES] = false;
        return;
    }
    $query = "DELETE FROM ".Database::TABLE_CALENDAR." WHERE id = :id";
    $params = ["id" => $_POST["event-id"]];
    $_SESSION[Calendar::SESS_DELETE_EVENT_RES] = $app->get_db()->exec($query, $params);
}