<?php
/**
 * Update column with empty value
 * @param Database $db   Database to perform update on
 * @param string $column Column name to update
 * @param bool $checkbox Whether column contains value for checkbox or not
 */
function make_empty(Database $db, string $column, bool $checkbox = false) {
    if ($checkbox) {
        $db->exec("UPDATE ".Database::TABLE_CALENDAR." SET $column = 0");
    } else {
        $db->exec("UPDATE ".Database::TABLE_CALENDAR." SET $column = NULL");
    }
}

/**
 * Check if array contains a value
 * @param array $array  Array to check
 * @param string $value Value to check for in array
 * @return bool         'true' if 'array' contains 'value', 'false' otherwise
 */
function contains(array $array, string $value): bool {
    foreach ($array as $item)
        if ($item == $value)
            return true;
    return false;
}

/**
 * Edit an event
 * @param Dashboard $app Dashboard-instance to which the Calendar-module is loaded
 */
function edit_event(Dashboard $app) {
    if (!isset($_POST["id"])) { $_SESSION[Calendar::SESS_EDIT_EVENT_RES] = false; return; } // ID-parameter not set
    $event = $app->get_db()->exec_fetch("SELECT * FROM ".Database::TABLE_CALENDAR." WHERE id = :id", ["id" => $_POST["id"]]);
    if (!$event || count($event) == 0) { $_SESSION[Calendar::SESS_EDIT_EVENT_RES] = false; return; } // No event with this id found
    $skip = [];
    foreach ($_POST as $key => $value) {
        if (contains($skip, $key)) continue; // This key is already processed and added to 'skip'
        if ($key == "save-edits-event") continue; // Skip the button
        if (!isset($value) || $value == "") continue; // Skip keys with empty values
        if ($key == "id") continue; // Skip POST-value of id, should not be updated
        $explode = explode("-", $key);
        if ($explode[0] == "edit") { // This key is a wrapper around a checkbox with dependent options
            array_push($skip, $explode[1]); // Skip this key after it is processed
            $option = $app->get_calendar()->get_option($explode[1]);
            // Edit values concerning a checkbox, but checkbox is not checked, so update all dependent options
            // of 'checkbox' to nothing
            if (!isset($_POST[$explode[1]])) {
                make_empty($app->get_db(), $option->column, true);
                foreach ($option->dependent_options as $dependent_option) {
                    array_push($skip, $dependent_option->column); // Skip all these keys after they are processed
                    make_empty($app->get_db(), $dependent_option->column);
                }
            } else {
                $app->get_db()->exec("UPDATE ".Database::TABLE_CALENDAR." SET $option->column = 1 WHERE id = :id",
                                     ["id" => $_POST["id"]]);
                foreach ($option->dependent_options as $dependent_option) {
                    array_push($skip, $dependent_option->column); // Skip all these keys after they are processed
                    $app->get_db()->exec("UPDATE ".Database::TABLE_CALENDAR." SET $dependent_option->column = :val WHERE id = :id",
                                         ["val" => $_POST[$dependent_option->column], "id" => $_POST["id"]]);
                }
            }
        } else { // It is just an option, not depending on or another option, nor having its own dependent option.
            $app->get_db()->exec("UPDATE " . Database::TABLE_CALENDAR . " SET $key = :val WHERE id = :id",
                                 ["val" => $value, "id" => $_POST["id"]]);
        }
    }
    $_SESSION[Calendar::SESS_EDIT_EVENT_RES] = true;
}