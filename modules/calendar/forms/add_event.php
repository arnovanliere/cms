<?php
/**
 * Add an event
 * @param Dashboard $app Dashboard-instance to which the Calendar-module is loaded
 */
function add_event(Dashboard $app) {
    if ($app->get_calendar() == null) return; // No Calendar-module is added
    $placeholders = [];
    $columns = "INSERT INTO ".Database::TABLE_CALENDAR." (";
    $values = ") VALUES(";
    foreach ($_POST as $key => $value) {
        if ($key == "add-event") continue; // Skip the POST-value for the button
        if ($app->get_calendar()->get_option($key)->type == CalendarOption::INPUT_CHECKBOX) {
            $value = ($value == "on") ? 1 : 0; // Checkboxes are saved as 1 or 0. Convert POST-value to 1 or 0.
        }
        if (!isset($value) || $value == "") continue; // Skip keys with no value
        $columns .= "$key, ";
        $values .= ":$key, ";
        $placeholders[$key] = $value;
    }
    $columns = rtrim($columns, ", "); // Remove trailing comma from the column-names
    $values = rtrim($values, ", "); // Remove trailing comma from the value-placeholders
    $values .= ")";
    $query = $columns . $values;
    $_SESSION[Calendar::SESS_ADD_EVENT_RES] = $app->get_db()->exec($query, $placeholders);
}