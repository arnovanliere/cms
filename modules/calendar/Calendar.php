<?php

include_once __DIR__ . "/pages/add_event.php";
include_once __DIR__ . "/pages/added_events.php";
include_once __DIR__ . "/pages/edit_event.php";

class Calendar extends Module {
    /**
     * @var array Options added to this module
     */
    private array $options;

    /**
     * @var CalendarSideModule Side-module for on another page
     */
    private CalendarSideModule $side_mod;

    /**
     * ID of the module
     */
    public const ID = "cal";

    /**
     * Tags of the pages ('aliases')
     */
    public const ADD_EVENT_TAG = "add-event";
    public const ADDED_EVENTS_TAG = "added-events";
    public const EDIT_EVENT_TAG = "edit-event";

    /**
     * Constants to save results of database-transactions
     */
    public const SESS_ADD_EVENT_RES = "add-event-res";
    public const SESS_EDIT_EVENT_RES = "edit-event-res";
    public const SESS_DELETE_EVENT_RES = "delete-event-res";

    /**
     * Sorting directions
     */
    public const SORT_ASC = "ASC";
    public const SORT_DESC = "DESC";

    /**
     * Calendar constructor.
     * @param array $options     Array of 'CalendarOption'
     * @throws CalendarException - If duplicate column-names are defined in 'options'
     *                           - If an option with column-name 'id' is defined
     *                           - If an option with an '-' in its column-name is defined
     *                           - If an option with column name with prefix 'tickets_' is defined
     */
    public function __construct(array $options) {
        $cols = [];
        foreach ($options as $option) {
            if ($option->column == "id") {
                throw new CalendarException("The 'id'-column is reserved.");
            }
            if (count(explode("-", $option->column)) > 1) {
                throw new CalendarException("Column-name cannot an '-' in it.");
            }
            if (explode("_", $option->column)[0] == "tickets") {
                throw new CalendarException("Column name cannot have prefix 'tickets_'");
            }
            array_push($cols, $option->column);
            if (isset($option->dependent_options)) {
                foreach ($option->dependent_options as $dependent_option) {
                    array_push($cols, $dependent_option->column);
                    if (count(explode("-", $dependent_option->column)) > 1) {
                        throw new CalendarException("Column-name cannot an '-' in it.");
                    }
                    if (explode("_", $dependent_option->column)[0] == "tickets") {
                        throw new CalendarException("Column name cannot have prefix 'tickets_'");
                    }
                }
            }
        }
        if (count(array_unique($cols)) < count($cols)) {
            throw new CalendarDuplicateColumn();
        }
        $this->options = $options;
        parent::__construct(self::ID, CAL_MOD_NAME, [
            self::ADDED_EVENTS_TAG => CAL_PAG_NAME_ADDED_EVENTS,
            self::ADD_EVENT_TAG => CAL_PAG_NAME_ADD_EVENT,
            self::EDIT_EVENT_TAG => CAL_PAG_NAME_EDIT_EVENT
        ]);
    }

    /**
     * Add columns necessary for storing the calendar-options and delete redundant ones.
     * Should be called after constructor and before doing anything else with this class.
     * Should only be called once (only if no Tickets-module is added).
     * When a Tickets-module is used as well, do not call this function. In that case
     * this is done by the 'init()' function of Tickets.
     * @param Database $db Database instance to perform the queries with
     */
    public function init(Database $db) {
        // Find all columns which are currently present in database
        $fields = [];
        $results = $db->exec_fetch("SHOW COLUMNS FROM " . Database::TABLE_CALENDAR);
        foreach ($results as $result) {
            array_push($fields, $result["Field"]);
        }
        // Find all the options which are added
        $options_cols = [];
        function get_added_options(array $options, array &$save) {
            foreach ($options as $option) {
                array_push($save, $option->column);
                if (isset($option->dependent_options)) {
                    get_added_options($option->dependent_options, $save);
                }
            }
        }
        get_added_options($this->options, $options_cols);
        // Search for options which have no corresponding column and add a column for those options
        foreach ($options_cols as $col) {
            $found = $col == "id";
            foreach ($fields as $field) {
                if ($field == $col) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $query = "ALTER TABLE " . Database::TABLE_CALENDAR . " ADD COLUMN " . $col;
                $cor_option = $this->get_option($col);
                if (isset($cor_option)) {
                    switch ($cor_option->type) {
                        case CalendarOption::INPUT_TEXT: { $query .= " VARCHAR(100)"; break; }
                        case CalendarOption::INPUT_CHECKBOX: { $query .= " INT(1) DEFAULT 0"; break; }
                        case CalendarOption::INPUT_DATETIME: { $query .= " DATETIME"; break; }
                        case CalendarOption::INPUT_DATE: { $query .= " DATE"; break; }
                        case CalendarOption::INPUT_NUMBER: { $query .= " INT(100)"; break; }
                        case CalendarOption::INPUT_FLOAT: { $query .= " DOUBLE(100, 2)"; break; }
                        default: $query .= " VARCHAR(100)";
                    }
                } else {
                    $query .= " VARCHAR(100)";
                }
                $db->exec($query);
            }
        }
        // Search for columns that have no corresponding option and drop those columns
        foreach ($fields as $field) {
            $found = $field == "id";
            foreach ($options_cols as $col) {
                if ($field == $col) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $db->exec("ALTER TABLE " . Database::TABLE_CALENDAR . " DROP COLUMN " . $field);
            }
        }
    }

    /**
     * Retrieve the added options for events
     * @return array Array of CalendarOption
     */
    public function get_options(): array {
        return $this->options;
    }

    /**
     * Add options
     * @param array $options     Options to add
     * @throws CalendarException If duplicate column-name is found
     */
    public function add_options(array $options) {
        $cols = [];
        // Retrieve all column-names of the options in 'array' and save them in 'cols'
        function get_cols(array $array, array $cols) {
            foreach ($array as $option) {
                array_push($cols, $option->column);
                if (isset($option->dependent_options)) {
                    foreach ($option->dependent_options as $dependent_option) {
                        array_push($cols, $dependent_option->column);
                    }
                }
            }
        }
        get_cols($this->options, $cols);
        get_cols($options, $cols);
        // Duplicate column-names present
        if (count(array_unique($cols)) < count($cols)) {
            throw new CalendarDuplicateColumn();
        }
        foreach ($options as $option) {
            array_push($this->options, $option);
        }
    }

    /**
     * Find an option with column 'col'
     * @param string $col          The name of the column of the option to find
     * @return CalendarOption|null 'null' if option doesn't exists, otherwise the corresponding option
     */
    public function get_option(string $col): ?CalendarOption {
        foreach ($this->options as $option) {
            if ($option->column == $col) {
                return $option;
            }
            if (isset($option->dependent_options)) {
                foreach ($option->dependent_options as $dependent_option) {
                    if ($dependent_option->column == $col) {
                        return $dependent_option;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Load the module
     * @param Dashboard $app     The parent from which this module is loaded
     * @throws CalendarException If something went wrong with the side-module
     * @throws Exception         If Popup throws exception
     */
    public function load(Dashboard $app) {
        if (!isset($_GET["p"])) {
            $this->load_default_page();
        } else {
            switch ($_GET["p"]) {
                case self::ADD_EVENT_TAG: {
                    echo get_add_event_page($app);
                    break;
                }
                case self::EDIT_EVENT_TAG: {
                    echo get_edit_event_page($app);
                    break;
                }
                case self::ADDED_EVENTS_TAG: {
                    echo get_added_events_page($app);
                    break;
                }
                default: $this->load_default_page();
            }
        }
    }

    /**
     * Render the form for events
     * @param Dashboard $app  The Dashboard-instance to which this is connected
     * @param array $options  The options to render
     * @param bool $required  Whether required options should marked with 'required'
     * @param bool $render_id Whether field for ID should be rendered (false in recursive calls)
     * @throws SetupException If unknown language is set
     */
    public function render_form(Dashboard $app, array $options, bool $required, bool $render_id) {
        if ($render_id) { echo '<input type="number" name="id" placeholder="ID*" required>'; }
        foreach ($options as $option) {
            if (isset($option->dependent_options) && !$required) {
                echo '<label for="edit-'.$option->column.'">'
                     .$app->get_string(CAL_EDIT_CONCERNING).' "'.$option->name.'"';
                echo '<input id="edit-'.$option->column.'" type="checkbox" name="edit-'.$option->column.'"
                      onclick="document.getElementById(\'edit-wrapper-' . $option->column . '\').classList.toggle(\'visible\')">';
                echo '</label>';
                echo '<div id="edit-wrapper-'.$option->column.'" class="wrapper-calendar-options">';
            }
            switch ($option->type) {
                case CalendarOption::INPUT_CHECKBOX: {
                    echo '<label for="' . $option->column . '">' . $option->name;
                    echo '<input id="' . $option->column . '" name="' . $option->column . '" type="checkbox"';
                    if ($option->required && $required) echo ' required ';
                    if (isset($option->dependent_options)) {
                        echo ' onclick="document.getElementById(\'wrapper-' . $option->column . '\').classList.toggle(\'visible\')"';
                    }
                    echo '></label>';
                    break;
                }
                case CalendarOption::INPUT_TEXT: {
                    echo '<input name="'.$option->column.'" type="text" placeholder="'.$option->name;
                    if ($option->required && $required) echo '*" required>';
                    else echo '">';
                    break;
                }
                case CalendarOption::INPUT_DATE: {
                    echo '<input name="'.$option->column.'" type="date"';
                    if ($option->required && $required) echo ' required>';
                    else echo '">';
                    break;
                }
                case CalendarOption::INPUT_DATETIME: {
                    echo '<input name="'.$option->column.'" type="datetime-local"';
                    if ($option->required && $required) echo ' required>';
                    else echo '">';
                    break;
                }
                case CalendarOption::INPUT_NUMBER: {
                    echo '<input name="'.$option->column.'" type="number" placeholder="'.$option->name;
                    if ($option->required && $required) echo '*" required>';
                    else echo '">';
                    break;
                }
                case CalendarOption::INPUT_FLOAT: {
                    echo '<input name="'.$option->column.'" type="number" step="0.01" placeholder="'.$option->name;
                    if ($option->required && $required) echo '*" required>';
                    else echo '">';
                    break;
                }
            }
            if (isset($option->dependent_options)) {
                echo '<div id="wrapper-' . $option->column . '" class="wrapper-calendar-options">';
                $this->render_form($app, $option->dependent_options, $required, false);
                echo '</div>';
                if (!$required) {
                    echo '</div>';
                }
            }
        }
    }

    /**
     * Add a side-module for this module. Should be called before get_side_module()
     * @param int $nr_events     How many events to display in the side-module. Default is 3.
     * @throws CalendarException If the parent (this class) could not be constructed
     */
    public function add_side_module(int $nr_events = 3) {
        $this->side_mod = new CalendarSideModule($this->options, $nr_events);
    }

    /**
     * Get the side-module. Should only be called after add_side_module.
     * @return CalendarSideModule The side-module for this module
     * @throws CalendarException  If no side-module was added
     */
    public function get_side_module(): CalendarSideModule {
        if (!isset($this->side_mod))
            throw new CalendarSideModuleUninitialized();
        return $this->side_mod;
    }

    /**
     * Get the events which are added in the database
     * @param Database $db       Database-instance to retrieve events from
     * @param int|null $max      Whether to limit number of events
     * @param string|null $sort  Column to sort on
     * @param string $sort_dir   "ASC" or "DESC"
     * @return array|bool        Array with events if query could be executed, false otherwise
     * @throws CalendarException If init() is not called or an unknown sort_dir is provided
     */
    public function get_events(Database $db, ?int $max = null, ?string $sort = null, string $sort_dir = Calendar::SORT_ASC) {
        if ($sort_dir != Calendar::SORT_ASC && $sort_dir != Calendar::SORT_DESC)
            throw new CalendarUnknownSortDirection();
        $placeholders = [];
        $query = "SELECT * FROM ".Database::TABLE_CALENDAR;
        if (isset($sort)) {
            $query .= " ORDER BY :col $sort_dir";
            $placeholders["col"] = $sort;
        }
        if (isset($max)) {
            $query .= " LIMIT :max";
            $placeholders["max"] = $max;
        }
        return $db->exec_fetch($query, $placeholders);
    }

}