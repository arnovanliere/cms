<?php

include_once __DIR__ . "/../forms/delete_event.php";

/**
 * Render the "Added Events"-page
 * @param Dashboard $app     The app to which this page is connected to
 * @return string            HTML-code of the page
 * @throws CalendarException If events could not be retrieved
 * @throws Exception         If Popup throws exception
 */
function get_added_events_page(Dashboard $app): string {
    $app->require_login();
    if (isset($_POST["delete-event"]) && isset($_POST["event-id"]))
        delete_event($app);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(CAL_PAG_NAME_ADDED_EVENTS) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
    <?php echo $app->get_menu() ?>
    <div class="page-content">
        <h1><?php echo $app->get_string(CAL_PAG_NAME_ADDED_EVENTS) ?></h1>
        <?php $events = $app->get_calendar()->get_events($app->get_db());
        if (is_array($events) && count($events) > 0) { // There are events found in the database and they are successfully retrieved
            ob_start(); ?>
            <div id="added-events-wrapper">
            <table>
                <tbody>
                    <tr>
                        <?php
                        echo "<th>".$app->get_string(CAL_TITLE_DELETE_EVENT)."</th><th>ID</th>";
                        foreach ($events[0] as $field => $value) {
                            $option = $app->get_calendar()->get_option($field);
                            if ($option != null) // Skip database-columns with no corresponding option in the Calendar-module. Should not happen btw.
                                echo "<th>$option->name</th>";
                        }
                        ?>
                    </tr>
                    <?php
                    foreach ($events as $event) {
                        echo "<tr>
                                <td>
                                    <form method='post'>
                                        <input type='hidden' name='event-id' value='".$event["id"]."'>
                                        <button type='submit' name='delete-event' class='warning'>"
                                            .$app->get_string(CAL_BTN_DELETE_EVENT).
                                        "</button>
                                    </form>
                                </td>";
                        foreach ($event as $field => $value) {
                            echo "<td>$value</td>";
                        }
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            </div>
            <?php
            echo ob_get_clean();
        } else { // No events were found
            echo '<div id="cal-no-events-found">'.$app->get_string(CAL_MSG_NO_EVENTS_FOUND).'</div>';
        }
        if (isset($_SESSION[Calendar::SESS_DELETE_EVENT_RES])) {
            if ($_SESSION[Calendar::SESS_DELETE_EVENT_RES]) {
                Popup::show_popup($app, Popup::SUCCESS, $app->get_string(CAL_MSG_DELETE_SUCCESS));
            } else {
                Popup::show_popup($app, Popup::ERROR, $app->get_string(CAL_MSG_DELETE_ERROR));
            }
            unset($_SESSION[Calendar::SESS_DELETE_EVENT_RES]);
        }
        ?>
    </div>
    </body>
    </html>
    <?php return ob_get_clean();
}