<?php

include_once __DIR__ . "/../forms/edit_event.php";

/**
 * Render the "Edit Event"-page
 * @param Dashboard $app The app to which this page is connected to
 * @return string        The HTML-code of this page
 * @throws Exception     If Popup throws exception
 */
function get_edit_event_page(Dashboard $app): string {
    $app->require_login();
    if (isset($_POST["save-edits-event"])) edit_event($app);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(CAL_PAG_NAME_EDIT_EVENT) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
        <?php echo $app->get_menu() ?>
        <div class="page-content">
            <h1><?php echo $app->get_string(CAL_PAG_NAME_EDIT_EVENT) ?></h1>
            <form class="calendar-form" method="post">
                <?php $app->get_calendar()->render_form($app, $app->get_calendar()->get_options(), false, true) ?>
                <button type="submit" name="save-edits-event"><?php echo $app->get_string(CAL_BTN_EDIT_EVENT) ?></button>
            </form>
            <?php
            if (isset($_SESSION[Calendar::SESS_EDIT_EVENT_RES])) {
                if ($_SESSION[Calendar::SESS_EDIT_EVENT_RES]) {
                    Popup::show_popup($app, Popup::SUCCESS, $app->get_string(CAL_MSG_EDIT_SUCCESS));
                } else {
                    Popup::show_popup($app, Popup::ERROR, $app->get_string(CAL_MSG_EDIT_ERROR));
                }
                unset($_SESSION[Calendar::SESS_EDIT_EVENT_RES]);
            }
            ?>
        </div>
    </body>
    </html>
    <?php return ob_get_clean();
}
