<?php
/**
 * Render client-side content for the calendar-page
 * @param Client $client       Client-instance to which this is connected
 * @param PageDetails $details PageDetails of the calendar-page
 * @return string              HTML-code of the page
 * @throws PagesException      If no pages were added
 * @throws CalendarException   If something went wrong with the side-module
 * @throws SetupException      If unknown language is set
 */

function get_client_content_calendar(Client $client, PageDetails $details): string {
    if ($client->get_pages()->get_page($details->alias)->calendar == null) {
        // Calendar-module is not added. Redirect user to default page.
        $client->load_default_page();
        die();
    }
    $calendar = $details->calendar;
    $events = $calendar->get_events($client->get_db());
    $searched = false;
    $searched_query = "";
    if (isset($_POST["search_btn"])) { // User tries to search for something
        foreach ($_POST as $key => $value) {
            if (explode("-", $key)[0] == "search" && isset($value) && $value != "") {
                $searched = true;
                $searched_query = $value;
                $value = "%$value%";
                $key = explode("-", $key)[1];
                $query = "SELECT * FROM " . Database::TABLE_CALENDAR . " WHERE $key LIKE :val";
                $params = ["val" => $value];
                $events = $client->get_db()->exec_fetch($query, $params);
            }
        }
    } else if (isset($_POST["filter"])) { // User tries to filter on something
        $query = "SELECT * FROM ".Database::TABLE_CALENDAR;
        $params = [];
        foreach ($_POST as $key => $value) {
            if (explode("-", $key)[0] == "search" || // Skip search-input
                !isset($value) || $value == "") continue;
            $query .= " WHERE $key = :$key";
            $params[$key] = $value;
        }
        $events = $client->get_db()->exec_fetch($query, $params);
    } else { // Load all events
        $events = $calendar->get_events($client->get_db());
    }
    ob_start();
    if (empty($events) && !$searched) { // No events are added
        echo "<div class='no-events-found'>{$client->get_string(CAL_MSG_NO_EVENTS_FOUND)}</div>";
    } else { // There are events, load them
        $options = $calendar->get_options();
        foreach ($options as $option) {
            if ($option->searchable) {
                // This option is searchable. Add a form with text-input to
                // search for events with a specific value for this option.
                echo "<form method='post'>";
                echo "<input name='search-{$option->column}' type='text' placeholder='{$option->name}'/>";
                echo "<button name='search_btn'>{$client->get_string(CAL_BTN_SEARCH)}</button>";
                echo "<button name='reset'>{$client->get_string(CAL_BTN_RESET)}</button>";
                echo "<form>";
            }
            if ($option->filterable) {
                // This option is filterable. Add a form with a dropdown to
                // filter events with a specific value for this option.
                $filter_options = $client->get_db()->exec_fetch("SELECT DISTINCT {$option->column} FROM ".Database::TABLE_CALENDAR);
                echo "<form method='post'>";
                echo "<select name='{$option->column}'>";
                foreach ($filter_options as $field => $value) {
                    echo "<option value='{$value[$option->column]}'>{$value[$option->column]}</option>";
                }
                echo "</select>";
                echo "<button name='filter'>{$client->get_string(CAL_BTN_FILTER)}</button>";
                echo "</form>";
            }
        }
        if (empty($events)) { // User has searched, no events were found.
            echo "<div class='no-events-found'>{$client->get_string(CAL_MSG_NO_EVENTS_FOUND_SEARCH)}\"$searched_query\"</div>";
        } else { // User has searched, events were found, display them.
            foreach ($events as $event) {
                echo "<div class='event'>";
                foreach ($event as $field => $value) {
                    if ($field == "id") continue;
                    echo "<div class='cal-$field'>$value</div>";
                }
                echo "</div>";
            }
        }
    }
    return ob_get_clean();
}