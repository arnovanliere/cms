<?php

include_once __DIR__ . "/../forms/add_event.php";

/**
 * Render the "Add Event"-page
 * @param Dashboard $app The app to which this page is connected to
 * @return string        HTML-code of the page
 * @throws Exception     If Popup throws exception
 */
function get_add_event_page(Dashboard $app): string {
    $app->require_login();
    if (isset($_POST["add-event"])) add_event($app);
    ob_start(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo $app->get_lang() ?>">
    <head>
        <title><?php echo $app->get_string(CAL_PAG_NAME_ADD_EVENT) ?></title>
        <?php echo Dashboard::get_header() ?>
    </head>
    <body>
        <?php echo $app->get_menu() ?>
        <div class="page-content">
            <h1><?php echo $app->get_string(CAL_PAG_NAME_ADD_EVENT) ?></h1>
            <form class="calendar-form" method="post">
                <?php $app->get_calendar()->render_form($app, $app->get_calendar()->get_options(), true, false) ?>
                <button type="submit" name="add-event"><?php echo $app->get_string(CAL_BTN_ADD_EVENT) ?></button>
            </form>
            <?php
            if (isset($_SESSION[Calendar::SESS_ADD_EVENT_RES])) {
                if ($_SESSION[Calendar::SESS_ADD_EVENT_RES]) {
                    Popup::show_popup($app, Popup::SUCCESS, $app->get_string(CAL_MSG_ADD_SUCCESS));
                } else {
                    Popup::show_popup($app, Popup::ERROR, $app->get_string(CAL_MSG_ADD_ERROR));
                }
                unset($_SESSION[Calendar::SESS_ADD_EVENT_RES]);
            }
            ?>
        </div>
    </body>
    </html>
    <?php return ob_get_clean();
}